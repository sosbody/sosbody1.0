﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using SosBody.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Infrastructure;

namespace SosBody.Infrastructure
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext() : base("IdentityDb") { }

        static AppIdentityDbContext()
        {
            //Database.SetInitializer<AppIdentityDbContext>(new DropCreateDatabaseAlways<AppIdentityDbContext>());
        }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<IncludeMetadataConvention>();
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //}
    }

    public class IdentityDbInit : DropCreateDatabaseIfModelChanges<AppIdentityDbContext>
    {
        protected override void Seed(AppIdentityDbContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }

        public void PerformInitialSetup(AppIdentityDbContext context)
        {

        }
    }
}