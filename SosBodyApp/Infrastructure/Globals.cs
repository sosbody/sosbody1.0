﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SosBody.Infrastructure
{
    public static class Globals
    {
        public static readonly string ProfileImagesPath = "/Uploads/ProfileImages/";

        public static readonly List<string> Specialization = new List<string>() { "Fyzioterapie funkčních poruch",
            "Fyzio po úrazech a operacích",
            "Fyzio dětí",
            "Fyzio neurologických diagnóz",
            "Fyzio gerontů",
            "Home Fyzio"
        };

        public static readonly string GoogleMapsApiKey = "AIzaSyAs3to2w-T45JFThZpzXckKBgTOIeK11c8";// "AIzaSyAt0cyg5UtwEjneTIEBV2UktaTJtkZMmvE";
    }
}