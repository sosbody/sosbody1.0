﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOSBody.Helpers.Google
{
    public interface ISyncHistoryRecorder
    {
        void WriteSyncHistoryRecord(string source1Id, string source2Id, DateTime syncTime);
        DateTime? GetLastSyncTime(string sourceId, string source2Id);
    }
}