﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Google.GData.Calendar;
using Google.GData.Extensions;
using Google.GData.Client;

namespace SOSBody.Helpers
{
    //http://www.daimto.com/google-calendar-api-authentication-with-c/
    public class CalendarHelper
    {
        public string ApplicationName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        /// <summary>
        /// GetService method will be accessed by the Google Calendar data API and return the service object: 
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static CalendarService GetService(string applicationName,
            string userName, string password)
        {
            //CalendarService service = new CalendarService(new BaseClientService.Initializer());

            CalendarService service = new CalendarService(applicationName);
            service.setUserCredentials(userName, password);
            return service;
        }

        /// <summary>
        /// GetAllEvents method reads the calendar data from the Google calendar by using query object. 
        /// This will require two parameters. CalendarService is the service object that you created in the previous step. 
        /// startDate is the actual query which you need to query from Google calendar. EventQuery object holds the query. 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="startData"></param>
        /// <returns></returns>
        public static IEnumerable<EventEntry> GetAllEvents(CalendarService service, DateTime? startData)
        {
            EventQuery query = new EventQuery();
            query.Uri = new Uri("http://www.google.com/calendar/feeds/" + service.Credentials.Username + "/private/full");
            if (startData != null)
                query.StartDate = startData.Value;

            EventFeed calFeed = service.Query(query);
            return calFeed.Entries.Cast<EventEntry>();
        }

        /// <summary>
        /// AddEvent method will add new calendar data into Google calendar. Event title, content, location, start time and end time have to be provided.
        /// Event title is your new calendar title
        /// Content is the description
        /// Location is event location
        /// Start time is when you plan to start the event
        /// End time is when you plan to finish the event
        /// </summary>
        /// <param name="service"></param>
        /// <param name="title"></param>
        /// <param name="contents"></param>
        /// <param name="location"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        public static void AddEvent(CalendarService service, string title,
            string contents, string location, DateTime startTime, DateTime endTime)
        {
            EventEntry entry = new EventEntry();

            // Set the title and content of the entry.
            entry.Title.Text = title;
            entry.Content.Content = contents;

            // Set a location for the event.
            Where eventLocation = new Where();
            eventLocation.ValueString = location;
            entry.Locations.Add(eventLocation);

            When eventTime = new When(startTime, endTime);
            entry.Times.Add(eventTime);

            Uri postUri = new Uri
            ("http://www.google.com/calendar/feeds/default/private/full");

            // Send the request and receive the response:
            AtomEntry insertedEntry = service.Insert(postUri, entry);
        }

        public static void ClearAll(CalendarService service)
        {
            var events = GetAllEvents(service, null);
            foreach (var eventEntry in events)
            {
                service.Delete(eventEntry);
            }
        }
    }
}