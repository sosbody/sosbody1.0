﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOSBody.Helpers.Google
{
    public class SyncManager<T> where T : IEquatable<T>
    {
        public string Name { get; set; }
        public ISyncDataSource<T> Source1 { get; set; }

        public IEnumerable<T> Synchronize(DateTime? lastSyncTime)
        {
            try
            {
                if (lastSyncTime != null)
                {
                    IEnumerable<T> list = Source1.GetItemHeaders(lastSyncTime).ToList();
                    return list;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}