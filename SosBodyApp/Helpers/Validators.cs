﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOSBody.Helpers
{
    public class Validators
    {
        public bool IsBankAccountValid(string prefix, string account)
        {
            if (account == null)
                return false;

            int[] weightedNums = { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
            bool prefixOk = false;
            bool accountOk = false;
            bool prefixNull = false;
            int modulo11 = 0;
            int j = 0;

            int total = 0;

            if (prefix != null)
            {
                char[] prefixArray = prefix.ToCharArray();
                if (prefixArray.Length > 10)
                    return false;

                total = 0;

                j = 0;
                for (int i = prefixArray.Length - 1; i >= 0; i--)
                {
                    if (Char.IsNumber(prefixArray[i]))
                    {
                        int num = Convert.ToInt16(prefixArray[i].ToString());
                        total += num * weightedNums[j++];
                    }
                    else
                        return false;
                }

                modulo11 = total % 11;
                if (modulo11 == 0)
                    prefixOk = true;
            }
            else
                prefixNull = true;

            char[] accountArray = account.ToCharArray();
            if (accountArray.Length > 10)
                return false;
            total = 0;

            j = 0;
            for (int i = accountArray.Length - 1; i >= 0; i--)
            {
                if (Char.IsNumber(accountArray[i]))
                {
                    int num = Convert.ToInt16(accountArray[i].ToString());
                    total += num * weightedNums[j++];
                }
                else
                    return false;
            }

            modulo11 = total % 11;
            if (modulo11 == 0)
                accountOk = true;

            return prefixOk && accountOk || accountOk && prefixNull;
        }


        public bool IsEmailValid(string email)
        {
            string emailTrimed = email.Trim();

            if (!string.IsNullOrEmpty(emailTrimed))
            {
                bool hasWhitespace = emailTrimed.Contains(" ");

                int indexOfAtSign = emailTrimed.LastIndexOf('@');

                if (indexOfAtSign > 0 && !hasWhitespace)
                {
                    string afterAtSign = emailTrimed.Substring(indexOfAtSign + 1);

                    int indexOfDotAfterAtSign = afterAtSign.LastIndexOf('.');

                    if (indexOfDotAfterAtSign > 0 && afterAtSign.Substring(indexOfDotAfterAtSign).Length > 1)
                        return true;
                }
            }

            return false;
        }
    }
}