﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SOSBody.Helpers
{
    public class PhoneHelper
    {
        IdentityDbEntities db = new IdentityDbEntities();

        public string GetPhonePure(string phoneNumber)
        {
            if (phoneNumber == null)
                return "";

            string ret = phoneNumber;

            if (phoneNumber.Contains('-'))
            {
                int p = phoneNumber.IndexOf('-');
                string substr = phoneNumber.Substring(p + 1);
                return substr;
            }

            return ret;
        }

        public string GetPhonePrefix(string phoneNumber)
        {
            if (phoneNumber == null)
                return "+420";

            string ret = "+420";

            if (phoneNumber.Contains('-'))
            {
                int p = phoneNumber.IndexOf('-');
                string substr = phoneNumber.Substring(0, p);
                return substr;
            }

            return ret;
        }

        public List<SelectListItem> InitPhonePrefixList(string phoneNumber)
        {
            List<SelectListItem> prefixList = new List<SelectListItem>();
            var phonePrefixList = db.PhonePrefix.ToList();

            if (String.IsNullOrEmpty(phoneNumber) || phoneNumber.Contains('-') == false)
            {
                foreach (PhonePrefix phonePrefix in phonePrefixList)
                {
                    SelectListItem item = new SelectListItem();
                    string str = phonePrefix.Text.ToString();
                    item.Value = str;
                    item.Text = str;
                    if (phonePrefix.Default)
                        item.Selected = true;
                    prefixList.Add(item);
                }
            }
            else if (phoneNumber.Contains('-'))
            {
                int p = phoneNumber.IndexOf('-');
                string substr = phoneNumber.Substring(0, p);
                var phonePrefix = db.PhonePrefix.Where(x => x.Text == substr).FirstOrDefault();
                if (phonePrefix == null)
                    return prefixList;

                foreach (PhonePrefix phone in phonePrefixList)
                {
                    SelectListItem item = new SelectListItem();
                    string str = phone.Text.ToString();
                    item.Value = str;
                    item.Text = str;
                    if (str == phonePrefix.Text)
                        item.Selected = true;
                    prefixList.Add(item);
                }
            }


            return prefixList;
        }

        public int GetPhonePrefixLength(string phoneNumber)
        {
            int ret = 9;
            if (String.IsNullOrEmpty(phoneNumber))
                return 9;

            if (phoneNumber.Contains('-'))
            {
                int p = phoneNumber.IndexOf('-');
                string str = phoneNumber.Substring(0, p);
                var phonePrefix = db.PhonePrefix.Where(x => x.Text == str).FirstOrDefault();
                if (phonePrefix == null)
                    return 9;

                return phonePrefix.Length;
            }

            return ret;
        }
    }
}