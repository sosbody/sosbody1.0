﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace SOSBody.Helpers
{
    public class Logger
    {
        IdentityDbEntities db = new IdentityDbEntities();

        public enum Level
        {
            Exception,
            Error,
            Warning,
            Trace
        }

        public void Log(DateTime date, Level level, string user, string info, string description)
        {
            Log log = new SOSBody.Log()
            {
                Date = DateTime.Now,
                Level = level.ToString(),
                UserId = user,
                Info = info,
                Description = description
            };

            try
            {
                db.Log.Add(log);
                db.SaveChanges();
            }
            catch(Exception ex)
            {
                string str = ex.Message;
            }

            if(level == Level.Error || level == Level.Exception)
            {
                MailMessage message = new MailMessage("info@sosbody.cz", "v.jirkovsky@soska.tech", "SOS Body ERROR ", date.ToString() + " " + info + " " + description);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.zoner.com");
                client.Send(message);
            }
        }
    }
}