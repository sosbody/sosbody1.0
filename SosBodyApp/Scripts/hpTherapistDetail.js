﻿SOSbody.therapistDetail = {
    mapCanvasSelector: '#googleMap',
    panelSelector: '#therapist-detail',
    render: function (item) {
        var panel = $(this.panelSelector);
        
        $(this.mapCanvasSelector).addClass('stretched');
        panel.fadeIn(300);

        var img = panel.find('.portrait');
        if (item.ProfileImage) {
            img.attr('src', item.ProfileImage);
        } else {
            img.attr('src', '/Images/no_profile_photo.png');
        }

        var title = "";
        var titlebehind = "";
        if (item.Title)
            title = item.Title;
        if (item.TitleBehind)
            titlebehind = item.TitleBehind;

        var fullAddress = item.Address.toString().replace('\n', '<br>');
        if (item.Street) {
            fullAddress = item.Street + ' ' + item.StreetNo + '<br>' + item.City;
        }

        var newValues = {
            name: (title + ' ' + item.Name + ' ' + item.LastName + ' ' + titlebehind + ' ' + '<a href="/Home/MedicProfile?userId=' + item.UserId + '"><img alt="info" src="images/info.png" width="30" height="30" title="info" /></a>').toString().trim(),
            address: fullAddress,
            price: item.Price,
            rating: 83, // temp
            profile: item.UserId,
            duration: item.Duration
        };

        var $name = panel.find('[data-attr=name]');
        var $address = panel.find('[data-attr=address]');
        var $rating = panel.find('.rating');
        var $header = panel.find('[data-attr=header]');
        $('input[id=userId]').val(item.UserId);

        $name.html(newValues.name);
        $address.html(newValues.address);
        $rating.html(newValues.rating + '&nbsp;%');

        var $scheduleTable = $('.schedule');
        $scheduleTable.html('');

        var moreButton = $('#more-button');
        var orderButton = $('#order-button');
        if (item.ActionList && item.ActionList.length) {
            var i = 0;
            orderButton.show();
            item.ActionList.forEach(function (term) {
                i++;
                if (i <= 4)
                {
                    // prepare text
                    var mmt = moment(term.Date, 'YYYY-MM-DD HH:mm:ss');
                    console.log(term);
                    mmt.locale('cs');
                    // render row
                    var termRow = $('<tr></tr>').attr('data-id', term.Id);
                    termRow.append($('<th></th>').html(mmt.format('dddd') + ' <i>(' + mmt.format('D. M.') + ')</i>'));
                    termRow.append($('<td></td>').html(mmt.format('k:mm')));
                    termRow.append($('<td></td>').html(term.Price + '&nbsp;Kč' + '&nbsp;(' + term.Duration + '&nbsp;min)'));
                    $scheduleTable.append(termRow);
                    if (i === 1) {
                        SOSbody.therapistDetail.selectTerm(term.Id);
                    }
                }
            });

            moreButton.attr(
            'href',
            moreButton.data('href-pattern') + newValues.profile.toString()
            );
            moreButton.show();

            $header.html('Nejbližší volné termíny');
        } else {
            orderButton.hide();
            moreButton.hide();
            $header.html('Nejsou vypsané žádné volné termíny');
        }

        $('.schedule tr').unbind('click');
        $('.schedule tr').on('click', function () {
            var id = $(this).data('id');
            SOSbody.therapistDetail.selectTerm(id);
        });
        
        // highlight results panel on select
        $('#therapists-list .active').removeClass('active');
        $('[data-therapist-id="' + item.OfficeId + '"]').find('div').addClass('active');
    },
    selectTerm: function (id) {
        var panel = $(this.panelSelector);
        /**
        var term;
        SOSbody.mapResults.forEach(function (row) {
            if (row.UserId === id) {
                term = row;
            }
        });
        if (!row) {
            return;
        }*/

        panel.find('.schedule tr').removeClass('active');
        panel.find('.schedule tr[data-id=' + id + ']').addClass('active');

        var orderButton = panel.find('#order-button');
        orderButton.attr(
            'href',
            orderButton.data('href-pattern') + id.toString()
        );
    },
    insertWatchdogEmail: function () {
        var email = $('#watchdog-email').val();
        var userId = $('#userId').val();
        
        $.ajax({
            type: "POST",
            url: "/Home/WatchdogEmail",
            async: false,
            data: ({ email: email, userId: userId }),
            success: function (result) {
                if (result.Result == "")
                {
                    $('#watchdog-email').val("");
                    alert("E-mail byl úspěšně vložen");
                }
                else
                {
                    alert(result.Result);
                    return;
                }
            },

        });
    },
    hide: function () {
        var $panel = $(this.panelSelector);
        $panel.hide();
        $(this.mapCanvasSelector).removeClass('stretched');
    }
};