﻿SOSbody.therapistList = {
    containerSelector: '#therapists-list > div',
    evenBoxHeights: function () {
        container = $(this.containerSelector);
        var itemBoxes = container.find('.item');
        itemBoxes.css('min-height', 0);

        var calculatedHeight = 10;
        itemBoxes.each(function (id, item) {
            calculatedHeight = Math.max(calculatedHeight, $(this).height());
        });
        itemBoxes.css('min-height', calculatedHeight);
    },
    render: function (data) {
        container = $(this.containerSelector);
        container.find('.item').remove();

        if (!data || !data.length) {
            container.parent().hide();
            return;
        }
        data.forEach(function (item) {
            console.log(item);
            
            if (item.ActionList != null && item.ActionList.length > 0)
            {
                var itemHtml = $('<div class="w3-margin"></div>');

                if (item.ProfileImage) {
                    var icon = $('<img>').attr('src', item.ProfileImage).addClass('portrait');
                } else {
                    var icon = $('<img>').attr('src', '/Images/no_profile_photo.png').addClass('portrait');
                }
                itemHtml.append(icon);

                if (item.LastName) {
                    var title = "";
                    var titlebehind = "";
                    if (item.Title)
                        title = item.Title;
                    if (item.TitleBehind)
                        titlebehind = item.TitleBehind;

                    var headline = $('<strong></strong>').html(title + ' ' + item.Name + ' ' + item.LastName + ' ' + titlebehind);
                    itemHtml.append(headline);
                } else if (!item.Name && !item.LastName) {
                    var headline = $('<strong>(neuvedeno)</strong><br>');
                    itemHtml.append(headline);
                }

                if (item.Address) {
                    var address = $('<address></address>');
                    var addressContent =
                        item.Street.toString().trim() + ' '
                        + item.StreetNo.toString().trim() + '<br>'
                        + item.City.toString().trim()
                    ;
                    address.html(addressContent)
                    itemHtml.append(address);
                }
                /*
                if (item.OpenningHours) {
                    var openingHrs = $('<span></span>').addClass('opening-hrs');
                    openingHrs.html('Otevírací doba: ' + item.OpenningHours);
                    itemHtml.append(openingHrs);
                }
                */
                if (item.Distance) {
                    var dist = $('<span></span>').addClass('distance');
                    var sanitizedDistance = Math.round(parseInt(item.Distance * 100)) / 100;
                    sanitizedDistance = sanitizedDistance.toString().replace('.', ',')
                    dist.html('Vzdálenost: ');
                    dist.append(
                        $('<i>').html(sanitizedDistance + ' km')
                    );
                    itemHtml.append(dist);
                    itemHtml.append('<br />');
                }
                //else {
                //    var dist = $('<span>Vzdálenost: -</span>').addClass('distance');
                //    itemHtml.append(dist);
                //}

                if (item.ActionList && item.ActionList.length) {
                    var dist = $('<span></span>').addClass('distance');
                    dist.html('Nejbližší volný termín: ');
                    var mmt = moment(item.ActionList[0].Date, 'YYYY-MM-DD HH:mm:ss');
                    mmt.locale('cs');
                    dist.append(
                        $('<i>').html(mmt.format('D. M. - k:mm'))
                        );
                    dist.append('<a href="/home/order/?id=' + item.ActionList[0].Id + '" class="button button_no_hover button_no_border" style="width:100%;">OBJEDNAT</a>')
                    dist.append('Další termíny...');
                    itemHtml.append(dist);
                }

                var boxHtml = $('<div class="item w3-col m6 l4 s12"></div>')
                    .html(itemHtml)
                    .attr('data-therapist-id', item.OfficeId)
                    .on('click', function () {
                        SOSbody.therapistDetail.render(item);
                    })
                ;
                container.append(boxHtml);
            }
        });
        
        this.evenBoxHeights();
    }
};

window.addEventListener('resize', SOSbody.therapistList.evenBoxHeights);