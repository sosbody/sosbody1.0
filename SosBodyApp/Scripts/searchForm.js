﻿SOSbody.searchForm = {
    
    adjust: function () {
        var winScrollTop = $(window).scrollTop();
        var threshold = 500;// todo - determine dynamically
        var form = $('.sticky-search-form');
        
        if (winScrollTop > threshold) {
            form.addClass('on');
            //alert($("#Address").text());
            //var e = document.getElementById('sticky-search-form--address');
            //var e2 = document.getElementById('Address');
            //var text = e2.innerText || e2.textContent;
            //alert(text);
        } else {
            form.removeClass('on');
            //alert($("#Address").text());
        }
    },
    showSpinner: function () {
        $('.search-spinner-overlay').show();
    }
};

window.addEventListener('scroll', SOSbody.searchForm.adjust);
$(document).ready(function () {
    var form = $('.hp-search-form');
    form.on('submit', function () {
        SOSbody.searchForm.showSpinner();
    });
});