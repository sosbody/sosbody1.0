﻿SOSbody.mapResults = [];
SOSbody.mapPosition = {};
SOSbody.mapMarkers = [];

SOSbody.setMapData = function (rawData) {
    if (!rawData || !rawData.Results) {
        return;
    }

    SOSbody.mapPosition = null;
    SOSbody.mapResults = [];
    rawData.Results.forEach(function (item) {
        if (item.UserId) {
            SOSbody.mapResults.push(item);
        } else {
            SOSbody.mapPosition = item;
        }
    });
    
    //SOSbody.mapPosition.diameter = rawData.Distance;
};

SOSbody.setupMap = function () {
    var zoom = 8; // default zoom
   
    // 1. use searched location
    if (this.mapPosition && this.mapPosition.Latitude && this.mapPosition.Longitude) {
        // set zoom by search diameter
        if (this.mapPosition.diameter && this.mapPosition.diameter <= 15) {
            zoom = 12;
        } else if (this.mapPosition.diameter && this.mapPosition.diameter <= 30) {
            zoom = 11;
        } else {
            zoom = 10;
        }

        return SOSbody.myMap(this.mapPosition.Latitude, this.mapPosition.Longitude, zoom);
    }
    else {
        // map centering
        // CZ center
        var defaultLocation = {
            lat: 49.7612062,
            lng: 15.763758
        };
        zoom = 7;
        SOSbody.myMap(defaultLocation.lat, defaultLocation.lng, zoom);
        //SOSbody.myMap(defaultLocation.lat, defaultLocation.lng, zoom, function () {
        //    // 2. use browser geolocation, if possible + enabled
        //    navigator.geolocation.getCurrentPosition(function (position) {
        //        if (!SOSbody.mapPosition || !SOSbody.mapPosition.length) {
        //            SOSbody.mapPosition = {
        //                Latitude: position.coords.latitude,
        //                Longitude: position.coords.longitude,
        //                Description: 'Vaše poloha'
        //            };
        //        }
        //        SOSbody.myMap(position.coords.latitude, position.coords.longitude, zoom);
        //    }, function (err) {
        //        // user rejected HTML5 geolocation, keep the map as it is
        //    });
        //});
    }
};

SOSbody.enableSearchAutocomplete = function () {
    google.maps.event.addDomListener(window, 'load', function () {
        var options = {
            types: ['geocode'],
            componentRestrictions: { country: ["cz", "sk"] }
        };

        // main search form
        var input = document.getElementById('Address');
        var places = new google.maps.places.Autocomplete(input, options);

        // floating search form
        var input = document.getElementById('sticky-search-form--address');
        var places = new google.maps.places.Autocomplete(input, options);
    });
};

SOSbody.myMap = function (lat, lng, zoom, callback) {
    myCenter = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        center: myCenter,
        zoom: zoom,
        scrollwheel: false,
        draggable: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        fullscreenControl: false,
        streetViewControl: false
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

    var infoWindowList = [];

    // render my position pin
    var myPosition = SOSbody.mapPosition;
    var scrollToMap = false;
    if (myPosition) {
        var searchedPositionMarker = new google.maps.Marker({
            position: new google.maps.LatLng(myPosition.Latitude, myPosition.Longitude),
            id: index,
            map: map,
            title: myPosition.Description
        });
        searchedPositionMarker.setMap(map);
        searchedPositionMarker.setIcon('/Images/map_symbol_35x45-red.gif');
        scrollToMap = true;
    }

    // render search results
    if (this.mapResults != null && this.mapResults.length > 0) {
        var index = 0;
        this.mapResults.forEach(function (item) {

            //if (!item.UserId) {
            //    continue;
            //}

            var it = { "Id": index, "PlaceName": item.Description, "GeoLong": item.Longitude, "GeoLat": item.Latitude };
            /*
            if (index > 0) {
                var contentString = '<div id="content">' +
                    '<input type="hidden" id="UserId" value="' + item.UserId + '">' +
                    '<div id="wrapper">' +

                    '<div id="first">' +
                    '<img class="img-circle" src="' + item.ProfileImage + '" width="100" height="100">' +
                    '<button type="submit" class="button" style="margin-top:30px;" onclick="detailClick()">DETAIL</button>' +
                    '</div>' +
                    '<div id="second">' +
                    '<p style="color:#4B875C;">' +
                    item.Name +
                    '<br />' +
                    item.LastName +
                    '</p>' +
                    '<p style="font-size:small;">' +
                    item.Address +
                    '</p><br />' +
                    '<p style="font-size:small;">Nejbližší volný termín: Cena<br />' +
                    item.OpenningHours +
                    '</p>' +
                    //'<p style="font-size:small;font-weight:bolder;color:#4B875C;"><br />'+
                    //item.Phone +
                    //'<br /><font style="font-weight:normal;color:#000;">'+
                    //item.Email +
                    //'</font></p>'+
                    '</div>' +

                    '</div>'
                '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });
                infoWindowList.push(infowindow);
            }
            */
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(it.GeoLat, it.GeoLong),
                id: index,
                map: map,
                title: item.Description
            });
            marker.setMap(map);
            marker.setIcon('/Images/map_symbol_35x45.gif');
            SOSbody.mapMarkers.push(marker);

            marker.addListener('click', function () {
                SOSbody.therapistDetail.render(item);
            });

            if (index > 0) {
                marker.addListener('click', function () {
                    for (var i = 0; i < infoWindowList.length; i++) {
                        var win = infoWindowList[i];
                        win.close(map, marker);
                    }
                    SOSbody.mapMarkers.forEach(function (mrkr) {
                        mrkr.setIcon('/Images/map_symbol_35x45.gif');
                    });
                    //marker.setIcon("/Images/map_symbol_35x45-red.gif");
                    //infowindow.open(map, marker);
                    $('.myDetail').hide();
                });

                marker.addListener('mouseout', function () {
                    //infowindow.close(map, marker);
                });


                marker.setIcon('/Images/map_symbol_35x45.gif');
            }
            index++;

        });

        scrollToMap = true;
    }

    if (scrollToMap) {
        // SOSbody.anchors.go('googleMap', 200);
    }

    if (callback) {
        callback();
    }
};