﻿SOSbody.anchors = {
    go: function (id, verticalMargin) {
        verticalMargin = verticalMargin || 0;
        var elem = $('#' + id.replace('#', ''));
        if (!elem.length) {
            console.warn('Scrolling target not found by id: ' + id);
            return;
        }
        var topPos = elem.offset().top;
        $('body, html').animate({
            scrollTop: topPos - verticalMargin
        }, 400);
    }
}