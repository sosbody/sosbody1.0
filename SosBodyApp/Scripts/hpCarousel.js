﻿SOSbody.hpCarousel = {
    init: function () {
        $('#hp-slick-wrapper').slick({
            slidesToShow: 4,
            infinite: true,
            prevArrow: $('.carousel-prev'),
            nextArrow: $('.carousel-next'),
            responsive: [
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 1150,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ],
            autoplay: true,
            speed: 300
        });
    },
    bindSelectAction: function (carouselData) {
        
        $('.therapist-carousel-item').on('click', function () {
            var itemId = $(this).data('user-id');

            var selectedItem = null;
            for (var i in carouselData) {

                if (carouselData[i].UserId.trim() === itemId) {
                    selectedItem = carouselData[i];
                    break;
                }
            }

            SOSbody.therapistDetail.render(selectedItem);
        });
    }
};

$(document).ready(function () {
    SOSbody.hpCarousel.init();

    if (typeof (hpCarouselItems) !== 'undefined' && hpCarouselItems) {
        SOSbody.hpCarousel.bindSelectAction(hpCarouselItems);
    }
});
