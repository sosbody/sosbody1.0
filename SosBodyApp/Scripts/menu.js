﻿$(document).ready(function () {
    var mobileMenu = $('#navDemo');

    var refreshBurgerIcon = function () {
        var burgerIcon = $('#menu_clicked i.fa');
        if (mobileMenu.hasClass('w3-show')) {
            burgerIcon.removeClass('fa-bars').addClass('fa-times');
        } else {
            burgerIcon.addClass('fa-bars').removeClass('fa-times');
        }
    }

    $('#menu_clicked').on('click', function () {
        mobileMenu.toggleClass('w3-show');
        refreshBurgerIcon();

        // close search bar (so that it doesn't conflict with mobile menu)
        $('.sticky-search-form').removeClass('on');
        $('#menu_search i').removeClass('fa-times').addClass('fa-search');
    });
    $(document).on('click', function (e) {
        var isMenuClicked = $(e.target).closest('#mymenu-large, #navDemo').length > 0;
        if (!isMenuClicked && mobileMenu.hasClass('w3-show')) {
            mobileMenu.removeClass('w3-show');
            refreshBurgerIcon();
        }
    });

    // search bar
    $('#menu_search').on('click', function () {
        var target = $('.sticky-search-form');
        target.toggleClass('on');

        var icon = $(this).find('i');
        if (target.hasClass('on')) {
            icon.addClass('fa-times').removeClass('fa-search');

            // close mobile menu (so that it doesn't conflict with search bar)
            mobileMenu.removeClass('w3-show');
            refreshBurgerIcon();
        } else {
            icon.removeClass('fa-times').addClass('fa-search');
        }
    });
});