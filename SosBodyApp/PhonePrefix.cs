//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SOSBody
{
    using System;
    using System.Collections.Generic;
    
    public partial class PhonePrefix
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string CountryShort { get; set; }
        public string Country { get; set; }
        public short Length { get; set; }
        public bool Default { get; set; }
    }
}
