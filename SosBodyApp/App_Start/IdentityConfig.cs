﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SosBody.Infrastructure;

namespace SosBody//.App_Start
{
    public class IdentityConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<AppIdentityDbContext>(AppIdentityDbContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.CreatePerOwinContext<AppRoleManager>(AppRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                ExpireTimeSpan = System.TimeSpan.FromMinutes(480),
                SlidingExpiration = true,
                CookieSecure = CookieSecureOption.Always,
                Provider = new CookieAuthenticationProvider {
                    OnResponseSignIn = context => {
                        context.Properties.AllowRefresh = true;
                        context.Properties.ExpiresUtc = System.DateTimeOffset.UtcNow.AddMinutes(480);
                    } }
                //Provider = new CookieAuthenticationProvider
                //{
                //    OnValidateIdentity = Microsoft.AspNet.Identity.Owin.SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUserManager>(
                //    validateInterval: System.TimeSpan.FromMinutes(480),
                //    regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                //}
            });

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, System.TimeSpan.FromHours(8));

            //https://www.c-sharpcorner.com/article/facebook-authentication-in-Asp-Net-mvc-5-0-part-fifteen/
            //app.UseFacebookAuthentication(
            //   appId: "1733554893354928",
            //   appSecret: "bae977b6ec94a13eeb8ca080d8b26340");

            ////app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            ////{
            ////     ClientId = "248882027328-477sa4fs2l45720f8slbuk0q5vtr0u53.apps.googleusercontent.com",
            ////     ClientSecret = "eLQfi0scc1OLeMPJzMmsBefE"
            ////});


            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "908832972298-nbnhgjrplkp2shnnlm8tvlar48v4kq2g.apps.googleusercontent.com",
            //    ClientSecret = "kTjpTfbJ_s1x6BQHIBEmgOiP"
            //});
            //app.UseGoogleAuthentication(
            //     clientId: "248882027328-gqc1ku1dqtl6pe7fqga07sr97uadb5mp.apps.googleusercontent.com",
            //     clientSecret: "M6p7eBYffuS2D4yWtgE8IOsD");
        }
    }
}