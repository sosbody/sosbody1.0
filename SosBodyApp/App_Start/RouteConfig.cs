﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SosBody
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Find",
            //    url: "{controller}/{action}",
            //    defaults: new { controller = "Home", action = "Find" }
            //);

            //routes.MapRoute(
            //    name: "Register",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Admin", action = "Register", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "Create",
            //    url: "{controller}/{action}",
            //    defaults: new { controller = "Admin", action = "Create" }
            //);
        }
    }
}