﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SosBody.Models;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using SosBody.Infrastructure;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using SOSBody.Helpers;
using System.IO;

namespace SOSBody.Controllers
{
    [Authorize(Roles = "SOSBodyAdmin")]
    public class DashboardController : Controller
    {
        IdentityDbEntities db = new IdentityDbEntities();
        private static Logger logger = new Logger();
        private static PhoneHelper ph = new PhoneHelper();

        private AppRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }

        public ActionResult Index(string sortOrder)
        {
            ViewBag.Name = "name";
            ViewBag.LastName = "lastname";
            ViewBag.Personal = "personal_desc";
            ViewBag.Office = "office_desc";
            ViewBag.Invoice = "invoice_desc";
            ViewBag.ProfileImage = "image_desc";

            var allMedics = db.AspNetUsers.ToList();

            AllMedicsModel allMedicsModel = new AllMedicsModel()
            {
                Medics = new List<BasicMedicsModel>()
            };

            int completedRegs = 0;
            int incompletedRegs = 0;        

            AppRole role = RoleManager.FindByName("SOSBodyMedic");
            var users = role.Users.ToList();

            foreach (var entity in users)
            {
                var medicDetail = db.MedicDetails.Where(x => x.UserId == entity.UserId).FirstOrDefault();
                var historyDetail = db.DashboardHistory.ToList();
                DashboardHistory history = null;
                if(medicDetail != null)
                    history = historyDetail.Where(x => x.UserLogin == medicDetail.Email).LastOrDefault();

                BasicMedicsModel model = new BasicMedicsModel()
                {
                    UserId = entity.UserId,
                    Name = medicDetail?.Name,
                    LastName = medicDetail?.Lastname,
                    IsProfileImage = IsProfileImage(Globals.ProfileImagesPath, entity.UserId),
                    IsMedicPersonalData = IsMedicPersonalProfileCreated(entity.UserId),
                    IsMedicOfficeData = IsMedicOfficeCreated(entity.UserId),
                    IsMedicInviceData = IsMedicInvoiceDataCreated(entity.UserId),
                    FreeDates = GetFreeDatesCount(entity.UserId),
                    TotalDates = GetDatesTotalCount(entity.UserId),
                    TotalOrders = GetOrderedDatesTotalCount(entity.UserId),
                    Email = medicDetail?.Email,
                    Phone = ph.GetPhonePrefix(medicDetail?.Phone) + ph.GetPhonePure(medicDetail?.Phone),
                    LastHistoryPost = history?.Text //(history != null) ? history.Text.Substring(0, Math.Min(history.Text.Length, 100)) : "",
                };

                if (model.IsMedicInviceData && model.IsMedicOfficeData && model.IsMedicPersonalData)
                    completedRegs++;
                else
                    incompletedRegs++;

                allMedicsModel.Medics.Add(model);
            }

            allMedicsModel.CompletedRegistrations = completedRegs;
            allMedicsModel.IncompletedRegistrations = incompletedRegs;

            switch (sortOrder)
            {
                case "name":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.Name).ToList();
                    ViewBag.Name = "name_desc";
                    break;
                case "name_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.Name).ToList();
                    ViewBag.Name = "name";
                    break;
                case "lastname":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.LastName).ToList();
                    ViewBag.LastName = "lastname_desc";
                    break;
                case "lastname_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.LastName).ToList();
                    ViewBag.LastName = "lastname";
                    break;
                case "personal_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.IsMedicPersonalData).ToList();
                    ViewBag.Personal = "personal";
                    break;
                case "personal":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.IsMedicPersonalData).ToList();
                    ViewBag.Personal = "personal_desc";
                    break;
                case "office_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.IsMedicOfficeData).ToList();
                    ViewBag.Office = "office";
                    break;
                case "office":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.IsMedicOfficeData).ToList();
                    ViewBag.Office = "office_desc";
                    break;
                case "invoice_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.IsMedicInviceData).ToList();
                    ViewBag.Invoice = "invoice";
                    break;
                case "invoice":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.IsMedicInviceData).ToList();
                    ViewBag.Invoice = "invoice_desc";
                    break;
                case "image_desc":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderByDescending(s => s.IsProfileImage).ToList();
                    ViewBag.ProfileImage = "image";
                    break;
                case "image":
                    allMedicsModel.Medics = allMedicsModel.Medics.OrderBy(s => s.IsProfileImage).ToList();
                    ViewBag.ProfileImage = "image_desc";
                    break;
                default:
                    break;
            }

            return View(allMedicsModel);
        }

        private bool IsMedicPersonalProfileCreated(string userId)
        {
            var medicDetail = db.MedicDetails.Where(x => x.UserId == userId);
            if (medicDetail == null || medicDetail.Count() == 0)
                return false;

            return true;
        }

        private bool IsMedicOfficeCreated(string userId)
        {
            var officeDetail = db.OfficeDetails.Where(x => x.UserId == userId);
            if (officeDetail == null || officeDetail.Count() == 0)
                return false;

            return true;
        }

        private bool IsMedicInvoiceDataCreated(string userId)
        {
            var invoiceDetail = db.MedicInvoiceDetails.Where(x => x.UserId == userId);
            if (invoiceDetail == null || invoiceDetail.Count() == 0)
                return false;

            return true;
        }

        private bool IsProfileImage(string dir, string UserId)
        {
            string directory = Server.MapPath(dir);
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                int lastSlashPos = fileName.LastIndexOf('\\');
                string name = fileName.Substring(lastSlashPos + 1);
                int lastComaPos = name.LastIndexOf('.');
                string subName = name.Substring(0, lastComaPos);

                if (subName == UserId)
                    return true;
            }

            return false;
        }

        protected int GetFreeDatesCount(string userId)
        {
            var dates = db.Calendar.Where(x => x.UserId == userId && x.Date >= DateTime.Now);
            if (dates == null)
                return 0;

            return dates.ToList().Count;
        }

        protected int GetDatesTotalCount(string userId)
        {
            return db.Calendar.Where(x => x.UserId == userId).ToList().Count;
        }

        protected int GetOrderedDatesTotalCount(string userId)
        {
            var orders = db.Orders.Where(x => x.MedicId == userId).ToList();
            int totalCount = 0;
            foreach(Order order in orders)
            {
                var payment = db.Payments.Where(x => x.OrderId == order.Id && x.Status == "PAID").FirstOrDefault();
                if (payment != null)
                    totalCount++;
            }

            return totalCount;
        }

        public ActionResult MedicDetail(string userId)
        {
            MedicDetailModel model = new MedicDetailModel()
            {
                Offices = new List<DashboardOfficeDetailModel>()
            };

            var medicDetail = db.MedicDetails.Where(x => x.UserId == userId).FirstOrDefault();

            model.Name = medicDetail?.Title + " " + medicDetail?.Name + " " + medicDetail?.Lastname + " " + medicDetail?.TitleBehind;
            model.Email = medicDetail?.Email;
            model.Phone = medicDetail?.Phone.Replace('-', ' ');
            model.Spec1 = medicDetail == null ? false : medicDetail.Spec1;
            model.Spec2 = medicDetail == null ? false : medicDetail.Spec2;
            model.Spec3 = medicDetail == null ? false : medicDetail.Spec3;
            model.Spec4 = medicDetail == null ? false : medicDetail.Spec4;
            model.Spec5 = medicDetail == null ? false : medicDetail.Spec5;
            model.Spec6 = medicDetail == null ? false : medicDetail.Spec6;

            var invoiceDetail = db.MedicInvoiceDetails.Where(x => x.UserId == userId).FirstOrDefault();

            model.IdentifyNo = invoiceDetail?.IdentifyNo;
            model.TaxIdentifyNo = invoiceDetail?.TaxIdentifyNo;
            model.BankAccount = (invoiceDetail == null || invoiceDetail.BankAccount == null) ? "" : invoiceDetail.BankAccount;
            model.InvoiceAddress = (invoiceDetail == null || invoiceDetail.Address == null) ? "" : invoiceDetail.Address;
            model.InvoiceEmail = (invoiceDetail == null || invoiceDetail.Email == null)? "" : invoiceDetail.Email;
            model.InvoicePhone = (invoiceDetail == null || invoiceDetail.Phone == null) ? "" : invoiceDetail.Phone.Replace('-', ' ');

            var offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
            foreach (OfficeDetail office in offices)
            {
                DashboardOfficeDetailModel officeModel = new DashboardOfficeDetailModel()
                {
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    City = office.City,
                    Country = office.Country,
                    PostalCode = office.PostalCode,
                    Name = office.Name,
                    Phone = office.Phone != null ? office.Phone.Replace('-', ' ') : "",
                    Price30 = 0,
                    Price60 = 0,
                    Price90 = 0
                };           

                var calendarSettings = db.CalendarSettings.Where(x => x.OfficeId == office.Id).FirstOrDefault();
                if(calendarSettings != null)
                {
                    officeModel.Price30 = calendarSettings.Price30;
                    officeModel.Price60 = calendarSettings.Price60;
                    officeModel.Price90 = calendarSettings.Price90;
                }

                model.Offices.Add(officeModel);
            }

            model.History = new List<ContactHistory>();

            var contactHistory = db.DashboardHistory.Where(x => x.UserLogin == model.Email).ToList();
            foreach (DashboardHistory item in contactHistory)
            {
                ContactHistory contactModel = new ContactHistory()
                {
                    Date = item.Date.ToString("dd. MM. yyyy"),
                    UserLogin = item.UserLogin,
                    Text = item.Text
                };

                model.History.Add(contactModel);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult MedicDetail(MedicDetailModel model)
        {
            var user = db.AspNetUsers.Where(x => x.Id == model.UserId).FirstOrDefault();

            if (String.IsNullOrEmpty(model.NewText) == false)
            {
                DashboardHistory history = new DashboardHistory()
                {
                    Date = DateTime.Now,
                    Text = model.NewText,
                    UserLogin = model.Email
                };

                db.DashboardHistory.Add(history);
                db.SaveChanges();
            }

            model.Offices = new List<DashboardOfficeDetailModel>();
            var medicDetail = db.MedicDetails.Where(x => x.UserId == model.UserId).FirstOrDefault();

            model.Name = medicDetail?.Title + " " + medicDetail?.Name + " " + medicDetail?.Lastname + " " + medicDetail?.TitleBehind;
            model.Email = medicDetail?.Email;
            model.Phone = medicDetail?.Phone.Replace('-', ' ');
            model.Spec1 = medicDetail == null ? false : medicDetail.Spec1;
            model.Spec2 = medicDetail == null ? false : medicDetail.Spec2;
            model.Spec3 = medicDetail == null ? false : medicDetail.Spec3;
            model.Spec4 = medicDetail == null ? false : medicDetail.Spec4;
            model.Spec5 = medicDetail == null ? false : medicDetail.Spec5;
            model.Spec6 = medicDetail == null ? false : medicDetail.Spec6;

            var invoiceDetail = db.MedicInvoiceDetails.Where(x => x.UserId == model.UserId).FirstOrDefault();

            model.IdentifyNo = invoiceDetail?.IdentifyNo;
            model.TaxIdentifyNo = invoiceDetail?.TaxIdentifyNo;
            model.BankAccount = (invoiceDetail == null || invoiceDetail.BankAccount == null) ? "" : invoiceDetail.BankAccount;
            model.InvoiceAddress = (invoiceDetail == null || invoiceDetail.Address == null) ? "" : invoiceDetail.Address;
            model.InvoiceEmail = (invoiceDetail == null || invoiceDetail.Email == null) ? "" : invoiceDetail.Email;
            model.InvoicePhone = (invoiceDetail == null || invoiceDetail.Phone == null) ? "" : invoiceDetail.Phone.Replace('-', ' ');

            var offices = db.OfficeDetails.Where(x => x.UserId == model.UserId).ToList();
            foreach (OfficeDetail office in offices)
            {
                DashboardOfficeDetailModel officeModel = new DashboardOfficeDetailModel()
                {
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    City = office.City,
                    Country = office.Country,
                    PostalCode = office.PostalCode,
                    Name = office.Name,
                    Phone = office.Phone != null ? office.Phone.Replace('-', ' ') : "",
                    Price30 = 0,
                    Price60 = 0,
                    Price90 = 0
                };

                var calendarSettings = db.CalendarSettings.Where(x => x.OfficeId == office.Id).FirstOrDefault();
                if (calendarSettings != null)
                {
                    officeModel.Price30 = calendarSettings.Price30;
                    officeModel.Price60 = calendarSettings.Price60;
                    officeModel.Price90 = calendarSettings.Price90;
                }

                model.Offices.Add(officeModel);
            }

            model.NewText = "";
            model.History = new List<ContactHistory>();

            var contactHistory = db.DashboardHistory.Where(x => x.UserLogin == user.UserName).ToList();
            foreach (DashboardHistory item in contactHistory)
            {
                ContactHistory contactModel = new ContactHistory()
                {
                    Date = item.Date.ToString("dd. MM. yyyy"),
                    UserLogin = item.UserLogin,
                    Text = item.Text
                };

                model.History.Add(contactModel);
            }

            ModelState.Clear();

            return View(model);
        }

        public ActionResult EmailDetail(string email)
        {
            var medicEmail = db.MedicEmails.Where(x => x.Email == email).FirstOrDefault();

            if(medicEmail == null)
            {
                logger.Log(DateTime.Now, Logger.Level.Exception, "SOSBodyAdmin", "Email doesn't exist", email);
                return RedirectToAction("Index", "Dashboard");
            }

            EmailDetailModel model = new EmailDetailModel()
            {
                Email = medicEmail.Email,
                Inserted = medicEmail.InsertedDate.ToString("dd. MM. yyyy"),
                Invited = medicEmail.InvitedDate != null ? medicEmail.InvitedDate?.ToString("dd. MM. yyyy") : "",
                History = new List<ContactHistory>(),
                NewText = ""
            };

            var contactHistory = db.DashboardHistory.Where(x => x.UserLogin == email).ToList();
            foreach (DashboardHistory item in contactHistory)
            {
                ContactHistory contactModel = new ContactHistory()
                {
                    Date = item.Date.ToString("dd. MM. yyyy"),
                    UserLogin = item.UserLogin,
                    Text = item.Text
                };

                model.History.Add(contactModel);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EmailDetail(EmailDetailModel model)
        {
            if(String.IsNullOrEmpty(model.NewText) == false)
            {
                DashboardHistory history = new DashboardHistory()
                {
                    Date = DateTime.Now,
                    Text = model.NewText,
                    UserLogin = model.Email
                };

                db.DashboardHistory.Add(history);
                db.SaveChanges();
            }

            model.NewText = "";
            model.History = new List<ContactHistory>();

            var contactHistory = db.DashboardHistory.Where(x => x.UserLogin == model.Email).ToList();
            foreach(DashboardHistory item in contactHistory)
            {
                ContactHistory contactModel = new ContactHistory()
                {
                    Date = item.Date.ToString("dd. MM. yyyy"),
                    UserLogin = item.UserLogin,
                    Text = item.Text
                };

                model.History.Add(contactModel);
            }

            ModelState.Clear();

            return View(model);
        }

        public ActionResult Emails()
        {
            //SOSBody.Helpers.MailChimpHelper mailChimpHelper = new Helpers.MailChimpHelper();
            //mailChimpHelper.Init();
            //var champaigns = await mailChimpHelper.GetChampaigns();
            //var lists = await mailChimpHelper.GetLists();


            AllEmailsModel model = new AllEmailsModel()
            {
                Emails = new List<EmailModel>()
            };

            var emails = db.MedicEmails.ToList().OrderBy(x => x.InsertedDate);
            foreach(MedicEmails email in emails)
            {
                var user = db.AspNetUsers.Where(x => x.Email == email.Email).FirstOrDefault();

                EmailModel emailModel = new EmailModel()
                {
                    Email = email.Email,
                    Inserted = email.InsertedDate.ToString("dd. MM. yyyy"),
                    Invited = email.InvitedDate != null ? email.InvitedDate?.ToString("dd. MM. yyyy") : ""
                };

                if(user == null)
                    model.Emails.Add(emailModel);
            }

            return View(model);
        }

        public ActionResult Logs()
        {
            AllLogsModel model = new AllLogsModel();
            model.Logs = new List<LogModel>();

            var logs = db.Log.OrderByDescending(x => x.Date).Take(200).ToList();
            foreach(Log log in logs)
            {
                LogModel m = new LogModel()
                {
                    Date = log.Date.ToString("dd. MM. yyyy HH:mm"),
                    UserId = log.UserId,
                    Level = log.Level,
                    Info = log.Info,
                    Description = log.Description
                };

                model.Logs.Add(m);
            }

            return View(model);
        }

        public ActionResult Messages()
        {
            AllMessagesModel model = new AllMessagesModel();
            model.Messages = new List<MessageModel>();

            var messages = db.Message.OrderByDescending(x => x.Date).ToList();
            foreach (Message message in messages)
            {
                MessageModel m = new MessageModel()
                {
                    Date = message.Date.ToString("dd. MM. yyyy"),
                    Email = message.Email,
                    Text = message.Text,
                    Description = message.Description
                };

                model.Messages.Add(m);
            }

            return View(model);
        }
        
        public ActionResult PatientNewsletters()
        {
            AllPatientNewslettersModel model = new AllPatientNewslettersModel()
            {
                Emails = new List<string>()
            };

            var newsletters = db.PatientNewsletter.ToList();

            foreach(PatientNewsletter item in newsletters)
            {
                model.Emails.Add(item.Email);
            }

            return View(model);
        }

        public ActionResult Watchdogs()
        {
            AllWatchdogsModel model = new AllWatchdogsModel()
            {
                Watchdogs = new List<WatchdogModel>()
            };

            var watchdogs = db.EmailWatchdog.ToList();

            foreach(EmailWatchdog item in watchdogs)
            {
                var medicDetail = db.MedicDetails.Where(x => x.UserId == item.UserId).FirstOrDefault();
                if(medicDetail != null)
                {
                    WatchdogModel m = new WatchdogModel()
                    {
                        Email = item.Email,
                        Name = medicDetail.Name + " " + medicDetail.Lastname
                    };

                    model.Watchdogs.Add(m);
                }
            }

            return View(model);
        }
    }
}