﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using SosBody.Infrastructure;
using SosBody.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using SOSBody;
using SOSBody.Controllers;
using System.IO;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using SosBody.Helpers;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.Globalization;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using System.Threading;
using Google.Apis.Calendar.v3.Data;
using System.Net.Http;
using Newtonsoft.Json;
using Google.Apis.Services;
using SOSBody.Helpers;
using System.Xml.Linq;
using Google.Apis.Util.Store;
using iTextSharp.text.pdf;

namespace SosBody.Controllers
{
    public class AdminController : Controller
    {
        //protected string googleplus_client_id = "248882027328-gqc1ku1dqtl6pe7fqga07sr97uadb5mp.apps.googleusercontent.com";
        //protected string googleplus_client_secret = "M6p7eBYffuS2D4yWtgE8IOsD";

        protected string googleplus_client_id = "248882027328-477sa4fs2l45720f8slbuk0q5vtr0u53.apps.googleusercontent.com";    // Replace this with your Client ID
        protected string googleplus_client_secret = "eLQfi0scc1OLeMPJzMmsBefE";                                                // Replace this with your Client Secret
        protected string googleplus_redirect_url = "https://dev.sosbody.cz/Admin/Calendar/";//"http://localhost:51672/Admin/GoogleCalendar/";                                         // Replace this with your Redirect URL; Your Redirect URL from your developer.google application should match this URL.
        //protected string googleplus_redirect_url = "https://localhost:44397/Admin/GoogleCalendar/";
        //protected string googleplus_redirect_url = "http://test.sosbody.cz/Admin/GoogleCalendar/";                                         // Replace this with your Redirect URL; Your Redirect URL from your developer.google application should match this URL.
        protected string Parameters;
        public CalendarService service;

        IdentityDbEntities db = new IdentityDbEntities();
        private CalendarManager _calendar = new CalendarManager();

        private static Logger logger = new Logger();
        private static PhoneHelper ph = new PhoneHelper();
        private static Validators validator = new Validators();

        [Authorize]
        public ActionResult Index()
        {
            return View(UserManager.Users);
        }

        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private AppRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }
        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [HttpGet]
        public ActionResult Registered()
        {
            return View();
        }

        public ActionResult RegisterMedic()
        {
            var code = HttpContext.Request.Params.Get("code");

            if (TempData["RegisterEmail"] != null)
            {
                CreateModel model = new CreateModel()
                {
                    Email = TempData["RegisterEmail"].ToString(),
                    Accept = false
                };

                return View(model);
            }

            if (code == null)
            {
                CreateModel model = new CreateModel()
                {
                    Accept = false
                };
                return View(model);
            }
            else
            {
                DataEncryptor keys = new DataEncryptor();
                var invitations = db.MedicInvitations.Where(x => x.Hash == code).ToList();

                if (invitations == null || invitations.Count == 0)
                    return View();
                else if (invitations[0].Active && code == invitations[0].Hash)
                {
                    CreateModel model = new CreateModel()
                    {
                        Email = invitations[0].Email,
                        Accept = false
                    };
                    return View(model);
                }
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RegisterMedic(CreateModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Hesla nejsou shodná");
                    logger.Log(DateTime.Now, Logger.Level.Warning, "No user id", "Register medic - Hesla nejsou shodná", null);
                    return View(model);
                }

                AppUser user = new AppUser()
                {
                    UserName = model.Email,
                    Email = model.Email
                };

                AppRole role = GetMedicRole();

                IdentityResult userResult = await UserManager.CreateAsync(user, model.Password);

                if (userResult.Succeeded)
                {
                    IdentityResult roleResult = await UserManager.AddToRoleAsync(user.Id, role.Name);
                    if (roleResult.Succeeded)
                    {
                        //InsertMedicDetails(user.Id, model);
                        //int officeDetailId = CreateDefaultOfficeDetail(user.Id);
                        //CalendarSettings calSettings = CreateDefaultCalendarSettings(user.Id, officeDetailId);

                        //await SignInAsync(user, false);

                        var invitations = db.MedicInvitations.Where(x => x.Email == model.Email).ToList();
                        if (invitations != null && invitations.Count > 0)
                        {
                            invitations[0].Active = false;
                            db.SaveChanges();
                        }

                        UserStatus status = new UserStatus()
                        {
                            UserId = user.Id,
                            RegisterDate = DateTime.Now,
                            Status = "NEW"
                        };

                        db.UserStatus.Add(status);
                        db.SaveChanges();

                        SendActivationEmail(user.Id, user.Email);

                        //AuthManager.SignOut();

                        logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "Register medic - New registered medic", "Status - " + status.Status);

                        return Redirect("/Admin/Registered");
                    }
                    else
                    {
                        foreach (string error in roleResult.Errors)
                            ModelState.AddModelError("", error);
                    }
                }
                else
                {
                    foreach (string error in userResult.Errors)
                        ModelState.AddModelError("", error);
                }
            }

            return View(model);
        }

        [Authorize]
        public ActionResult RegisterMedicStep1()
        {
            string userId = User.Identity.GetUserId();

            var user = db.AspNetUsers.Where(x => x.Id == userId);

            var detail = db.MedicDetails.Where(x => x.UserId == userId);
            if (detail.ToList().Count == 0)
            {
                FormMedicModel model = new FormMedicModel()
                {
                    UserId = userId,
                    Offices = new List<OfficeDetailModel>(),
                    Email = user.ToList()[0].Email,
                    PhonePrefixList = ph.InitPhonePrefixList(""),
                    PhonePrefixLength = ph.GetPhonePrefixLength(""),
                    //MapDayItemTypes = InitDayDropDownList(),
                    //MapMonthItemTypes = InitMonthDropDownList(),
                    //MapYearItemTypes = InitYearDropDownList()
                };

                return View(model);
            }
            else
            {
                var det = detail.ToList()[0];
                FormMedicModel model = new FormMedicModel()
                {
                    Id = det.Id,
                    UserId = userId,
                    Name = det.Name,
                    Lastname = det.Lastname,
                    Phone = ph.GetPhonePure(det.Phone),
                    Email = det.Email,
                    Title = det.Title,
                    TitleBehind = det.TitleBehind,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    //MapYearItemTypes = InitYearDropDownList(),
                    //MapMonthItemTypes = InitMonthDropDownList(),
                    //MapDayItemTypes = InitDayDropDownList(),
                    Spec1 = det.Spec1,
                    Spec2 = det.Spec2,
                    Spec3 = det.Spec3,
                    Spec4 = det.Spec4,
                    Spec5 = det.Spec5,
                    Spec6 = det.Spec6,
                    PhonePrefix = ph.GetPhonePrefix(det.Phone),
                    PhonePrefixList = ph.InitPhonePrefixList(det.Phone),
                    PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone)
                };

                //if (det.DateOfBirth != null && det.DateOfBirth.HasValue)
                //{
                //    model.Year = det.DateOfBirth.Value.Year.ToString();
                //    model.Month = det.DateOfBirth.Value.Month.ToString();
                //    model.Day = det.DateOfBirth.Value.Day.ToString();
                //}

                return View(model);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult RegisterMedicStep1(FormMedicModel model)
        {
            var item = db.PhonePrefix.Where(x => x.Text == model.PhonePrefix).FirstOrDefault();
            int phoneLength = 9;
            if (item != null)
                phoneLength = item.Length;

            if (ModelState.IsValid && ph.GetPhonePure(model.Phone).Length == phoneLength)
            {
                var detail = db.MedicDetails.Where(x => x.Id == model.Id);
                if (detail == null || detail.ToList().Count == 0)
                {
                    MedicDetail det = new MedicDetail()
                    {
                        UserId = model.UserId,
                        Name = model.Name,
                        Lastname = model.Lastname,
                        Phone = model.PhonePrefix + "-" + model.Phone,
                        Email = model.Email,
                        Title = model.Title,
                        TitleBehind = model.TitleBehind,
                        //DateOfBirth = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), Convert.ToInt32(model.Day)),
                        Spec1 = model.Spec1,
                        Spec2 = model.Spec2,
                        Spec3 = model.Spec3,
                        Spec4 = model.Spec4,
                        Spec5 = model.Spec5,
                        Spec6 = model.Spec6
                    };
                    db.MedicDetails.Add(det);
                }
                else
                {
                    MedicDetail det = db.MedicDetails.Find(model.Id);
                    if (det != null)
                    {
                        det.Name = model.Name;
                        det.Lastname = model.Lastname;
                        det.Phone = model.PhonePrefix + "-" + model.Phone;
                        det.Email = model.Email;
                        det.Title = model.Title;
                        det.TitleBehind = model.TitleBehind;
                        //if (model.Year != null && model.Month != null && model.Day != null)
                        //    det.DateOfBirth = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), Convert.ToInt32(model.Day));
                        det.Spec1 = model.Spec1;
                        det.Spec2 = model.Spec2;
                        det.Spec3 = model.Spec3;
                        det.Spec4 = model.Spec4;
                        det.Spec5 = model.Spec5;
                        det.Spec6 = model.Spec6;
                    }
                }

                //http://stackoverflow.com/questions/11581147/entity-framework-exception-invalid-object-name
                try
                {
                    int res = db.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Log(DateTime.Now, Logger.Level.Exception, model.UserId, "Form medic - " + ex.Message, ex.InnerException.ToString());
                }
                return RedirectToAction("RegisterMedicStep2", "Admin");
            }
            else
            {
                if (ph.GetPhonePure(model.Phone).Length != phoneLength && ph.GetPhonePure(model.Phone).Length != 0)
                    model.ErrorMessage = "Délka telefonního čísla není platná";

                //model.MapYearItemTypes = InitYearDropDownList();
                //model.MapMonthItemTypes = InitMonthDropDownList();
                //model.MapDayItemTypes = InitDayDropDownList();
                //if (model.Year == null)
                //    model.Year = "  Rok  ";
                //if (model.Month == null)
                //    model.Month = "  Měsíc  ";
                //if (model.Day == null)
                //    model.Day = "  Den  ";
                model.UserId = model.UserId;
                model.Offices = new List<OfficeDetailModel>();
                model.ProfileImage = GetProfileImage(Globals.ProfileImagesPath);
                model.PhonePrefix = ph.GetPhonePrefix(model.Phone);
                model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                model.PhonePrefixLength = ph.GetPhonePrefixLength(model.Phone);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult RegisterMedicStep2()
        {
            string userId = User.Identity.GetUserId();

            var office = db.OfficeDetails.Where(x => x.UserId == userId);
            if (office == null || office.Count() == 0)
            {
                OfficeDetailModel model = new OfficeDetailModel()
                {
                    UserId = userId,
                    Name = "",
                    Street = "",
                    StreetNo = "",
                    PostalCode = "",
                    City = "",
                    Country = "Česká republika",
                    Latitude = "",
                    Longitude = "",
                    Phone = "",
                    PhonePrefixLength = ph.GetPhonePrefixLength(""),
                    PhonePrefixList = ph.InitPhonePrefixList("")
                };

                return View(model);
            }
            else
            {
                var det = office.ToList()[0];

                OfficeDetailModel model = new OfficeDetailModel()
                {
                    Id = det.Id,
                    UserId = det.UserId,
                    Name = det.Name,
                    Street = det.Street,
                    StreetNo = det.StreetNo,
                    PostalCode = det.PostalCode,
                    City = det.City,
                    Country = det.Country,
                    Latitude = det.Latitude,
                    Longitude = det.Longitude,
                    Phone = ph.GetPhonePure(det.Phone),
                    PhonePrefix = ph.GetPhonePrefix(det.Phone),
                    PhonePrefixList = ph.InitPhonePrefixList(det.Phone),
                    PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone)
                };

                return View(model);
            }       
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> RegisterMedicStep2(OfficeDetailModel model)
        {
            if (ModelState.IsValid)
            {
                var detail = db.OfficeDetails.Where(x => x.Id == model.Id);
                if (detail == null || detail.ToList().Count == 0)
                {
                    OfficeDetail det = new OfficeDetail()
                    {
                        UserId = model.UserId,
                        Name = model.Name,
                        Street = model.Street,
                        StreetNo = model.StreetNo,
                        Country = model.Country,
                        PostalCode = model.PostalCode,
                        City = model.City,
                        Latitude = model.Latitude,
                        Longitude = model.Longitude,
                        Phone = model.PhonePrefix + "-" + model.Phone
                    };
                    db.OfficeDetails.Add(det);
                    db.SaveChanges();


                    GeoPoint point = await GetPoint(model.Street + " " + model.StreetNo + " " + model.City + " " + model.PostalCode);
                    if (point != null && point.Latitude != null && point.Longitude != null)
                    {
                        CachedResults cachedRes = new CachedResults()
                        {
                            UserId = model.UserId,
                            OfficeId = det.Id,
                            Name = det.Name,
                            Street = det.Street,
                            StreetNo = det.StreetNo,
                            PostalCode = det.PostalCode,
                            City = det.City,
                            Country = det.Country,
                            Latitude = point.Latitude,
                            Longitude = point.Longitude
                        };

                        db.CachedResults.Add(cachedRes);
                        db.SaveChanges();
                    }
                    else
                        logger.Log(DateTime.Now, Logger.Level.Error, model.UserId, "CachedResults store failed", "");

                    var officeDetail = db.OfficeDetails.Where(x => x.UserId == model.UserId);
                    if(officeDetail != null && officeDetail.Count() > 0)
                        CreateDefaultCalendarSettings(model.UserId, officeDetail.ToList()[0].Id);
                }
                else
                {
                    OfficeDetail det = db.OfficeDetails.Find(model.Id);
                    if (det != null)
                    {
                        det.Id = model.Id;
                        det.UserId = model.UserId;
                        det.Name = model.Name;
                        det.Street = model.Street;
                        det.StreetNo = model.StreetNo;
                        det.Country = model.Country;
                        det.PostalCode = model.PostalCode;
                        det.City = model.City;
                        det.Latitude = model.Latitude;
                        det.Longitude = model.Longitude;
                        det.Phone = model.PhonePrefix + "-" + model.Phone;
                    }
                }

                try
                {
                    int res = db.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Log(DateTime.Now, Logger.Level.Exception, model.UserId, "Form medic - " + ex.Message, ex.InnerException.ToString());
                }
                return RedirectToAction("RegisterMedicStep3", "Admin");
            }
            else
            {
                model.PhonePrefix = ph.GetPhonePrefix(model.Phone);
                model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult RegisterMedicStep3()
        {
            string userId = User.Identity.GetUserId();

            var m = TempData["InvoiceData"] as InvoiceDetailModel;

            var detail = db.MedicInvoiceDetails.Where(x => x.UserId == userId);
            if (detail == null || detail.ToList().Count == 0)
            {
                var medicDetail = db.MedicDetails.Where(x => x.UserId == userId).FirstOrDefault();
                string email = "";
                string phone = "";
                if(medicDetail != null)
                {
                    email = medicDetail.Email;
                    phone = medicDetail.Phone;
                }

                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    UserId = userId,
                    Email = email,
                    Phone = ph.GetPhonePure(phone),
                    PhonePrefixLength = ph.GetPhonePrefixLength(phone),
                    PhonePrefixList = ph.InitPhonePrefixList(phone),
                    PhonePrefix = ph.GetPhonePrefix(phone),
                    Accept = false
                };

                if (m != null)
                {
                    model.Address = m.Address;
                    model.BankAccount = m.BankAccount;
                    model.Email = m.Email;
                    model.IdentifyNo = m.IdentifyNo;
                    model.Name = m.Name;
                    model.Phone = m.Phone;
                    model.TaxIdentifyNo = m.TaxIdentifyNo;
                    model.UserId = m.UserId;
                    model.ProfileImage = GetProfileImage(Globals.ProfileImagesPath);
                    model.Postfix = m.Postfix;
                    model.Prefix = m.Prefix;
                    model.PhonePrefix = m.PhonePrefix;
                }

                model.PostfixList = InitPostfixList();

                if (model.Postfix == null)
                    model.Postfix = "";               

                return View(model);
            }
            else
            {
                MedicInvoiceDetail det = detail.ToList()[0];
                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    Id = det.Id,
                    UserId = det.UserId,
                    Name = det.Name,
                    Address = det.Address,
                    Phone = ph.GetPhonePure(det.Phone),
                    Email = det.Email,
                    IdentifyNo = det.IdentifyNo,
                    TaxIdentifyNo = det.TaxIdentifyNo,
                    Prefix = GetPrefixFromBankAccount(det.BankAccount),
                    BankAccount = GetBankAccount(det.BankAccount),
                    Postfix = GetPostfixFromBankAccount(det.BankAccount),
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    PhonePrefix = ph.GetPhonePrefix(det.Phone),
                };

                if (m != null)
                {
                    model.Address = m.Address;
                    model.Prefix = GetPrefixFromBankAccount(m.BankAccount);
                    model.BankAccount = GetBankAccount(m.BankAccount);
                    model.Postfix = GetPostfixFromBankAccount(m.BankAccount);
                    model.BankAccount = m.BankAccount;
                    model.Email = m.Email;
                    model.IdentifyNo = m.IdentifyNo;
                    model.Name = m.Name;
                    model.Phone = m.Phone;
                    model.TaxIdentifyNo = m.TaxIdentifyNo;
                    model.UserId = m.UserId;
                }

                model.PhonePrefixList = ph.InitPhonePrefixList(det.Phone);
                model.PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone);
                model.PostfixList = InitPostfixList();

                return View(model);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult RegisterMedicStep3(InvoiceDetailModel model)
        {
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(model.Postfix) == false && String.IsNullOrEmpty(model.BankAccount) == true)
                {
                    model.ErrorMessage = "Bankovní účet není vyplněný";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }
                else if (String.IsNullOrEmpty(model.Postfix) == true && String.IsNullOrEmpty(model.BankAccount) == false)
                {
                    model.ErrorMessage = "Koncovka bankovního účtu není vyplněná";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }
                else if (String.IsNullOrEmpty(model.Prefix) == false && (String.IsNullOrEmpty(model.Postfix) == true || String.IsNullOrEmpty(model.BankAccount) == true))
                {
                    model.ErrorMessage = "Číslo bankovního účtu není kompletní";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }

                var item = db.PhonePrefix.Where(x => x.Text == model.PhonePrefix).FirstOrDefault();
                int phoneLength = 9;
                if (item != null)
                    phoneLength = item.Length;

                if (ph.GetPhonePure(model.Phone).Length != phoneLength && ph.GetPhonePure(model.Phone).Length != 0)
                {
                    model.ErrorMessage = "Délka telefonního čísla není platná";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);
                    return View(model);
                }

                if (validator.IsBankAccountValid(model.Prefix, model.BankAccount) == false)
                {
                    model.ErrorMessage = "Číslo bankovního účtu obsahuje chyby";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }

                string userId = User.Identity.GetUserId();

                var detail = db.MedicInvoiceDetails.Where(x => x.Id == model.Id);
                if (detail == null || detail.ToList().Count == 0)
                {
                    MedicInvoiceDetail det = new MedicInvoiceDetail()
                    {
                        Id = model.Id,
                        UserId = model.UserId,
                        Name = model.Name,
                        Address = model.Address,
                        IdentifyNo = model.IdentifyNo,
                        TaxIdentifyNo = model.TaxIdentifyNo,
                        Phone = String.IsNullOrEmpty(model.Phone) ? "" : model.PhonePrefix + "-" + model.Phone,
                        Email = model.Email,
                        BankAccount = (String.IsNullOrEmpty(model.Prefix) == false ? model.Prefix + "-" : "") + (String.IsNullOrEmpty(model.BankAccount) == true ? "" : model.BankAccount + "/" + model.Postfix)
                    };
                    db.MedicInvoiceDetails.Add(det);
                }
                else
                {
                    MedicInvoiceDetail det = db.MedicInvoiceDetails.Find(model.Id);
                    if (det != null)
                    {
                        det.UserId = model.UserId;
                        det.Name = model.Name;
                        det.Address = model.Address;
                        det.IdentifyNo = model.IdentifyNo;
                        det.TaxIdentifyNo = model.TaxIdentifyNo;
                        det.Phone = String.IsNullOrEmpty(model.Phone) ? "" : model.PhonePrefix + "-" + model.Phone;
                        det.Email = model.Email;
                        det.BankAccount = (String.IsNullOrEmpty(model.Prefix) == false ? model.Prefix + "-" : "") + (String.IsNullOrEmpty(model.BankAccount) == true ? "" : model.BankAccount + "/" + model.Postfix);
                    }
                }

                int res = db.SaveChanges();
                return RedirectToAction("HomeMedic", "Admin");
            }

            model.PostfixList = InitPostfixList();
            model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
            model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

            return View(model);
        }

        /// <summary>
        /// Email 001
        /// </summary>
        /// <param name="email"></param>
        private void SendPatientActivationEmail(string email)
        {
            string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Vážená paní, vážený pane,</strong><br /><br />
                        <strong>děkujeme za registraci a vítáme Vás na portálu SOS Body.</strong>

                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>
            ";
            MailMessage message = new MailMessage("info@sosbody.cz", email, "Potvrzení registrace SOS Body", body);
            message.IsBodyHtml = true;

            SmtpClient client = new SmtpClient("smtp.zoner.com");
            client.Send(message);
        }

        /// <summary>
        /// Email 002
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="email"></param>
        private void SendActivationEmail(string userId, string email)
        {
            string hash = "";
            using (SHA256 shaHash = SHA256.Create())
            {
                hash = GetSha256Hash(shaHash, email);
            }

            if (String.IsNullOrEmpty(hash))
            {
                logger.Log(DateTime.Now, Logger.Level.Error, email, "Send activation email - Hash failed", null);
                return;// ERROR
            }

            //MedicInvitation invitation = new MedicInvitation()
            //{
            //    UserId = email,
            //    Email = email,
            //    Hash = hash,
            //    Date = DateTime.Now,
            //    Active = true
            //};


            try
            {
                //https://tech.trailmax.info/2015/05/asp-net-identity-invalid-token-for-password-reset-or-email-confirmation/
                var code = UserManager.GenerateEmailConfirmationToken(userId);
                string codeStr = System.Web.HttpUtility.UrlEncode(code);
                //if (code != codeStr)
                //    logger.Log(DateTime.Now, Logger.Level.Error, userId, "Code string not equal to codeStr", "SendActivationEmail");
                logger.Log(DateTime.Now, Logger.Level.Trace, userId, "E-mail confirm token", codeStr);

                var callbackUrl = Url.Action("ConfirmPassword", "Account", new { userId = userId, code = codeStr }, protocol: Request.Url.Scheme);

                //UserManager.SendEmail(email, "Potvrzení hesla", "Heslo obnovíte kliknutím zde: <a href=\"" + callbackUrl + "\">link</a>");

                string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Vážená paní, vážený pane,</strong><br /><br />
                        <strong>velice Vám děkujeme za registraci. Potvrďte prosím svůj e-mail kliknutím na odkaz níže </strong><br /><br />";

                body += "<a href =\"" + callbackUrl + "\">" + callbackUrl + "</a>";
                body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>
            ";
                //string body = "Vážená paní, vážený pane,\n\n velice Vám děkujeme za registraci. Pro potvrzení e-mailu prosím klikněte ".Replace("\n", Environment.NewLine);
                ////body += "http://localhost:51672/Admin/RegisterMedic?code=";
                //body += "<a href =\"" + callbackUrl + "\">zde</a>";
                ////body += hash;
                //body += "\n\nVáš SOS Body team".Replace("\n", Environment.NewLine);
                MailMessage message = new MailMessage("info@sosbody.cz", email, "Potvrzení e-mailu SOS Body", body);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.zoner.com");
                client.Send(message);
            }
            catch(Exception ex)
            {
                logger.Log(DateTime.Now, Logger.Level.Error, userId, "Activation e-mail sent ERROR", ex.Message);
            }

            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Activation e-mail sent", null);
        }

        public ActionResult DeleteProfileError()
        {
            return View();
        }

        public ActionResult DeletePatientProfileError()
        {
            return View();
        }

        public ActionResult DeleteProfileConfirmed()
        {
            return View();
        }

        public ActionResult DeletePatientProfileConfirmed()
        {
            return View();
        }

        [Authorize]
        public ActionResult DeleteProfileImpl(string userId, string code)
        {
            if (ModelState.IsValid)
            {
                if (code == null || userId == null)
                {
                    logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Delete profile confirmation failed", "UserId: " + userId);
                    return View("DeleteProfileError");
                }
                else
                {
                    string codeStr = System.Web.HttpUtility.UrlDecode(code);
                    var result = UserManager.ConfirmEmail(userId, codeStr);
                    if (result.Succeeded)
                    {
                        var user = UserManager.FindById(userId);
                        if (user != null)
                        {
                            AuthManager.SignOut();
                            
                            db.Calendar.RemoveRange(db.Calendar.Where(x => x.UserId == userId));
                            db.CachedResults.RemoveRange(db.CachedResults.Where(x => x.UserId == userId));
                            db.CalendarSettings.RemoveRange(db.CalendarSettings.Where(x => x.UserId == userId));
                            db.OfficeDetails.RemoveRange(db.OfficeDetails.Where(x => x.UserId == userId));
                            db.MedicInvoiceDetails.RemoveRange(db.MedicInvoiceDetails.Where(x => x.UserId == userId));
                            db.MedicDetails.RemoveRange(db.MedicDetails.Where(x => x.UserId == userId));
                            db.AspNetRoles.RemoveRange(db.AspNetRoles.Where(x => x.Id == userId));
                            db.AspNetUsers.RemoveRange(db.AspNetUsers.Where(x => x.Id == userId));

                            db.SaveChanges();

                            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Profile deleted successfully", "UserId: " + userId);

                            return View("DeleteProfileConfirmed");
                        }
                    }
                }
            }

            return View("DeleteProfileError");
        }

        [Authorize]
        public ActionResult DeletePatientProfileImpl(string userId, string code)
        {
            if (ModelState.IsValid)
            {
                if (code == null || userId == null)
                {
                    logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Delete patient profile confirmation failed", "UserId: " + userId);
                    return View("DeletePatientProfileError");
                }
                else
                {
                    string codeStr = System.Web.HttpUtility.UrlDecode(code);
                    var result = UserManager.ConfirmEmail(userId, codeStr);
                    if (result.Succeeded)
                    {
                        var user = UserManager.FindById(userId);
                        if (user != null)
                        {
                            AuthManager.SignOut();

                            db.AspNetRoles.RemoveRange(db.AspNetRoles.Where(x => x.Id == userId));
                            db.AspNetUsers.RemoveRange(db.AspNetUsers.Where(x => x.Id == userId));

                            db.SaveChanges();

                            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Patient profile deleted successfully", "UserId: " + userId);

                            return View("DeletePatientProfileConfirmed");
                        }
                    }
                }
            }

            return View("DeletePatientProfileError");
        }

        /// <summary>
        /// Email 003
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="email"></param>
        private void SendDeleteProfileEmail(string userId, string email)
        {
            string hash = "";
            using (SHA256 shaHash = SHA256.Create())
            {
                hash = GetSha256Hash(shaHash, email);
            }

            if (String.IsNullOrEmpty(hash))
            {
                logger.Log(DateTime.Now, Logger.Level.Error, email, "Send delete profile e-mail - Hash failed", null);
                return;// ERROR
            }


            var code = UserManager.GenerateEmailConfirmationToken(userId);
            string codeStr = System.Web.HttpUtility.UrlEncode(code);

            var callbackUrl = Url.Action("DeleteProfileImpl", "Admin", new { userId = userId, code = codeStr }, protocol: Request.Url.Scheme);

            string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Vážená paní, vážený pane,</strong><br /><br />
                        <strong>pro úplné smazání Vašeho profilu prosím klikněte na tento odkaz </strong>";

            body += "<a href =\"" + callbackUrl + "\">" + callbackUrl + "</a>";
            body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>
            ";
            
            MailMessage message = new MailMessage("info@sosbody.cz", email, "Smazání profilu SOS Body", body);
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.zoner.com");
            client.Send(message);
            
            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Delete profile e-mail sent", null);
        }

        /// <summary>
        /// Email 004
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="email"></param>
        private void SendDeletePatientProfileEmail(string userId, string email)
        {
            string hash = "";
            using (SHA256 shaHash = SHA256.Create())
            {
                hash = GetSha256Hash(shaHash, email);
            }

            if (String.IsNullOrEmpty(hash))
            {
                logger.Log(DateTime.Now, Logger.Level.Error, email, "Send delete profile e-mail - Hash failed", null);
                return;// ERROR
            }


            var code = UserManager.GenerateEmailConfirmationToken(userId);
            string codeStr = System.Web.HttpUtility.UrlEncode(code);

            var callbackUrl = Url.Action("DeletePatientProfileImpl", "Admin", new { userId = userId, code = codeStr }, protocol: Request.Url.Scheme);

            string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Vážená paní, vážený pane,</strong><br /><br />
                        <strong>pro dokončení smazání Vašeho profilu prosím klikněte na tento odkaz </strong>";

            body += "<a href =\"" + callbackUrl + "\">" + callbackUrl + "</a>";
            body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>
            ";
            
            MailMessage message = new MailMessage("info@sosbody.cz", email, "Smazání profilu SOS Body", body);
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.zoner.com");
            client.Send(message);

            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Delete profile e-mail sent", null);
        }

        private CalendarSettings CreateDefaultCalendarSettings(string userId, int officeId)
        {
            var settings = db.CalendarSettings.Where(x => x.OfficeId == officeId).FirstOrDefault();
            if (settings != null)
                return null;

            CalendarSettings calendarSettings = GetDefaultCalendarSettings();
            calendarSettings.UserId = userId;
            calendarSettings.OfficeId = officeId;

            db.CalendarSettings.Add(calendarSettings);
            db.SaveChanges();

            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Created Default Calendar Settings", null);

            return calendarSettings;
        }

        private int CreateDefaultOfficeDetail(string userId)
        {
            OfficeDetail model = new OfficeDetail()
            {
                UserId = userId,
                Name = "",
                Street = "",
                StreetNo = "",
                PostalCode = "",
                City = "",
                Country = "Česká republika",
                Latitude = "",
                Longitude = ""
            };

            db.OfficeDetails.Add(model);
            db.SaveChanges();

            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Created Default Office Detail", "OfficeDetail id = " + model.Id.ToString());

            return model.Id;
        }

        private bool IsValidEmail(string email)
        {
            string emailTrimed = email.Trim();

            if (!string.IsNullOrEmpty(emailTrimed))
            {
                bool hasWhitespace = emailTrimed.Contains(" ");

                int indexOfAtSign = emailTrimed.LastIndexOf('@');

                if (indexOfAtSign > 0 && !hasWhitespace)
                {
                    string afterAtSign = emailTrimed.Substring(indexOfAtSign + 1);

                    int indexOfDotAfterAtSign = afterAtSign.LastIndexOf('.');

                    if (indexOfDotAfterAtSign > 0 && afterAtSign.Substring(indexOfDotAfterAtSign).Length > 1)
                        return true;
                }
            }

            return false;
        }
        //private void InsertMedicDetails(string userId, CreateModel model)
        //{
        //    MedicDetail det = new MedicDetail()
        //    {
        //        UserId = userId,
        //        Name = model.Name,
        //        Lastname = model.Lastname,
        //        Phone = model.Phone,
        //        Email = model.Email
        //    };
        //    db.MedicDetails.Add(det);

        //    //http://stackoverflow.com/questions/11581147/entity-framework-exception-invalid-object-name
        //    db.SaveChanges();

        //    logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Insert Medic Details", null);
        //}

        public AppRole GetMedicRole()
        {
            AppRole role = RoleManager.FindByName("SOSBodyMedic");
            if (role == null)
            {
                AppRole newRole = new AppRole()
                {
                    Name = "SOSBodyMedic"
                };

                RoleManager.Create(newRole);
                role = newRole;
            }

            return role;
        }

        public AppRole GetAdminRole()
        {
            AppRole role = RoleManager.FindByName("SOSBodyAdmin");
            if (role == null)
            {
                AppRole newRole = new AppRole()
                {
                    Name = "SOSBodyAdmin"
                };

                RoleManager.Create(newRole);
                role = newRole;
            }

            return role;
        }

        public AppRole GetPatientRole()
        {
            AppRole role = RoleManager.FindByName("SOSBodyPatient");
            if (role == null)
            {
                AppRole newRole = new AppRole()
                {
                    Name = "SOSBodyPatient"
                };

                RoleManager.Create(newRole);
                role = newRole;
            }

            return role;
        }

        public ActionResult RegisterPatient()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RegisterPatient(CreateModelPatient model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = new AppUser()
                {
                    UserName = model.Email,
                    Email = model.Email
                };

                AppRole role = GetPatientRole();

                IdentityResult userResult = await UserManager.CreateAsync(user, model.Password);

                if (userResult.Succeeded)
                {
                    IdentityResult roleResult = await UserManager.AddToRoleAsync(user.Id, role.Name);

                    if (roleResult.Succeeded)
                    {
                        await SignInAsync(user, false);
                        SendPatientActivationEmail(model.Email);
                        logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "Register patient - New registered patient", "email: " + model.Email);
                        return Redirect("/Admin/HomePatient");
                    }
                    else
                    {
                        foreach (string error in roleResult.Errors)
                            ModelState.AddModelError("", error);
                    }
                }
                else
                {
                    foreach (string error in userResult.Errors)
                        ModelState.AddModelError("", error);
                }
            }

            return View(model);
        }

        private async Task SignInAsync(AppUser user, bool isPersistent)
        {
            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthManager.SignOut();
            AuthManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, ident);
        }


        [Authorize]
        [HttpPost]
        public ActionResult LookUp(string ico, string redirect, string email, string phone, string phonePrefix, string prefix, string bankAccount, string postfix, string taxIdentifyNo)
        {
            //87968151
            string URLString = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico=" + ico;

            string userId = User.Identity.GetUserId();
            string street = "";
            string city = "";
            string cityPart = "";
            string buildingNo = "";
            string orientNo = "";
            string name = "";
            string psc = "";
            //string item = "";
            //string label = "";
            int resultCount = 0;

            XmlTextReader reader = new XmlTextReader(URLString);
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        if (reader.Name == "dtt:Nazev_obce")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                city = reader.Value;
                        }
                        else if (reader.Name == "dtt:Nazev_casti_obce")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                cityPart = reader.Value;
                        }
                        else if (reader.Name == "dtt:Cislo_domovni")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                buildingNo = reader.Value;
                        }
                        else if (reader.Name == "dtt:Cislo_orientacni")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                orientNo = reader.Value;
                        }
                        else if (reader.Name == "are:Obchodni_firma")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                name = reader.Value;
                        }
                        else if (reader.Name == "dtt:Nazev_ulice")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                street = reader.Value;
                        }
                        else if (reader.Name == "dtt:PSC")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                psc = reader.Value;
                        }
                        else if (reader.Name == "are:Pocet_zaznamu")
                        {
                            reader.Read();
                            if (reader.NodeType == XmlNodeType.Text)
                                resultCount = Convert.ToInt32(reader.Value);
                        }
                        //else if (reader.Name == "dtt:Soud")
                        //{
                        //    reader.Read();
                        //    if (reader.NodeType == XmlNodeType.Text)
                        //        item = reader.Value;
                        //}
                        //else if (reader.Name == "dtt:Oddil_vlozka")
                        //{
                        //    reader.Read();
                        //    if (reader.NodeType == XmlNodeType.Text)
                        //        label = reader.Value;

                        //}

                            break;
                    case XmlNodeType.Text:
                        //Display the text in each element.
                        //string name = reader.Name;
                        //Console.WriteLine(reader.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        break;
                }
            }


            string address = "";
            if (resultCount > 0)
            {
                address = street + " " + buildingNo;
                if (String.IsNullOrEmpty(orientNo) == false)
                    address += "/" + orientNo + " ";
                address += ", " + psc + ", " + city;
                if (String.IsNullOrEmpty(cityPart) == false)
                    address += " - " + cityPart;
            }

            InvoiceDetailModel model = new InvoiceDetailModel()
            {
                UserId = userId,
                Name = name,
                Address = address,
                Phone = phone,
                Email = email,
                IdentifyNo = ico,
                TaxIdentifyNo = taxIdentifyNo,
                Prefix = prefix,
                BankAccount = bankAccount,
                Postfix = postfix,
                PhonePrefix = phonePrefix,
                ProfileImage = GetProfileImage(Globals.ProfileImagesPath)
            };

            TempData["InvoiceData"] = model;

            logger.Log(DateTime.Now, Logger.Level.Trace, model.UserId, "Look up", "ICO - " + ico);

            var redirectUrl = new UrlHelper(Request.RequestContext).Action(redirect);
            return Json(new { Url = redirectUrl });
        }

        [Authorize]
        [HttpPost]
        public ActionResult Action(int actionId)
        {
            string userId = User.Identity.GetUserId();

            var action = db.Calendar.Where(x => x.Id == actionId).ToList()[0];
            if (action != null)
            {
                string officeName = "";
                var office = db.OfficeDetails.Where(x => x.Id == action.OfficeId).FirstOrDefault();
                if(office != null)
                    officeName = office.Name;

                string actionTimeHour = action.Date.Hour.ToString();
                string actionTimeMinute = action.Date.Minute != 0 ? action.Date.Minute.ToString() : "00";

                return Json(new { hour = actionTimeHour, minute = actionTimeMinute, info = action.Info, sosbody = action.IsSOSbody, userid = action.UserId, duration = action.Duration, price = action.Price, booked = action.Booked, officeName = officeName });
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult GetPhoneNumberLength(string prefix)
        {
            var item = db.PhonePrefix.Where(x => x.Text == prefix).FirstOrDefault();
            int length = 9;
            if (item != null)
                length = item.Length;

            return Json(new { Length = length });
        }


        //http://www.c-sharpcorner.com/article/auto-complete-address-using-google-api-in-Asp-Net-mvc-5/
        //https://forums.asp.net/t/1981980.aspx?ASp+Net+MVC+Using+Google+API

        [Authorize]
        public ActionResult CalendarSettings(int? calIndex)
        {
            string userId = User.Identity.GetUserId();

            CalendarSettings calendarSettings;

            var offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
            if (offices == null || offices.Count == 0)
                return Redirect("/Home/ErrorPage");

            int currentCalendarId = calIndex ?? offices[0].Id;

            calendarSettings = db.CalendarSettings.FirstOrDefault(x => x.OfficeId == currentCalendarId);
            if (calendarSettings == null)
                return Redirect("/Home/ErrorPage");

            CalendarSettingsModel model = new CalendarSettingsModel()
            {
                Id = calendarSettings.Id,
                OfficeId = calendarSettings.OfficeId,
                Monday = calendarSettings.Monday,
                Tuesday = calendarSettings.Tuesday,
                Wednesday = calendarSettings.Wednesday,
                Thursday = calendarSettings.Thursday,
                Friday = calendarSettings.Friday,
                Saturday = calendarSettings.Saturday,
                Sunday = calendarSettings.Sunday,
                MondayStartHour = calendarSettings.MondayStartHour,
                MondayEndHour = calendarSettings.MondayEndHour,
                TuesdayStartHour = calendarSettings.TuesdayStartHour,
                TuesdayEndHour = calendarSettings.TuesdayEndHour,
                WednesdayStartHour = calendarSettings.WednesdayStartHour,
                WednesdayEndHour = calendarSettings.WednesdayEndHour,
                ThursdayStartHour = calendarSettings.ThursdayStartHour,
                ThursdayEndHour = calendarSettings.ThursdayEndHour,
                FridayStartHour = calendarSettings.FridayStartHour,
                FridayEndHour = calendarSettings.FridayEndHour,
                SaturdayStartHour = calendarSettings.SaturdayStartHour,
                SaturdayEndHour = calendarSettings.SaturdayEndHour,
                SundayStartHour = calendarSettings.SundayStartHour,
                SundayEndHour = calendarSettings.SundayEndHour,
                MapHoursItemTypes = InitHoursDropDownList(),
                GoogleCalendarConnection = calendarSettings.GoogleCalendar,
                ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                Price30 = calendarSettings.Price30,
                Price60 = calendarSettings.Price60,
                Price90 = calendarSettings.Price90,
                CurrentCalendarId = currentCalendarId
            };

            model.Offices = new List<OfficeDetailModel>();

            foreach (OfficeDetail office in offices)
            {
                OfficeDetailModel officeModel = new OfficeDetailModel()
                {
                    Id = office.Id,
                    Name = office.Name,
                    UserId = office.UserId,
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    PostalCode = office.PostalCode,
                    City = office.City,
                    Country = office.Country,
                    Latitude = office.Latitude,
                    Longitude = office.Longitude,
                    Phone = office.Phone
                };

                model.Offices.Add(officeModel);
            }

            return View(model);
        }


        [Authorize]
        [HttpPost]
        public ActionResult CalendarSettings(CalendarSettingsModel model)
        {
            string userId = User.Identity.GetUserId();

            var calSettings = db.CalendarSettings.Find(model.Id);
            if (calSettings != null)
            {
                calSettings.Monday = model.Monday;
                calSettings.Tuesday = model.Tuesday;
                calSettings.Wednesday = model.Wednesday;
                calSettings.Thursday = model.Thursday;
                calSettings.Friday = model.Friday;
                calSettings.Saturday = model.Saturday;
                calSettings.Sunday = model.Sunday;
                calSettings.MondayStartHour = (byte)model.MondayStartHour;
                calSettings.MondayEndHour = (byte)model.MondayEndHour;
                calSettings.TuesdayStartHour = (byte)model.TuesdayStartHour;
                calSettings.TuesdayEndHour = (byte)model.TuesdayEndHour;
                calSettings.WednesdayStartHour = (byte)model.WednesdayStartHour;
                calSettings.WednesdayEndHour = (byte)model.WednesdayEndHour;
                calSettings.ThursdayStartHour = (byte)model.ThursdayStartHour;
                calSettings.ThursdayEndHour = (byte)model.ThursdayEndHour;
                calSettings.FridayStartHour = (byte)model.FridayStartHour;
                calSettings.FridayEndHour = (byte)model.FridayEndHour;
                calSettings.SaturdayStartHour = (byte)model.SaturdayStartHour;
                calSettings.SaturdayEndHour = (byte)model.SaturdayEndHour;
                calSettings.SundayStartHour = (byte)model.SundayStartHour;
                calSettings.SundayEndHour = (byte)model.SundayEndHour;
                calSettings.GoogleCalendar = model.GoogleCalendarConnection;
                calSettings.UserId = userId;
                calSettings.OfficeId = model.OfficeId;
                calSettings.Id = model.Id;
                calSettings.Price30 = model.Price30;
                calSettings.Price60 = model.Price60;
                calSettings.Price90 = model.Price90;
            }

            db.SaveChanges();

            model.MapHoursItemTypes = InitHoursDropDownList();
            model.ProfileImage = GetProfileImage(Globals.ProfileImagesPath);

            var offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
            model.Offices = new List<OfficeDetailModel>();

            foreach (OfficeDetail office in offices)
            {
                OfficeDetailModel officeModel = new OfficeDetailModel()
                {
                    Id = office.Id,
                    Name = office.Name,
                    UserId = office.UserId,
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    PostalCode = office.PostalCode,
                    City = office.City,
                    Country = office.Country,
                    Latitude = office.Latitude,
                    Longitude = office.Longitude,
                    Phone = office.Phone
                };

                model.Offices.Add(officeModel);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult HomeBilling()
        {
            string userId = User.Identity.GetUserId();

            var detail = db.MedicInvoiceDetails.Where(x => x.UserId == userId);
            if (detail == null || detail.ToList().Count == 0)
            {
                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    UserId = userId,
                    Address = "",
                    BankAccount = "",
                    Email = "",
                    IdentifyNo = "",
                    Name = "",
                    Phone = "",
                    TaxIdentifyNo = "",
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath)
                };

                return View(model);
            }
            else
            {
                MedicInvoiceDetail det = detail.ToList()[0];
                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    Id = det.Id,
                    UserId = det.UserId,
                    Name = det.Name,
                    Address = det.Address,
                    Phone = det.Phone.Replace('-', ' '),
                    Email = det.Email,
                    IdentifyNo = det.IdentifyNo,
                    TaxIdentifyNo = det.TaxIdentifyNo,
                    BankAccount = det.BankAccount,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath)
                };

                return View(model);
            }
        }

        [Authorize]
        public ActionResult HomeOffice()
        {
            string userId = User.Identity.GetUserId();

            var detail = db.MedicDetails.Where(x => x.UserId == userId);
            if (detail.ToList().Count == 0)
            {
                FormMedicModel model = new FormMedicModel();
                //model.SpecializationList = InitMedicSpecialization();
                model.Offices = new List<OfficeDetailModel>();
                OfficeDetailModel officeModel = new OfficeDetailModel();

                model.Offices.Add(officeModel);
                return View(model);
            }
            else
            {
                SOSBody.MedicDetail det = detail.ToList()[0];
                FormMedicModel model = new FormMedicModel()
                {
                    Id = det.Id,
                    Name = det.Name,
                    Lastname = det.Lastname,
                    Phone = det.Phone,
                    Email = det.Email,
                    Title = det.Title,
                    TitleBehind = det.TitleBehind,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    Spec1 = det.Spec1,
                    Spec2 = det.Spec2,
                    Spec3 = det.Spec3,
                    Spec4 = det.Spec4,
                    Spec5 = det.Spec5,
                    Spec6 = det.Spec6
                    //SpecializationList = InitMedicSpecialization()
                };

                List<OfficeDetail> offices = db.OfficeDetails.Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToList();
                model.Offices = new List<OfficeDetailModel>();

                foreach (OfficeDetail office in offices)
                {
                    OfficeDetailModel officeModel = new OfficeDetailModel()
                    {
                        Id = office.Id,
                        Name = office.Name,
                        UserId = office.UserId,
                        Street = office.Street,
                        StreetNo = office.StreetNo,
                        PostalCode = office.PostalCode,
                        City = office.City,
                        Country = office.Country,
                        Latitude = office.Latitude,
                        Longitude = office.Longitude,
                        Phone = office.Phone
                    };

                    if (officeModel.City != "" && officeModel.Name != "")
                        model.Offices.Add(officeModel);
                    else
                    {
                        db.OfficeDetails.Remove(office);
                        db.SaveChanges();

                        var calendarSettings = db.CalendarSettings.FirstOrDefault(x => x.OfficeId == office.Id);
                        if (calendarSettings != null)
                        {
                            db.CalendarSettings.Remove(calendarSettings);
                            db.SaveChanges();

                            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Calendar settings deleted", "Calendar settintgs Id - " + calendarSettings.Id);
                        }
                    }
                }

                return View(model);
            }
        }

        [Authorize]
        public ActionResult HomeMedic()
        {
            string userId = User.Identity.GetUserId();

            var detail = db.MedicDetails.Where(x => x.UserId == userId);
            if (detail.ToList().Count == 0)
            {
                FormMedicModel model = new FormMedicModel();
                //model.SpecializationList = InitMedicSpecialization();
                model.Offices = new List<OfficeDetailModel>();
                OfficeDetailModel officeModel = new OfficeDetailModel();

                model.Offices.Add(officeModel);
                return View(model);
            }
            else
            {
                SOSBody.MedicDetail det = detail.ToList()[0];
                FormMedicModel model = new FormMedicModel()
                {
                    Id = det.Id,
                    Name = det.Name,
                    Lastname = det.Lastname,
                    Phone = det.Phone.Replace('-', ' '),
                    Email = det.Email,
                    Title = det.Title,
                    TitleBehind = det.TitleBehind,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    Spec1 = det.Spec1,
                    Spec2 = det.Spec2,
                    Spec3 = det.Spec3,
                    Spec4 = det.Spec4,
                    Spec5 = det.Spec5,
                    Spec6 = det.Spec6
                    //SpecializationList = InitMedicSpecialization()
                };

                List<OfficeDetail> offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
                model.Offices = new List<OfficeDetailModel>();

                foreach (OfficeDetail office in offices)
                {
                    OfficeDetailModel officeModel = new OfficeDetailModel()
                    {
                        Id = office.Id,
                        Name = office.Name,
                        UserId = office.UserId,
                        Street = office.Street,
                        StreetNo = office.StreetNo,
                        PostalCode = office.PostalCode,
                        City = office.City,
                        Country = office.Country,
                        Latitude = office.Latitude,
                        Longitude = office.Longitude,
                        Phone = office.Phone
                    };

                    model.Offices.Add(officeModel);
                }

                return View(model);
            }
        }

        [Authorize]
        public ActionResult HomePatient()
        {
            string userId = User.Identity.GetUserId();

            HomePatientModel model = new HomePatientModel()
            {
                UserId = userId
            };

            return View(model);
        }

        [Authorize]
        public ActionResult HistoryPatient()
        {
            string userId = User.Identity.GetUserId();

            HistoryPatientModel model = new HistoryPatientModel();
            model.Reservations = new List<ReservationDetailModel>();

            var orders = db.Orders.Where(x => x.UserId == userId).ToList();
            if (orders == null)
                return View(model);

            foreach(Order order in orders)
            {
                var office = db.OfficeDetails.Where(x => x.Id == order.OfficeId).FirstOrDefault();
                if (office == null)
                    return View(model);

                var medic = db.MedicDetails.Where(x => x.UserId == office.UserId).FirstOrDefault();
                if (medic == null)
                    return View(model);

                var action = db.Calendar.Where(x => x.Id == order.ActionId).FirstOrDefault();
                if (action == null)
                    return View();

                string phone = ph.GetPhonePure(office.Phone);
                if (String.IsNullOrEmpty(phone) == true)
                    phone = medic.Phone;

                var payment = db.Payments.Where(x => x.OrderId == order.Id).FirstOrDefault();
                if (payment == null)
                    return View(model);

                ReservationDetailModel resultModel = new ReservationDetailModel()
                {
                    OrderId = order.Id,
                    Code = payment.TransId,
                    MedicName = medic.Name + " " + medic.Lastname,
                    MedicAddress = office.Street + " " + office.StreetNo + ", " + office.City,
                    Phone = phone,
                    Email = medic.Email,
                    CancelEnabled = false,
                    Date = action.Date.ToString("dd. MM. yyyy HH:mm")
                };

                DateTime dateNow = DateTime.Now;
                TimeSpan span = action.Date.Subtract(dateNow);
                if (span.TotalHours > 24 && payment.Status != "CANCELLED")
                    resultModel.CancelEnabled = true;

                model.Reservations.Add(resultModel);
            }

            return View(model);
        }

        [Authorize]
        public ActionResult FormMedic()
        {
            string userId = User.Identity.GetUserId();

            var detail = db.MedicDetails.Where(x => x.UserId == userId);
            if (detail.ToList().Count == 0)
            {
                FormMedicModel model = new FormMedicModel();
                //model.MapYearItemTypes = InitYearDropDownList();
                //model.MapMonthItemTypes = InitMonthDropDownList();
                //model.MapDayItemTypes = InitDayDropDownList();
                //if (model.Year == null)
                //    model.Year = "  Rok  ";
                //if (model.Month == null)
                //    model.Month = "  Měsíc  ";
                //if (model.Day == null)
                //    model.Day = "  Den  ";
                model.UserId = userId;
                return View(model);
            }
            else
            {
                MedicDetail det = detail.ToList()[0];
                FormMedicModel model = new FormMedicModel()
                {
                    Id = det.Id,
                    UserId = det.UserId,
                    Name = det.Name,
                    Lastname = det.Lastname,
                    Phone = ph.GetPhonePure(det.Phone),
                    Email = det.Email,
                    Title = det.Title,
                    TitleBehind = det.TitleBehind,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    //MapYearItemTypes = InitYearDropDownList(),
                    //MapMonthItemTypes = InitMonthDropDownList(),
                    //MapDayItemTypes = InitDayDropDownList(),
                    Spec1 = det.Spec1,
                    Spec2 = det.Spec2,
                    Spec3 = det.Spec3,
                    Spec4 = det.Spec4,
                    Spec5 = det.Spec5,
                    Spec6 = det.Spec6,
                    PhonePrefix = ph.GetPhonePrefix(det.Phone),
                    PhonePrefixList = ph.InitPhonePrefixList(det.Phone),
                    PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone)
                };

                //if (det.DateOfBirth != null && det.DateOfBirth.HasValue)
                //{
                //    model.Year = det.DateOfBirth.Value.Year.ToString();
                //    model.Month = det.DateOfBirth.Value.Month.ToString();
                //    model.Day = det.DateOfBirth.Value.Day.ToString();
                //}

                return View(model);
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult FormMedic(FormMedicModel model)
        {
            string userId = User.Identity.GetUserId();

            var item = db.PhonePrefix.Where(x => x.Text == model.PhonePrefix).FirstOrDefault();
            int phoneLength = 9;
            if (item != null)
                phoneLength = item.Length;

            if (ModelState.IsValid && ph.GetPhonePure(model.Phone).Length == phoneLength)
            {
                var detail = db.MedicDetails.Where(x => x.Id == model.Id);
                if (detail == null || detail.ToList().Count == 0)
                {
                    MedicDetail det = new MedicDetail()
                    {
                        UserId = userId,
                        Name = model.Name,
                        Lastname = model.Lastname,
                        Phone = model.PhonePrefix + "-" + model.Phone,
                        Email = model.Email,
                        Title = model.Title,
                        TitleBehind = model.TitleBehind,
                        //DateOfBirth = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), Convert.ToInt32(model.Day)),
                        Spec1 = model.Spec1,
                        Spec2 = model.Spec2,
                        Spec3 = model.Spec3,
                        Spec4 = model.Spec4,
                        Spec5 = model.Spec5,
                        Spec6 = model.Spec6
                    };
                    db.MedicDetails.Add(det);
                }
                else
                {
                    MedicDetail det = db.MedicDetails.Find(model.Id);
                    if (det != null)
                    {
                        det.Name = model.Name;
                        det.Lastname = model.Lastname;
                        det.Phone = model.PhonePrefix + "-" + model.Phone;
                        det.Email = model.Email;
                        det.Title = model.Title;
                        det.TitleBehind = model.TitleBehind;
                        //if (model.Year != null && model.Month != null && model.Day != null)
                        //    det.DateOfBirth = new DateTime(Convert.ToInt32(model.Year), Convert.ToInt32(model.Month), Convert.ToInt32(model.Day));
                        det.Spec1 = model.Spec1;
                        det.Spec2 = model.Spec2;
                        det.Spec3 = model.Spec3;
                        det.Spec4 = model.Spec4;
                        det.Spec5 = model.Spec5;
                        det.Spec6 = model.Spec6;
                    }
                }

                //http://stackoverflow.com/questions/11581147/entity-framework-exception-invalid-object-name
                try
                {
                    int res = db.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Form medic - " + ex.Message, ex.InnerException.ToString());
                }
                return Redirect("/Admin/HomeMedic");
            }
            else
            {
                if(ph.GetPhonePure(model.Phone).Length != phoneLength && ph.GetPhonePure(model.Phone).Length != 0)
                    model.ErrorMessage = "Délka telefonního čísla není platná";
                //ModelState.AddModelError("Date", "Neplatné datum");
                //model = new FormMedicModel();
                //model.MapYearItemTypes = InitYearDropDownList();
                //model.MapMonthItemTypes = InitMonthDropDownList();
                //model.MapDayItemTypes = InitDayDropDownList();
                //if (model.Year == null)
                //    model.Year = "  Rok  ";
                //if (model.Month == null)
                //    model.Month = "  Měsíc  ";
                //if (model.Day == null)
                //    model.Day = "  Den  ";
                model.UserId = userId;
                model.Offices = new List<OfficeDetailModel>();
                model.ProfileImage = GetProfileImage(Globals.ProfileImagesPath);
                model.PhonePrefix = ph.GetPhonePrefix(model.Phone);
                model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                model.PhonePrefixLength = ph.GetPhonePrefixLength(model.Phone);
            }

            //return Redirect("HomeMedic");
            return View(model);
        }

        private List<SelectListItem> InitHoursDropDownList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 0; i < 24; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                list.Add(item);
            }

            return list;
        }

        private List<SelectListItem> InitYearDropDownList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            DateTime date = DateTime.Now;

            for (int i = date.Year; i > 1900; i--)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                list.Add(item);
            }

            return list;
        }

        private List<SelectListItem> InitMonthDropDownList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 1; i < 13; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                list.Add(item);
            }

            return list;
        }

        private List<SelectListItem> InitDayDropDownList()
        {
            List<SelectListItem> list = new List<SelectListItem>();

            for (int i = 1; i < 32; i++)
            {
                SelectListItem item = new SelectListItem();
                item.Value = i.ToString();
                item.Text = i.ToString();
                list.Add(item);
            }

            return list;
        }

        public string GetProfileImage(string dir)
        {
            string directory = Server.MapPath(dir);
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                int lastSlashPos = fileName.LastIndexOf('\\');
                string name = fileName.Substring(lastSlashPos + 1);
                int lastComaPos = name.LastIndexOf('.');
                string subName = name.Substring(0, lastComaPos);

                if (subName == User.Identity.GetUserId())
                    return dir + name;
            }

            return "~/Images/no_profile_photo.png";
        }

        //public ActionResult FormPatient()
        // {
        //    string userId = User.Identity.GetUserId();

        //var detail = db.PatientDetails.Where(x => x.UserId == userId);
        //if (detail.ToList().Count == 0)
        //{
        //    FormPatientModel model = new FormPatientModel();
        //    return View(model);
        //}
        //else
        //{
        //    PatientDetail det = detail.ToList()[0];
        //    FormPatientModel model = new FormPatientModel()
        //    {
        //        Id = det.Id,
        //        Phone = det.Phone
        //    };
        //   return View(model);
        //}
        //}

        [HttpPost]
        public ActionResult FormPatient(FormPatientModel model)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();

                //var detail = db.PatientDetails.Where(x => x.Id == model.Id);
                //if (detail == null || detail.ToList().Count == 0)
                //{

                //    PatientDetail det = new PatientDetail()
                //    {
                //        UserId = userId,
                //        Phone = model.Phone
                //    };
                //    db.PatientDetails.Add(det);
                //}
                //else
                //{
                //    PatientDetail det = db.PatientDetails.Find(model.Id);
                //    if (det != null)
                //    {
                //        det.Phone = model.Phone;
                //    }
                //}

                //int res = db.SaveChanges();
                return Redirect("HomePatient");
            }

            return View(model);
        }

        [Authorize]
        public ActionResult Office()
        {
            bool EnableAddOfficeButton = true;

            if(Session["AddOfficeButtonDisabled"] != null)
            {
                if ((int)Session["AddOfficeButtonDisabled"] == 1)
                    EnableAddOfficeButton = false;
            }


            string userId = User.Identity.GetUserId();

            OfficeListModel officeListModel = new OfficeListModel();
            officeListModel.Offices = new List<OfficeDetailModel>();
            officeListModel.ShowAddOfficeButton = EnableAddOfficeButton;

            var offices = db.OfficeDetails.Where(x => x.UserId == userId).OrderByDescending(x => x.Id).ToList();
            if (offices == null || offices.Count == 0)
            {
                OfficeDetailModel model = new OfficeDetailModel()
                {
                    UserId = userId,
                    Name = "",
                    Street = "",
                    StreetNo = "",
                    PostalCode = "",
                    City = "",
                    Country = "Česká republika",
                    Latitude = "",
                    Longitude = "",
                    Phone = "",
                    PhonePrefixLength = 9,
                    PhonePrefixList = ph.InitPhonePrefixList("")
                };

                officeListModel.Offices.Add(model);

                return View(officeListModel);
            }
            else
            {
                foreach (OfficeDetail det in offices)
                {
                    OfficeDetailModel model = new OfficeDetailModel()
                    {
                        Id = det.Id,
                        UserId = det.UserId,
                        Name = det.Name,
                        Street = det.Street,
                        StreetNo = det.StreetNo,
                        PostalCode = det.PostalCode,
                        City = det.City,
                        Country = det.Country,
                        Latitude = det.Latitude,
                        Longitude = det.Longitude,
                        Phone = ph.GetPhonePure(det.Phone),
                        PhonePrefix = ph.GetPhonePrefix(det.Phone),
                        PhonePrefixList = ph.InitPhonePrefixList(det.Phone),
                        PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone)
                    };

                    officeListModel.Offices.Add(model);
                }

                return View(officeListModel);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Office(OfficeListModel model)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();

                foreach (var office in model.Offices)
                {
                    var detail = db.OfficeDetails.Where(x => x.Id == office.Id).ToList();
                    if (detail == null || detail.ToList().Count == 0)
                    {

                        OfficeDetail det = new OfficeDetail()
                        {
                            Id = office.Id,
                            UserId = office.UserId,
                            Name = office.Name,
                            Street = office.Street,
                            StreetNo = office.StreetNo,
                            PostalCode = office.PostalCode,
                            City = office.City,
                            Country = office.Country,
                            Latitude = office.Latitude,
                            Longitude = office.Longitude,
                            Phone = office.PhonePrefix + "-" + office.Phone
                        };
                        db.OfficeDetails.Add(det);
                        db.SaveChanges();

                        // Here we create calendar settings to this office
                        CreateDefaultCalendarSettings(userId, det.Id);
                    }
                    else
                    {
                        OfficeDetail det = db.OfficeDetails.Find(office.Id);
                        if (det != null)
                        {
                            det.UserId = office.UserId;
                            det.Name = office.Name;
                            det.Street = office.Street;
                            det.StreetNo = office.StreetNo;
                            det.PostalCode = office.PostalCode;
                            det.City = office.City;
                            det.Country = office.Country;
                            det.Latitude = office.Latitude;
                            det.Longitude = office.Longitude;
                            det.Phone = office.PhonePrefix + "-" + office.Phone;
                        }

                        var cachedResult = db.CachedResults.Where(x => x.OfficeId == office.Id).FirstOrDefault();
                        if(cachedResult == null)
                        {
                            GeoPoint point = await GetPoint(det.Street + " " + det.StreetNo + " " + det.City + " " + det.PostalCode);
                            if (point != null)
                            {
                                CachedResults cachedRes = new CachedResults()
                                {
                                    UserId = userId,
                                    OfficeId = det.Id,
                                    Name = det.Name,
                                    Street = det.Street,
                                    StreetNo = det.StreetNo,
                                    PostalCode = det.PostalCode,
                                    City = det.City,
                                    Country = det.Country,
                                    Latitude = point.Latitude,
                                    Longitude = point.Longitude
                                };

                                db.CachedResults.Add(cachedRes);
                            }
                        }
                        else
                        {
                            cachedResult.City = office.City;
                            cachedResult.Country = office.Country;
                            cachedResult.Name = office.Name;
                            cachedResult.Street = office.Street;
                            cachedResult.StreetNo = office.StreetNo;
                            cachedResult.PostalCode = office.PostalCode;
                        }
                    }
                }

                Session["AddOfficeButtonDisabled"] = null;

                //http://stackoverflow.com/questions/11581147/entity-framework-exception-invalid-object-name
                db.SaveChanges();
                return Redirect("HomeOffice");
            }

            foreach(var office in model.Offices)
            {
                office.PhonePrefix = ph.GetPhonePrefix(office.Phone);
                office.PhonePrefixList = ph.InitPhonePrefixList(office.Phone);
                office.PhonePrefixLength = ph.GetPhonePrefixLength(office.PhonePrefix + "-" + office.Phone);
            }

            return View(model);
        }


        [Authorize]
        public ActionResult AddOffice()
        {
            string userId = User.Identity.GetUserId();

            OfficeDetail newOffice = new OfficeDetail()
            {
                UserId = userId,
                Name = "",
                Street = "",
                StreetNo = "",
                PostalCode = "",
                City = "",
                Country = "Česká republika",
                Latitude = "",
                Longitude = ""
            };

            //var offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
            db.OfficeDetails.Add(newOffice);
            db.SaveChanges();

            CreateDefaultCalendarSettings(userId, newOffice.Id);

            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Office added", null);

            Session["AddOfficeButtonDisabled"] = 1;

            return Redirect("/Admin/Office");
        }

        [Authorize]
        public ActionResult DeleteOffice(bool confirm, int? officeId)
        {
            // if user confirm to delete then this action will fire
            // and you can pass true value. If not, then it is already not confirmed.

            if (officeId != null && officeId != 0)
            {
                var office = db.OfficeDetails.Where(x => x.Id == officeId).FirstOrDefault();
                if (office != null)
                {
                    string id = office.Id.ToString();
                    string userId = User.Identity.GetUserId();
                    db.OfficeDetails.Remove(office);
                    var cachedResult = db.CachedResults.Where(x => x.OfficeId == office.Id).FirstOrDefault();
                    if (cachedResult != null)
                        db.CachedResults.Remove(cachedResult);
                    db.SaveChanges();

                    logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Office deleted", "Office Id - " + id);

                    var calendarSettings = db.CalendarSettings.FirstOrDefault(x => x.OfficeId == office.Id);
                    if (calendarSettings != null)
                    {
                        Session["AddOfficeButtonDisabled"] = null;

                        db.CalendarSettings.Remove(calendarSettings);
                        db.SaveChanges();

                        logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Calendar settings deleted", "Calendar settintgs Id - " + calendarSettings.Id);
                    }
                }
            }

            return Redirect("/Admin/Office");
        }

        [Authorize]
        public ActionResult DeleteProfile(string userId)
        {
            if (userId == null)
                return View("/Admin/DeleteProfileError");

            string email = UserManager.GetEmail(userId);
            SendDeleteProfileEmail(userId, email);

            return View();
        }

        [Authorize]
        public ActionResult DeletePatientProfile(string userId)
        {
            if (userId == null)
                return View("/Admin/DeletePaientProfileError");

            string email = UserManager.GetEmail(userId);
            SendDeletePatientProfileEmail(userId, email);

            return View();
        }

        private async Task<GeoPoint> GetPoint(string _address)
        {
            GeoPoint location = new GeoPoint();

            string address = _address;
            //string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key={1}", Uri.EscapeDataString(address), Globals.GoogleMapsApiKey);
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&clientID=248882027328-gqc1ku1dqtl6pe7fqga07sr97uadb5mp.apps.googleusercontent.com?signature=M6p7eBYffuS2D4yWtgE8IOsD", address);

            try
            {
                using (var client = new HttpClient())
                {
                    var request = await client.GetAsync(requestUri);
                    var content = await request.Content.ReadAsStringAsync();
                    var xmlDocument = XDocument.Parse(content);

                    XElement status = xmlDocument.Element("GeocodeResponse").Element("status");
                    XElement errorMessage = xmlDocument.Element("GeocodeResponse").Element("error_message");
                    if (status.Value == "OK")
                    {
                        try
                        {
                            XElement result = xmlDocument.Element("GeocodeResponse").Element("result");
                            if (result != null)
                            {
                                XElement locationElement = result.Element("geometry").Element("location");
                                XElement lat = locationElement.Element("lat");
                                XElement lng = locationElement.Element("lng");
                                location.Latitude = lat.Value;
                                location.Longitude = lng.Value;
                                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Get Point - successful: " + _address, "");
                            }
                            else
                                logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "Get Point - no result from googlemaps api: " + _address, "");
                        }
                        catch (Exception ex)
                        {
                            location.Latitude = "0.0";
                            location.Longitude = "0.0";
                            location.error = ex.Message;
                            logger.Log(DateTime.Now, Logger.Level.Exception, "Anonymous user", "Get Point - address: " + _address + " - " + ex.Message, ex.InnerException.ToString());
                        }
                    }
                    else if (status.Value == "ZERO_RESULTS")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "ZERO_RESULTS " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "OVER_QUERY_LIMIT")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "OVER_QUERY_LIMIT " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "INVALID_REQUEST")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "INVALID_REQUEST " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "REQUEST_DENIED")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "REQUEST_DENIED " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "UNKNOWN_ERROR")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "UNKNOWN_ERROR " + _address, errorMessage != null ? errorMessage.Value : "");
                }

                return location;
            }
            catch (Exception ex)
            {
                logger.Log(DateTime.Now, Logger.Level.Error, "Anonymous user", "google maps api - no connection", ex.Message);
                return null;
            }
        }


        public ActionResult DetailView(string UserId)
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Billing()
        {
            string userId = User.Identity.GetUserId();

            var m = TempData["InvoiceData"] as InvoiceDetailModel;

            var detail = db.MedicInvoiceDetails.Where(x => x.UserId == userId);
            if (detail == null || detail.ToList().Count == 0)
            {
                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    UserId = userId
                };

                if (m != null)
                {
                    model.Address = m.Address;
                    model.BankAccount = m.BankAccount;
                    model.Email = m.Email;
                    model.IdentifyNo = m.IdentifyNo;
                    model.Name = m.Name;
                    model.Phone = m.Phone;
                    model.TaxIdentifyNo = m.TaxIdentifyNo;
                    model.UserId = m.UserId;
                    model.ProfileImage = GetProfileImage(Globals.ProfileImagesPath);
                    model.Postfix = m.Postfix;
                    model.Prefix = m.Prefix;
                }

                model.PostfixList = InitPostfixList();

                if (model.Postfix == null)
                    model.Postfix = "";
                if (model.PhonePrefix == null)
                    model.PhonePrefix = "";
                model.PhonePrefixLength = ph.GetPhonePrefixLength(model.Phone);
                model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);

                return View(model);
            }
            else
            {
                MedicInvoiceDetail det = detail.ToList()[0];
                InvoiceDetailModel model = new InvoiceDetailModel()
                {
                    Id = det.Id,
                    UserId = det.UserId,
                    Name = det.Name,
                    Address = det.Address,
                    Phone = ph.GetPhonePure(det.Phone),
                    Email = det.Email,
                    IdentifyNo = det.IdentifyNo,
                    TaxIdentifyNo = det.TaxIdentifyNo,
                    Prefix = GetPrefixFromBankAccount(det.BankAccount),
                    BankAccount = GetBankAccount(det.BankAccount),
                    Postfix = GetPostfixFromBankAccount(det.BankAccount),
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath),
                    PhonePrefix = ph.GetPhonePrefix(det.Phone),
                    Accept = true
                };

                if (m != null)
                {
                    model.Address = m.Address;
                    model.Prefix = m.Prefix;
                    model.BankAccount = m.BankAccount;
                    model.Postfix = m.Postfix;
                    model.Email = m.Email;
                    model.IdentifyNo = m.IdentifyNo;
                    model.Name = m.Name;
                    model.Phone = m.Phone;
                    model.TaxIdentifyNo = m.TaxIdentifyNo;
                    model.UserId = m.UserId;
                }

                model.PhonePrefixList = ph.InitPhonePrefixList(det.Phone);
                model.PhonePrefixLength = ph.GetPhonePrefixLength(det.Phone);
                model.PostfixList = InitPostfixList();

                return View(model);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Billing(InvoiceDetailModel model)
        {
            if (ModelState.IsValid)
            {
                if(String.IsNullOrEmpty(model.Postfix) == false && String.IsNullOrEmpty(model.BankAccount) == true)
                {
                    model.ErrorMessage = "Bankovní účet není vyplněný";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }
                else if(String.IsNullOrEmpty(model.Postfix) == true && String.IsNullOrEmpty(model.BankAccount) == false)
                {
                    model.ErrorMessage = "Koncovka bankovního účtu není vyplněná";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }
                else if(String.IsNullOrEmpty(model.Prefix) == false && (String.IsNullOrEmpty(model.Postfix) == true || String.IsNullOrEmpty(model.BankAccount) == true))
                {
                    model.ErrorMessage = "Číslo bankovního účtu není kompletní";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }

                var item = db.PhonePrefix.Where(x => x.Text == model.PhonePrefix).FirstOrDefault();
                int phoneLength = 9;
                if (item != null)
                    phoneLength = item.Length;

                if(ph.GetPhonePure(model.Phone).Length != phoneLength && ph.GetPhonePure(model.Phone).Length != 0)
                {
                    model.ErrorMessage = "Délka telefonního čísla není platná";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }

                if(validator.IsBankAccountValid(model.Prefix, model.BankAccount) == false)
                {
                    model.ErrorMessage = "Číslo bankovního účtu obsahuje chyby";
                    model.PostfixList = InitPostfixList();
                    model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
                    model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

                    return View(model);
                }

                string userId = User.Identity.GetUserId();

                var detail = db.MedicInvoiceDetails.Where(x => x.Id == model.Id);
                if (detail == null || detail.ToList().Count == 0)
                {
                    MedicInvoiceDetail det = new MedicInvoiceDetail()
                    {
                        Id = model.Id,
                        UserId = model.UserId,
                        Name = model.Name,
                        Address = model.Address,
                        IdentifyNo = model.IdentifyNo,
                        TaxIdentifyNo = model.TaxIdentifyNo,
                        Phone = String.IsNullOrEmpty(model.Phone) ? "" : model.PhonePrefix + "-" + model.Phone,
                        Email = model.Email,
                        BankAccount = (String.IsNullOrEmpty(model.Prefix) == false ? model.Prefix + "-" : "") + (String.IsNullOrEmpty(model.BankAccount) == true ? "" : model.BankAccount + "/" + model.Postfix)
                    };
                    db.MedicInvoiceDetails.Add(det);
                }
                else
                {
                    MedicInvoiceDetail det = db.MedicInvoiceDetails.Find(model.Id);
                    if (det != null)
                    {
                        det.UserId = model.UserId;
                        det.Name = model.Name;
                        det.Address = model.Address;
                        det.IdentifyNo = model.IdentifyNo;
                        det.TaxIdentifyNo = model.TaxIdentifyNo;
                        det.Phone = String.IsNullOrEmpty(model.Phone) ? "" : model.PhonePrefix + "-" + model.Phone;
                        det.Email = model.Email;
                        det.BankAccount = (String.IsNullOrEmpty(model.Prefix) == false ? model.Prefix + "-" : "") + (String.IsNullOrEmpty(model.BankAccount) == true ? "" : model.BankAccount + "/" + model.Postfix);
                    }
                }

                int res = db.SaveChanges();
                return Redirect("/Admin/HomeBilling");
            }

            model.PostfixList = InitPostfixList();
            model.PhonePrefixList = ph.InitPhonePrefixList(model.Phone);
            model.PhonePrefixLength = ph.GetPhonePrefixLength(model.PhonePrefix + "-" + model.Phone);

            return View(model);
        }

        private string GetPrefixFromBankAccount(string account)
        {
            string ret = "";
            if (account == null)
                return ret;

            if(account.Contains("-"))
            {
                int pos = account.IndexOf("-");
                ret = account.Substring(0, pos);
            }

            return ret;
        }

        private string GetBankAccount(string account)
        {
            string ret = "";
            if (account == null)
                return ret;

            if (account.Contains("-"))
            {
                int pos = account.IndexOf("-");
                account = account.Substring(pos+1);
            }

            if(account.Contains("/"))
            {
                int pos = account.LastIndexOf("/");
                ret = account.Substring(0, pos);
            }

            return ret;
        }

        private string GetPostfixFromBankAccount(string account)
        {
            string ret = "";
            if (account == null)
                return ret;

            if (account.Contains("/"))
            {
                int pos = account.LastIndexOf("/");
                ret = account.Substring(pos+1);
            }

            return ret;
        }

        private List<SelectListItem> InitPostfixList()
        {
            List<SelectListItem> prefixList = new List<SelectListItem>();
            var bankAccountList = db.BankAccountPostfix.ToList();

            foreach (BankAccountPostfix postfix in bankAccountList)
            {
                SelectListItem item = new SelectListItem();
                string str = postfix.Postfix.ToString();
                if (str.Length != 4)
                    str = "0" + str;
                item.Value = str;
                item.Text = str;
                prefixList.Add(item);
            }

            return prefixList;
        }

    [Authorize]
        public ActionResult Pozvani()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Pozvani(InviteMedicModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    ModelState.AddModelError("", "Neplatný e-mail");
                    logger.Log(DateTime.Now, Logger.Level.Warning, "No user id", "Pozvani", null);
                    return View();
                }


                //DataEncryptor keys = new DataEncryptor();
                //var iv = keys.IV;
                //var key = keys.Key;
                //string hash = keys.EncryptString(model.Email);
                //string decrypt = keys.DecryptString(encrypt);

                string hash = "";
                using (SHA256 shaHash = SHA256.Create())
                {
                    hash = GetSha256Hash(shaHash, model.Email);
                }

                if (String.IsNullOrEmpty(hash))
                    return View(); // ERROR

                var invitations = db.MedicInvitations.Where(x => x.Email == model.Email).ToList();
                if (invitations != null && invitations.Count > 0)
                {
                    ModelState.AddModelError("", "Tento e-mail byl již použitý.");
                    logger.Log(DateTime.Now, Logger.Level.Warning, "No user id", "Pozvani", "Tento e-mail byl již použitý");
                    return View(); //  allready exists
                }

                MedicInvitation invitation = new MedicInvitation()
                {
                    UserId = user.Id,
                    Email = model.Email,
                    Hash = hash,
                    Date = DateTime.Now,
                    Active = true
                };

                //https://stackoverflow.com/questions/4057748/save-byte-into-a-sql-server-database-from-c-sharp

                //try
                //{
                //    var code = UserManager.GeneratePasswordResetTokenAsync(user.Id);
                //    string codeStr = System.Web.HttpUtility.UrlEncode(code.Result);
                //    var callbackUrl = Url.Action("ResetOldPassword", "Account", new { UserId = user.Id, code = codeStr }, protocol: Request.Url.Scheme);

                //    //await UserManager.SendEmailAsync(user.Id, "Obnova hesla", "Heslo obnovíte kliknutím zde: <a href=\"" + callbackUrl + "\">link</a>");


                string body = @"Vážená paní, vážený pane,
                
";
                body += "http://localhost:51672/Admin/RegisterMedic?code=";
                body += hash;
                body += @"

                Váš SOS Body team
                ";
                MailMessage message = new MailMessage("info@sosbody.cz", user.Email, "Pozvánka do SOS Body", body);
                //    message.IsBodyHtml = true;
                //    SmtpClient client = new SmtpClient("smtp.zoner.com");
                //    client.Send(message);
                //}
                //catch (Exception ex)
                //{
                //    string y = ex.Message;
                //}

                db.MedicInvitations.Add(invitation);
                db.SaveChanges();

                logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "Pozvani", "Medic invited - " + invitation.Email);

                return View("PozvaniPotvrzeno");
            }

            return View();
        }


        [Authorize]
        public ActionResult PozvaniPotvrzeno()
        {
            return View();
        }

        private string GetSha256Hash(SHA256 shaHash, string input)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = shaHash.ComputeHash(Encoding.UTF8.GetBytes(input));
            

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private DateTime FirstDateOfWeekISO8601(int year, int weekOfYear, int hour, int minute)
        {
            DateTime jan1 = new DateTime(year, 1, 1, hour, minute, 0);
            int daysOffset = DayOfWeek.Monday - jan1.DayOfWeek;

            DateTime firstMonday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstMonday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstMonday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        [Authorize]
        public async Task<ActionResult> GoogleCalendar()
        {
            if (Session.Contents.Count > 0)
            {
                if (Session["loginWith"] != null)
                {
                    if (Session["loginWith"].ToString() == "google")
                    {
                        try
                        {
                            var url = Request.Url.Query;
                            if (url != "")
                            {
                                string queryString = url.ToString();
                                char[] delimiterChars = { '=' };
                                string[] words = queryString.Split(delimiterChars);
                                string code = words[1];

                                if (code != null)
                                {
                                    //get the access token 
                                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                                    webRequest.Method = "POST";
                                    Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(Parameters);
                                    webRequest.ContentType = "application/x-www-form-urlencoded";
                                    webRequest.ContentLength = byteArray.Length;
                                    Stream postStream = webRequest.GetRequestStream();
                                    // Add the post data to the web request
                                    postStream.Write(byteArray, 0, byteArray.Length);
                                    postStream.Close();

                                    WebResponse response = webRequest.GetResponse();
                                    postStream = response.GetResponseStream();
                                    StreamReader reader = new StreamReader(postStream);
                                    string responseFromServer = reader.ReadToEnd();

                                    GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                                    if (serStatus != null)
                                    {
                                        string accessToken = string.Empty;
                                        accessToken = serStatus.access_token;

                                        if (!string.IsNullOrEmpty(accessToken))
                                        {
                                            // This is where you want to add the code if login is successful.
                                            //getgoogleplususerdataSer(accessToken);

                                            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                                               new ClientSecrets
                                               {
                                                   ClientId = googleplus_client_id,
                                                   ClientSecret = googleplus_client_secret,
                                               },
                                               new[] { CalendarService.Scope.Calendar },
                                               "user",
                                               CancellationToken.None).Result;


                                            // Create the service.
                                            service = new CalendarService(new BaseClientService.Initializer()
                                            {
                                                HttpClientInitializer = credential,
                                                ApplicationName = "Calendar API Sample",
                                            });

                                            //TempData["GoogleService"] = service;
                                            Session["GoogleService"] = service;

                                            return Redirect("/Admin/Calendar");


                                            //// Define parameters of request.
                                            //EventsResource.ListRequest request = service.Events.List("primary");
                                            //request.TimeMin = DateTime.Now;
                                            //request.ShowDeleted = false;
                                            //request.SingleEvents = true;
                                            //request.MaxResults = 10;
                                            //request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                                            //// List events.
                                            //Events events = request.Execute();
                                            //List<Event> sosBodyEventList = new List<Event>();

                                            //if (events.Items != null && events.Items.Count > 0)
                                            //{
                                            //    foreach (var eventItem in events.Items)
                                            //    {
                                            //        if (eventItem.Summary != null && eventItem.Summary.ToUpper() == "SOSBODY")
                                            //            sosBodyEventList.Add(eventItem);

                                            //        //string when = eventItem.Start.DateTime.ToString();
                                            //        //if (String.IsNullOrEmpty(when))
                                            //        //{
                                            //        //    when = eventItem.Start.Date;
                                            //        //}
                                            //        //Console.WriteLine("{0} ({1})", eventItem.Summary, when);
                                            //    }
                                            //}
                                        }
                                        else
                                        { }
                                    }
                                    else
                                    { }
                                }
                                else
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            string userId = User.Identity.GetUserId();
                            logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Google Calendar - " + ex.Message, ex.InnerException.ToString());
                            //throw new Exception(ex.Message, ex);
                            //Response.Redirect("index.aspx");
                        }
                    }
                }
                else
                {
                    var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
                    Session["loginWith"] = "google";
                    Response.Redirect(Googleurl);
                }
            }
            else
            {
                //http://localhost:60531/Account/Login
                //https://stackoverflow.com/questions/39674841/unable-to-get-access-and-refresh-token-after-authenticating-with-google-api


                //UserCredential credential;
                //string[] Scopes = { CalendarService.Scope.CalendarReadonly };


                //using (var stream =
                //    new FileStream("~/bin/client_id.json", FileMode.Open, FileAccess.Read))
                //{
                //    string credPath = System.Environment.GetFolderPath(
                //        System.Environment.SpecialFolder.Personal);
                //    credPath = Path.Combine(credPath, ".credentials/calendar-dotnet-quickstart.json");

                //    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                //        GoogleClientSecrets.Load(stream).Secrets,
                //        Scopes,
                //        "user",
                //        CancellationToken.None,
                //        new FileDataStore(credPath, true)).Result;
                //    Console.WriteLine("Credential file saved to: " + credPath);
                //}

                //https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=583306224539-atbcaa8ne8g85e8kc006o6vmq99qiid0.apps.googleusercontent.com&as=-3cafe9258bcbe40a&nosignup=1&destination=http%3A%2F%2Flocalhost&approval_state=!ChRHM1o2X21aa2ZkSUxYM29zd2liRhIfODI3WXJUd3RraWtSWUFOYVJfLXV5LTJYUnczVkNoWQ%E2%88%99ACThZt4AAAAAWkpb8764NJuyf7cw5Id-dRJ70VDwlm3S&xsrfsig=AHgIfE-ijOvu6cB7RYhEGdVp5IpWdVTNkg&flowName=GeneralOAuthFlow

                //https://developers.google.com/identity/protocols/OAuth2

                //https://accounts.google.com/o/oauth2/v2/auth?redirect_uri=https%3A%2F%2Fdevelopers.google.com%2Foauthplayground&prompt=consent&response_type=code&client_id=407408718192.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline


                //var Googleurl = "https://accounts.google.com/o/oauth2/v2/auth?redirect_uri=" + googleplus_redirect_url + "&prompt=consent&response_type=code&client_id= " + googleplus_client_id;// 407408718192.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&access_type=offline";
                //var Googleurl = "https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=407408718192.apps.googleusercontent.com&as=-5c29cc67539697aa&destination=https%3A%2F%2Fdevelopers.google.com&approval_state=!ChRjSGt0dGRLMzJHa1JCTEprRXFaZhIfODZ3aURJS0l6SUFiWUFOYVJfLXV5LTEzNzZzNkFoWQ%E2%88%99AHw7d_cAAAAAWicfAAQvbfy8hLHDgTViNcfshdGktseG&xsrfsig=AHgIfE-CW1odWgnz9oe_XueC8T4t7vilUw&flowName=GeneralOAuthFlow
                var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
                Session["loginWith"] = "google";
                Response.Redirect(Googleurl);
            }

            return View();
        }

        private async void getgoogleplususerdataSer(string access_token)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //catching the exception
                string userId = User.Identity.GetUserId();
                logger.Log(DateTime.Now, Logger.Level.Exception, userId, "getgoogleplususerdataSer - " + ex.Message, ex.InnerException.ToString());
            }
        }

        public ActionResult GetNewTermAvailability(string officeId, string calTime, string calDuration, string calWeek, string calYear, string calDayHourIndex)
        {
            int hour, minute;

            if (calTime.Contains(":30") == true)
            {
                int p = calTime.IndexOf(":");
                string hourStr = calTime.Substring(0, p);
                string minuteStr = calTime.Substring(p + 1);
                hour = Convert.ToInt32(hourStr);
                minute = Convert.ToInt32(minuteStr);
            }
            else
            {
                minute = 0;
                hour = Convert.ToInt32(calTime);
            }

            DateTime date = new DateTime();
            int dayIndex, hourIndex, year, week;
            int pos = calDayHourIndex.IndexOf('_');
            dayIndex = Convert.ToInt32(calDayHourIndex.Substring(0, pos));
            hourIndex = Convert.ToInt32(calDayHourIndex.Substring(pos + 1));
            year = Convert.ToInt32(calYear);
            week = Convert.ToInt32(calWeek);

            CultureInfo cul = CultureInfo.CreateSpecificCulture("cs-CZ");

            DateTime dd = DateTime.Now;
            dd = dd.AddDays(7 * (week));

            var firstDateWeek = dd.AddDays(-((dd.DayOfWeek - cul.DateTimeFormat.FirstDayOfWeek + 7) % 7)).Date;
            DateTime lastDateWeek = firstDateWeek.AddDays(7);

            firstDateWeek = firstDateWeek.AddDays(dayIndex);
            date = new DateTime(firstDateWeek.Year, firstDateWeek.Month, firstDateWeek.Day, hour, minute, 0);

            string userId = User.Identity.GetUserId();
            int OfficeId = Convert.ToInt32(officeId);
            var dates = db.Calendar.Where(x => x.UserId == userId).Where(x => x.Date.Year == date.Year && x.Date.Month == date.Month && x.Date.Day == date.Day).OrderBy(x => x.Date).ToList();

            int maxDuration = 90;
            int selectedDuration = Convert.ToInt16(calDuration);
            int startTime = 0;
            int endTime = 30;
            string selectedTime = calTime;
            if(calTime.Contains(':') && calTime.Length > 2)
                selectedTime = calTime.Substring(calTime.Length - 2, 2);

            DateTime newStart = date;
            DateTime newEnd = date.AddMinutes(selectedDuration);

            foreach (SOSBody.Calendar cal in dates)
            {
                DateTime exStart = cal.Date;
                DateTime exEnd = cal.Date.AddMinutes(cal.Duration);

                TimeSpan eSnS = exStart.Subtract(newStart);
                TimeSpan eSnE = exStart.Subtract(newEnd);
                TimeSpan eEnS = exEnd.Subtract(newStart);
                TimeSpan eEnE = exEnd.Subtract(newEnd);

                int s1 = (int)eSnS.TotalMinutes;
                int s2 = (int)eSnE.TotalMinutes;
                int s3 = (int)eEnS.TotalMinutes;
                int s4 = (int)eEnE.TotalMinutes;


                if(s1 >= 0 && s2 >= 0 && s3 >= 0 && s4 >= 0)
                {
                    if(s1 < maxDuration)
                        maxDuration = s1;
                    if (maxDuration == 0)
                        maxDuration = 30;
                    if (endTime >= maxDuration)
                        endTime = startTime;
                    continue;
                }
                else if (s1 <= 0 && s2 <= 0 && s3 <= 0 && s4 <= 0)
                {
                    startTime = newStart.Minute;
                    selectedTime = newStart.Minute.ToString();
                    continue;
                }
                else if (s1 > 0 && s2 < 0)
                {
                    maxDuration = s1;
                    selectedDuration = s1;
                    endTime = s1;
                }
                else if(s3 > 0 && s4 < 0)
                {
                    startTime = s3;
                }
                else if (s1 == 0)
                {
                    startTime = s3;
                    selectedTime = s3.ToString();
                    newStart = newStart.AddMinutes(s3);
                    newEnd = newEnd.AddMinutes(s3);
                }

                //TimeSpan span12 = exEnd.Subtract(newStart);
                //if (span12.TotalMinutes > 0)
                //{
                //    TimeSpan span22 = exStart.Subtract(newStart);

                //    if ((int)span12.TotalMinutes > selectedDuration)
                //    {
                //        if (span22.TotalMinutes > 0)
                //        {
                //            int y = 0;
                //        }
                //        else
                //        {
                //            int y = 0;
                //        }
                //    }
                //    else
                //    {
                //        //TimeSpan span22 = exStart.Subtract(newStart);
                //        if (span22.TotalMinutes > 0)
                //        {
                //            int y = 0;
                //        }
                //        else
                //        {
                //            int y = 0;
                //        }
                //        //newStart = newStart.AddMinutes((int)span12.TotalMinutes);
                //        //newEnd = newEnd.AddMinutes((int)span12.TotalMinutes);
                //    }
                //}
                //else
                //{
                //    // OK
                //    // exEnd sooner than newStart
                //    continue;
                //}

                //TimeSpan span21 = exStart.Subtract(newEnd);
                //if (span21.TotalMinutes >= 0)
                //{
                //    // OK
                //    // newEnd sooner than exStart
                //    continue;
                //}
                //else
                //{
                //    newEnd = newEnd.AddMinutes((int)span21.TotalMinutes);
                //    maxDuration += (int)span21.TotalMinutes;
                //    selectedDuration = maxDuration;
                //}


                //DateTime actionDate = date;
                //DateTime d = cal.Date;
                //int durationInMinutes = /*minute +*/ Convert.ToInt32(cal.Duration);
                //int durationInHours = 0;
                //if (durationInMinutes == 30)
                //{
                //    durationInHours = 0;
                //    durationInMinutes = 30;
                //}
                //else if (durationInMinutes == 60)
                //{
                //    durationInHours = 1;
                //    durationInMinutes = 0;
                //}
                //else if (durationInMinutes == 90)
                //{
                //    durationInHours = 1;
                //    durationInMinutes = 30;
                //}
                //if (d <= date) // before
                //{
                //    d = d.AddHours(durationInHours);
                //    d = d.AddMinutes(durationInMinutes);
                //    TimeSpan span = d.Subtract(date);
                //    int minuteDist = /*Math.Abs(*/(int)span.TotalMinutes;
                //    selectedTime = d.Minute.ToString();
                //}
                //else // after
                //{
                //    TimeSpan span = actionDate.Subtract(cal.Date);
                //    int minuteDist = Math.Abs((int)span.TotalMinutes);
                //    if (minuteDist < maxDuration)
                //        maxDuration = minuteDist;
                //    if (selectedDuration > maxDuration)
                //        selectedDuration = maxDuration; 
                //}
            }

            //if ((startTime + selectedDuration) > endTime)
            //endTime = startTime;

            return Json(new { maxDuration = maxDuration, selectedDuration = selectedDuration , startTime = startTime, endTime = endTime, selectedTime = selectedTime, hour = date.Hour.ToString()});
        }

        public ActionResult SaveAction(string officeId, string calTime, string calDuration, string calText, string calWeek, string calYear, string calDayHourIndex, bool calIsSOSbody, string calPrice)
        {
            string userId = User.Identity.GetUserId();

            int hour, minute;

            if(calTime.Contains(":30") == true)
            {
                int p = calTime.IndexOf(":");
                string hourStr = calTime.Substring(0, p);
                string minuteStr = calTime.Substring(p + 1);
                hour = Convert.ToInt32(hourStr);
                minute = Convert.ToInt32(minuteStr);
            }
            else
            {
                minute = 0;
                hour = Convert.ToInt32(calTime);
            }

            decimal price = Convert.ToDecimal(calPrice);

            int dayIndex, hourIndex, year, week;
            int pos = calDayHourIndex.IndexOf('_');
            dayIndex = Convert.ToInt32(calDayHourIndex.Substring(0, pos));
            hourIndex = Convert.ToInt32(calDayHourIndex.Substring(pos + 1));
            year = Convert.ToInt32(calYear);
            week = Convert.ToInt32(calWeek);
            int durationInMinutes = Convert.ToInt32(calDuration);
            int durationInHours = 0;
            if(durationInMinutes == 30)
            {
                durationInHours = 0;
                durationInMinutes = 0;
            }
            else if(durationInMinutes == 60)
            {
                durationInHours = 1;
                durationInMinutes = 0;
            }
            else if(durationInMinutes == 90)
            {
                durationInHours = 1;
                durationInMinutes = 30;
            }
            //else if(durationInMinutes == 120)
            //{
            //    durationInHours = 2;
            //    durationInMinutes = 0;
            //}

            CultureInfo cul = CultureInfo.CreateSpecificCulture("cs-CZ");

            DateTime dd = DateTime.Now;
            dd = dd.AddDays(7 * (week));

            var firstDateWeek = dd.AddDays(-((dd.DayOfWeek - cul.DateTimeFormat.FirstDayOfWeek + 7) % 7)).Date;
            DateTime lastDateWeek = firstDateWeek.AddDays(7);

            firstDateWeek = firstDateWeek.AddDays(dayIndex);
            DateTime date = new DateTime(firstDateWeek.Year, firstDateWeek.Month, firstDateWeek.Day, hour, minute, 0);

            SOSBody.Calendar model = new SOSBody.Calendar()
            {
                UserId = userId,
                IsSOSbody = calIsSOSbody,
                Date = date,
                Duration = Convert.ToDouble(calDuration),
                Info = calText,
                OfficeId = Convert.ToInt32(officeId),
                Price = price,
                Inserted = DateTime.Now
            };

            try
            {
                db.Calendar.Add(model);
                db.SaveChanges();
                logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Save action", "Action saved - action id: " + model.Id);
            }
            catch (Exception ex)
            {
                logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Save Action - " + ex.Message, ex.InnerException.ToString());
            }

            if ((date.Minute + durationInMinutes) == 60)
            {
                durationInHours++;
                durationInMinutes = 0;
            }
            // Event for google calendar
            Event newEvent = new Event()
            {
                Summary = calIsSOSbody == true ? "sosbody" : "",
                Description = calText,
                Start = new EventDateTime()
                {
                    DateTime = date,
                    TimeZone = "Europe/Prague"
                },
                End = new EventDateTime()
                {
                    DateTime = new DateTime(date.Year, date.Month, date.Day, date.Hour + durationInHours, date.Minute + durationInMinutes, 0),
                    TimeZone = "Europe/Prague"
                }
            };

            if(service == null)
            {
                service = Session["GoogleService"] as CalendarService;
            }

            String calendarId = "primary";
            if (service != null)
            {
                EventsResource.InsertRequest request = service.Events.Insert(newEvent, calendarId);
                Event createdEvent = request.Execute();
            }

            string url = "Calendar?weekIndex=" + week + "&calIndex=" + officeId; 
            return Redirect(url);
        }

        public ActionResult RemoveAction(int id)
        {
            try
            {
                var action = db.Calendar.Where(x => x.Id == id).FirstOrDefault();
                if (action != null)
                {
                    db.Calendar.Remove(action);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string userId = User.Identity.GetUserId();
                logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Remove Action - " + ex.Message, ex.InnerException.ToString());
            }

            return Redirect("Calendar");
        }

        public ActionResult ChangeAction(string officeId, string calTime, string calDuration, string calText, string calWeek, bool calIsSOSbody, int id, string calPrice)
        {
            var action = db.Calendar.Find(id);

            int hour, minute;

            if (calTime.Contains(":30") == true)
            {
                int p = calTime.IndexOf(":");
                string hourStr = calTime.Substring(0, p);
                string minuteStr = calTime.Substring(p + 1);
                hour = Convert.ToInt32(hourStr);
                minute = Convert.ToInt32(minuteStr);
            }
            else
            {
                minute = 0;
                hour = Convert.ToInt32(calTime);
            }

            if (action != null)
            {
                double duration = Convert.ToDouble(calDuration);
                action.Duration = duration;
                action.Info = calText;
                action.Date = new DateTime(action.Date.Year, action.Date.Month, action.Date.Day, action.Date.Hour, minute, 0);
                action.IsSOSbody = calIsSOSbody;
                action.Price = Convert.ToDecimal(calPrice);
                action.OfficeId = Convert.ToInt16(officeId);
                db.SaveChanges();
            }

            return Redirect("Calendar");
        }

        private CalendarSettings GetDefaultCalendarSettings()
        {
            CalendarSettings calSettings = new CalendarSettings()
            {
                Monday = true,
                Tuesday = true,
                Wednesday = true,
                Thursday = true,
                Friday = true,
                Saturday = true,
                Sunday = true,
                MondayStartHour = 8,
                MondayEndHour = 18,
                TuesdayStartHour = 8,
                TuesdayEndHour = 18,
                WednesdayStartHour = 8,
                WednesdayEndHour = 18,
                ThursdayStartHour = 8,
                ThursdayEndHour = 18,
                FridayStartHour = 8,
                FridayEndHour = 18,
                SaturdayStartHour = 8,
                SaturdayEndHour = 18,
                SundayStartHour = 8,
                SundayEndHour = 18,
                Price30 = 0,
                Price60 = 0,
                Price90 = 0
            };

            return calSettings;
        }

        private CalendarModel GetCalendar(CalendarSettings calSettings, int weekIndex, int? calIndex)
        {
            string userId = User.Identity.GetUserId();

            var offices = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
            if (offices == null || offices.Count == 0)
                return null;

            //int currentCalendarId = calIndex ?? offices[0].Id;
            CultureInfo cul = CultureInfo.CreateSpecificCulture("cs-CZ");//.CurrentCulture;

            int dayIndex = 0;

            DateTime date = DateTime.Now;
            date = date.AddDays(7 * weekIndex);

            switch (cul.Calendar.GetDayOfWeek(date))
            {
                case DayOfWeek.Monday:
                    dayIndex = 0;
                    break;
                case DayOfWeek.Tuesday:
                    dayIndex = 1;
                    break;
                case DayOfWeek.Wednesday:
                    dayIndex = 2;
                    break;
                case DayOfWeek.Thursday:
                    dayIndex = 3;
                    break;
                case DayOfWeek.Friday:
                    dayIndex = 4;
                    break;
                case DayOfWeek.Saturday:
                    dayIndex = 5;
                    break;
                case DayOfWeek.Sunday:
                    dayIndex = 6;
                    break;
            }

            var firstDateWeek = date.AddDays(-((date.DayOfWeek - cul.DateTimeFormat.FirstDayOfWeek + 7) % 7)).Date;
            DateTime lastDateWeek = firstDateWeek.AddDays(7);

            CalendarModel model = new CalendarModel();
            model.StartHourIndex = GetCalMinValue(calSettings);
            model.EndHourIndex = GetCalMaxValue(calSettings);
            model.StartHours = new List<int>();
            model.StartHours.Add(calSettings.MondayStartHour);
            model.StartHours.Add(calSettings.TuesdayStartHour);
            model.StartHours.Add(calSettings.WednesdayStartHour);
            model.StartHours.Add(calSettings.ThursdayStartHour);
            model.StartHours.Add(calSettings.FridayStartHour);
            model.StartHours.Add(calSettings.SaturdayStartHour);
            model.StartHours.Add(calSettings.SundayStartHour);
            model.EndHours = new List<int>();
            model.EndHours.Add(calSettings.MondayEndHour);
            model.EndHours.Add(calSettings.TuesdayEndHour);
            model.EndHours.Add(calSettings.WednesdayEndHour);
            model.EndHours.Add(calSettings.ThursdayEndHour);
            model.EndHours.Add(calSettings.FridayEndHour);
            model.EndHours.Add(calSettings.SaturdayEndHour);
            model.EndHours.Add(calSettings.SundayEndHour);
            model.Days = new List<Day>();
            model.Year = date.Year;
            model.WeekIndex = weekIndex;
            model.Offices = new List<OfficeDetailModel>();
            //model.CurrentCalendarId = currentCalendarId;
            model.Price30 = new List<int>();//calSettings.Price30;
            model.Price60 = new List<int>();//calSettings.Price60;
            model.Price90 = new List<int>();//calSettings.Price90;

            foreach (OfficeDetail office in offices)
            {
                OfficeDetailModel officeModel = new OfficeDetailModel()
                {
                    Id = office.Id,
                    UserId = office.UserId,
                    Name = office.Name,
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    PostalCode = office.PostalCode,
                    City = office.City,
                    Country = office.Country,
                    Latitude = office.Latitude,
                    Longitude = office.Longitude,
                    Phone = office.Phone
                };
                model.Offices.Add(officeModel);

                var calSet = db.CalendarSettings.Where(x => x.OfficeId == office.Id).FirstOrDefault();
                if(calSet != null)
                {
                    model.Price30.Add(calSet.Price30);
                    model.Price60.Add(calSet.Price60);
                    model.Price90.Add(calSet.Price90);
                }
            }
            model.OfficeCount = model.Offices.Count;

            var calendar = db.Calendar.Where(x => x.UserId == userId).Where(x => x.Date > firstDateWeek).Where(x => x.Date < lastDateWeek).OrderBy(x => x.Date).ToList(); //Where(x => x.OfficeId == currentCalendarId)

            DateTime dateToday = DateTime.Now;

            if(service != null)
            {
                EventsResource.ListRequest request = service.Events.List("primary");
                request.TimeMin = firstDateWeek;
                request.TimeMax = lastDateWeek;
                request.ShowDeleted = false;
                request.SingleEvents = true;
                request.MaxResults = 100;
                request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;


                try
                {
                    // List events.
                    Events events = request.Execute();

                }
                catch(Exception ex)
                {
                    logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Google calendar events inport failed", ex.Message);
                }
            }

            for (int i = 0; i < 7; i++)
            {
                Day day = new Day();
                DateTime tempDate = firstDateWeek.AddDays(i);
                day.DateStr = (tempDate.Day).ToString() + '.' + tempDate.Month.ToString() + '.';
                day.Actions = new List<Models.Action>();
                if (i == 5 || i == 6)
                    day.Weekend = true;
                else
                    day.Weekend = false;
                day.Index = i;
                day.Visible = GetDayVisibility(i, calSettings);
                day.IsToday = false;
                if(tempDate.Year == dateToday.Year && tempDate.Month == dateToday.Month && tempDate.Day == dateToday.Day)
                    day.IsToday = true;

                foreach (SOSBody.Calendar cal in calendar)
                {
                    int diff = cal.Date.Day - date.Day;
                    if(cal.Date.Month != date.Month)
                    {
                        if(diff < 0)
                        {
                            int month = cal.Date.Month - 1 > 0 ? cal.Date.Month - 1 : 1;
                            int days = cul.Calendar.GetDaysInMonth(date.Year, month);
                            diff += days;
                        }
                        else
                        {
                            int month = cal.Date.Month;
                            int days = cul.Calendar.GetDaysInMonth(date.Year, month);
                            diff -= days;
                        }
                    }

                    if (i == (dayIndex + diff))
                    {
                        Models.Action action = new Models.Action();
                        action.Id = cal.Id;
                        if (cal.IsSOSbody)
                            action.Info = "SOS Body ";
                        //action.Info += cal.Info;
                        action.StartIndex = cal.Date.Hour;
                        action.Duration = (float)(cal.Duration) / 60.0f;
                        action.StartPos = (float)(cal.Date.Minute) / 60.0f;
                        action.Price = Convert.ToInt32(cal.Price);
                        action.Booked = cal.Booked;
                        action.IsSosBody = cal.IsSOSbody;
                        day.Actions.Add(action);
                    }
                }
                model.Days.Add(day);
            }

            return model;
        }

        private int GetCalMaxValue(CalendarSettings settings)
        {
            int max = 0;
            if (settings.MondayEndHour > max)
                max = settings.MondayEndHour;
            if (settings.TuesdayEndHour > max)
                max = settings.TuesdayEndHour;
            if (settings.WednesdayEndHour > max)
                max = settings.WednesdayEndHour;
            if (settings.ThursdayEndHour > max)
                max = settings.ThursdayEndHour;
            if (settings.FridayEndHour > max)
                max = settings.FridayEndHour;
            if (settings.SaturdayEndHour > max)
                max = settings.SaturdayEndHour;
            if (settings.SundayEndHour > max)
                max = settings.SundayEndHour;

            return max;
        }

        private int GetCalMinValue(CalendarSettings settings)
        {
            int min = 24;
            if (settings.MondayStartHour < min)
                min = settings.MondayStartHour;
            if (settings.TuesdayStartHour < min)
                min = settings.TuesdayStartHour;
            if (settings.WednesdayStartHour < min)
                min = settings.WednesdayStartHour;
            if (settings.ThursdayStartHour < min)
                min = settings.ThursdayStartHour;
            if (settings.FridayStartHour < min)
                min = settings.FridayStartHour;
            if (settings.SaturdayStartHour < min)
                min = settings.SaturdayStartHour;
            if (settings.SundayStartHour < min)
                min = settings.SundayStartHour;

            return min;
        }

        private bool GetDayVisibility(int index, CalendarSettings calSettings)
        {
            bool visible = false;
            switch(index)
            {
                case 0:
                    visible = calSettings.Monday;
                    break;
                case 1:
                    visible = calSettings.Tuesday;
                    break;
                case 2:
                    visible = calSettings.Wednesday;
                    break;
                case 3:
                    visible = calSettings.Thursday;
                    break;
                case 4:
                    visible = calSettings.Friday;
                    break;
                case 5:
                    visible = calSettings.Saturday;
                    break;
                case 6:
                    visible = calSettings.Sunday;
                    break;
                default:
                    visible = false;
                    break;
            }

            return visible;
        }

        [Authorize]
        public ActionResult Calendar(int? weekIndex, int? calIndex)
        {
            //service = TempData["GoogleService"] as CalendarService;
            service = Session["GoogleService"] as CalendarService;

            string userId = User.Identity.GetUserId();
            CalendarSettings calSettings;
            var calendarSettings = db.CalendarSettings.Where(x => x.UserId == userId).ToList();
            if (calendarSettings == null || calendarSettings.Count == 0)   // Default calendar settings
            {
                var officeDetail = db.OfficeDetails.Where(x => x.UserId == userId).ToList();
                if (officeDetail == null || officeDetail.Count == 0)
                {
                    int officeId = CreateDefaultOfficeDetail(userId);
                    calSettings = CreateDefaultCalendarSettings(userId, officeId);
                }
                else
                {
                    logger.Log(DateTime.Now, Logger.Level.Error, userId, "No calendar settings and no office detail", null);
                    return Redirect("/Home/ErrorPage");
                }
            }
            else
                calSettings = calendarSettings[calendarSettings.Count - 1];

            if (calIndex != null)
                calSettings = db.CalendarSettings.Where(x => x.OfficeId == calIndex).ToList()[0];

            if (service == null && calSettings.GoogleCalendar == true)
                return Redirect("/Admin/GoogleCalendar");

            CultureInfo cul = CultureInfo.CreateSpecificCulture("cs-CZ");//.CurrentCulture;
            DateTime date = DateTime.Now;

            int weekNum = cul.Calendar.GetWeekOfYear(
                 date,
                 CalendarWeekRule.FirstDay,
                 DayOfWeek.Monday);

            int week = weekIndex ?? 0;

            var model = GetCalendar(calSettings, week, calIndex);

            return View(model);
        }

        private List<string> InitHeaderTimes()
        {
            List<string> header = new List<string>();

            for(int i = 0; i < 24; i++)
            {
                string str = i.ToString();
                str += ":00";

                header.Add(str);
            }

            return header;
        }

        [HttpPost]
        [Authorize]
        public ActionResult FileUpload(HttpPostedFileBase file)
        {
            string userId = User.Identity.GetUserId();
            string pathForSaving = Server.MapPath(Globals.ProfileImagesPath);

            if (file != null && file.ContentLength < 200000)
            {
                DeleteProfileImage(Globals.ProfileImagesPath, userId); // If exists

                string picName = System.IO.Path.GetFileName(file.FileName);
                int comaPos = picName.LastIndexOf('.');
                picName = userId + picName.Substring(comaPos);

                if (this.CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        file.SaveAs(Path.Combine(pathForSaving, picName));
                        logger.Log(DateTime.Now, Logger.Level.Trace, userId, "File Upload", "File saved as - " + picName);
                    }
                    catch (Exception ex)
                    {
                        logger.Log(DateTime.Now, Logger.Level.Exception, userId, "File Upload - " + ex.Message, ex.InnerException.ToString());
                    }
                }

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                //using (MemoryStream ms = new MemoryStream())
                //{
                //    file.InputStream.CopyTo(ms);
                //    byte[] array = ms.GetBuffer();
                //}

            }
            else if(file != null) // needs to be resized
            {
                DeleteProfileImage(Globals.ProfileImagesPath, userId); // If exists

                file.SaveAs(Path.Combine(pathForSaving, file.FileName));
                Image image = ResizePhoto(Path.Combine(pathForSaving, file.FileName), 200, 200);

                string picName = System.IO.Path.GetFileName(file.FileName);
                int comaPos = picName.LastIndexOf('.');
                picName = userId + picName.Substring(comaPos);
                image.Save(Path.Combine(pathForSaving, picName));

                logger.Log(DateTime.Now, Logger.Level.Trace, userId, "File Upload", "File resized - " + picName);

                DeleteProfileImage(Globals.ProfileImagesPath, file.FileName, true);
            }

            return Redirect("FormMedic");
        }

        [Authorize]
        public ActionResult DeleteProfileImage()
        {
            string userId = User.Identity.GetUserId();
            DeleteProfileImage(Globals.ProfileImagesPath, userId);

            return Redirect("FormMedic");
        }

        private void DeleteProfileImage(string dir, string filename)
        {
            string directory = Server.MapPath(dir);
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                int lastSlashPos = fileName.LastIndexOf('\\');
                string name = fileName.Substring(lastSlashPos + 1);
                int lastComaPos = name.LastIndexOf('.');
                string subName = name.Substring(0, lastComaPos);

                if (subName == filename)
                {
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);
                }
            }
        }

        private void DeleteProfileImage(string dir, string filename, bool withExtension)
        {
            string directory = Server.MapPath(dir);
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                int lastSlashPos = fileName.LastIndexOf('\\');
                string name = fileName.Substring(lastSlashPos + 1);
                if (withExtension == false)
                {
                    int lastComaPos = name.LastIndexOf('.');
                    name = name.Substring(0, lastComaPos);
                }

                if (name == filename)
                {
                    if (System.IO.File.Exists(fileName))
                        System.IO.File.Delete(fileName);
                }
            }
        }


        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    /*TODO: You must process this exception.*/
                    string userId = User.Identity.GetUserId();
                    logger.Log(DateTime.Now, Logger.Level.Exception, userId, "Create Folder If Needed - " + ex.Message, ex.InnerException.ToString());
                    result = false;
                }
            }
            return result;
        }

        private Image ResizePhoto(string fullName, int desiredWidth, int desiredHeight)
        {
            //throw error if bouning box is to small
            if (desiredWidth < 4 || desiredHeight < 4)
                throw new InvalidOperationException("Bounding Box of Resize Photo must be larger than 4X4 pixels.");
            var original = Bitmap.FromFile(fullName);

            //store image widths in variable for easier use
            var oW = (decimal)original.Width;
            var oH = (decimal)original.Height;
            var dW = (decimal)desiredWidth;
            var dH = (decimal)desiredHeight;

            //check if image already fits
            if (oW < dW && oH < dH)
                return original; //image fits in bounding box, keep size (center with css) If we made it biger it would stretch the image resulting in loss of quality.

            //check for double squares
            if (oW == oH && dW == dH)
            {
                //image and bounding box are square, no need to calculate aspects, just downsize it with the bounding box
                Bitmap square = new Bitmap(original, (int)dW, (int)dH);
                original.Dispose();
                return square;
            }

            //check original image is square
            if (oW == oH)
            {
                //image is square, bounding box isn't.  Get smallest side of bounding box and resize to a square of that center the image vertically and horizonatally with Css there will be space on one side.
                int smallSide = (int)Math.Min(dW, dH);
                Bitmap square = new Bitmap(original, smallSide, smallSide);
                original.Dispose();
                return square;
            }

            //not dealing with squares, figure out resizing within aspect ratios            
            if (oW > dW && oH > dH) //image is wider and taller than bounding box
            {
                var r = Math.Min(dW, dH) / Math.Min(oW, oH); //two demensions so figure out which bounding box demension is the smallest and which original image demension is the smallest, already know original image is larger than bounding box
                var nH = oW * r; //will downscale the original image by an aspect ratio to fit in the bounding box at the maximum size within aspect ratio.
                var nW = oW * r;
                var resized = new Bitmap(original, (int)nW, (int)nH);
                original.Dispose();
                return resized;
            }
            else
            {
                if (oW > dW) //image is wider than bounding box
                {
                    var r = dW / oW; //one demension (width) so calculate the aspect ratio between the bounding box width and original image width
                    var nW = oW * r; //downscale image by r to fit in the bounding box...
                    var nH = oW * r;
                    var resized = new Bitmap(original, (int)nW, (int)nH);
                    original.Dispose();
                    return resized;
                }
                else
                {
                    //original image is taller than bounding box
                    var r = dH / oH;
                    var nH = oH * r;
                    var nW = oW * r;
                    var resized = new Bitmap(original, (int)nW, (int)nH);
                    original.Dispose();
                    return resized;
                }
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult CreatePdf()
        {
            string path = CreatePdfFromTemplate();

            if(String.IsNullOrEmpty(path) == false)
                path = path.Replace("~", "..");

            return Json(new { pdf = path });
        }

        private string CreatePdfFromTemplate()
        {
            string userId = User.Identity.GetUserId();

            var medic = db.MedicDetails.Where(x => x.UserId == userId).FirstOrDefault();
            if (medic == null)
                return "";

            var invoice = db.MedicInvoiceDetails.Where(x => x.UserId == userId).FirstOrDefault();
            if (invoice == null)
                return "";

            PdfReader reader = new PdfReader(Server.MapPath("~/Doc/SOS_Body-Zprostredkovatelska_smlouva_template.pdf"));

            string outputFileName = "~/Doc/SOS_Body-Zprostredkovatelska_smlouva_" + userId + ".pdf";

            using (PdfStamper stamper = new PdfStamper(reader, new FileStream(Server.MapPath(outputFileName), FileMode.Create)))
            {
                AcroFields form = stamper.AcroFields;
                var fieldKeys = form.Fields.Keys;
                foreach (string fieldKey in fieldKeys)
                {
                    if (fieldKey.Contains("Name"))
                    {
                        form.SetField(fieldKey, medic.Name);
                    }
                    if (fieldKey.Contains("Last"))
                    {
                        form.SetField(fieldKey, medic.Lastname);
                    }
                    if (fieldKey.Contains("Email"))
                    {
                        form.SetField(fieldKey, medic.Email.ToLower());
                    }
                    if (fieldKey.Contains("ICO"))
                    {
                        form.SetField(fieldKey, invoice.IdentifyNo);
                    }
                    if (fieldKey.Contains("Address"))
                    {
                        form.SetField(fieldKey, invoice.Address);
                    }
                    if (fieldKey.Contains("Person"))
                    {
                        form.SetField(fieldKey, medic.Title + " " + medic.Name + " " + medic.Lastname + " " + medic.TitleBehind);
                    }
                    if (fieldKey.Contains("Item"))
                    {
                        form.SetField(fieldKey, "");
                    }
                    if (fieldKey.Contains("Label"))
                    {
                        form.SetField(fieldKey, "");
                    }
                }
                //The below will make sure the fields are not editable in
                //the output PDF.
                stamper.FormFlattening = true;
            }

            return outputFileName;
        }

    }
}
