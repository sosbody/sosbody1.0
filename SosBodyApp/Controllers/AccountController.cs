﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using SosBody.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SosBody.Infrastructure;
using System.Net.Mail;
using SOSBody;
using SOSBody.Helpers;

//https://docs.microsoft.com/en-us/aspnet/identity/overview/features-api/best-practices-for-deploying-passwords-and-other-sensitive-data-to-aspnet-and-azure
//https://stackoverflow.com/questions/19002864/how-do-i-implement-password-reset-with-asp-net-identity-for-asp-net-mvc-5-0

namespace SosBody.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        IdentityDbEntities db = new IdentityDbEntities();
        //
        // GET: /Account/

        private static Logger logger = new Logger();

        [AllowAnonymous]
        public ActionResult Login(string email)
        {
            string returnUrl = "/Admin";
            ViewBag.returnUrl = returnUrl;
            //logger.Log(DateTime.Now, Logger.Level.Trace, "null", "ReturnUrl set to ViewBag 2", "ReturnUrl: " + returnUrl);

            if (String.IsNullOrEmpty(email))
                return View();
            else
            {
                LoginModel model = new LoginModel()
                {
                    Username = email
                };

                return View(model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        //[RequireHttps]
        public async Task<ActionResult> Login(LoginModel details, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                AppUser user = await UserManager.FindAsync(details.Username, details.Password);
                if (user == null)
                {
                    ModelState.AddModelError("", "Neplatné přihlašovací údaje");
                    logger.Log(DateTime.Now, Logger.Level.Warning, "null", "Neplatné přihlašovací údaje", null);
                }
                else
                {
                    //Session["RunSession"] = 1;
                    ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, ident);
                    
                    if (user.Roles.Count > 0)
                    {
                        AppRole role = RoleManager.FindById(user.Roles.FirstOrDefault().RoleId);
                        if (role.Name == "SOSBodyMedic")
                        {
                            //if(db.UserStatus.Any(x => x.UserId == user.Id) == false)
                            //    return Redirect("/Home/ErrorPage");
                            //var userStatus = db.UserStatus.Where(x => x.UserId == user.Id);
                            //if (userStatus == null)
                            //    return Redirect("/Home/ErrorPage");

                            //if(userStatus.Status == "NEW")
                            //{
                            //    ModelState.AddModelError("", "Plnohodnotný přístup do systému Vám zatím nebyl povolen. Zkuste to později.");
                            //    ViewBag.returnUrl = returnUrl;
                            //    return View(details);
                            //}
                            //else
                            //ViewBag.returnUrl = returnUrl;

                            logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "User logged", "Role - " + role.Name);

                            var medicDetail = db.MedicDetails.Where(x => x.UserId == user.Id);
                            var officeDetail = db.OfficeDetails.Where(x => x.UserId == user.Id);
                            var invoiceDetail = db.MedicInvoiceDetails.Where(x => x.UserId == user.Id);
                            if (medicDetail == null || medicDetail.Count() == 0 || officeDetail == null || officeDetail.Count() == 0 || invoiceDetail == null || invoiceDetail.Count() == 0)
                                return Redirect("/Admin/RegisterMedicStep1");

                            return Redirect("/Admin/HomeMedic");
                        }
                        else if (role.Name == "SOSBodyPatient")
                        {
                            logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "User logged", "Role - " + role.Name);
                            return Redirect("/Admin/HomePatient");
                        }
                        else if (role.Name == "SOSBodyAdmin")
                        {
                            logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "User logged", "Role - " + role.Name);
                            return Redirect("/Dashboard/Index");
                        }
                        else
                        {
                            logger.Log(DateTime.Now, Logger.Level.Error, user.Id, "User login", "Role - no role exists");
                            return Redirect("/Home/ErrorPage");
                        }
                    }

                    logger.Log(DateTime.Now, Logger.Level.Trace, "null", "Login - Redirect(returnUrl)", "ReturnUrl: " + returnUrl);
                    return Redirect(returnUrl);
                }
            }
            ViewBag.returnUrl = returnUrl;
            //logger.Log(DateTime.Now, Logger.Level.Trace, "null", "ReturnUrl set to ViewBag", "ReturnUrl: " + returnUrl);
            return View(details);
        }

        [Authorize]
        public ActionResult Logout()
        {
            string userId = User.Identity.GetUserId();
            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "User logout", null);
            AuthManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult PasswordReset(string email)
        {
            PasswordResetEmailModel model = new PasswordResetEmailModel()
            {
                Email = String.IsNullOrEmpty(email) ? "" : email
            };

            return View(model);
        }


        //https://forums.asp.net/t/2013914.aspx?GenerateEmailConfirmationTokenAsync+throws+No+IUserTokenProvider+is+registered+
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> PasswordReset(PasswordResetEmailModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)// || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    logger.Log(DateTime.Now, Logger.Level.Warning, "null", "Password Reset - Neplatný email", null);
                    ModelState.AddModelError("", "Neplatný e-mail");
                    return View();
                }    

                try
                {
                    var code = UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    string codeStr = System.Web.HttpUtility.UrlEncode(code.Result);

                    var callbackUrl = Url.Action("ResetOldPassword", "Account", new { UserId = user.Id, code = code.Result }, protocol: Request.Url.Scheme);
                 
                    MailMessage message = new MailMessage("info@sosbody.cz", user.Email, "Obnova hesla", "Heslo obnovíte kliknutím <a href=\'" + callbackUrl + "\'>zde.</a>");
                    message.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient("smtp.zoner.com");
                    client.Send(message);

                    logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "Password Reset - Obnova hesla", codeStr);
                }
                catch (Exception ex)
                {
                    logger.Log(DateTime.Now, Logger.Level.Exception, user.Id, ex.Message, ex.InnerException.ToString());
                }

                return View("PasswordResetEmailSent");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult PasswordResetEmailSent()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmPassword(string userId, string code)
        {
            if(ModelState.IsValid)
            {
                if (code == null || userId == null)
                {
                    logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Activation e-mail confirmation failed", code);
                    return View("ConfirmPasswordError");
                }
                else
                {
                    var user = UserManager.FindById(userId);
                    if (user != null)
                    {
                        string codeStr = System.Web.HttpUtility.UrlDecode(code);
                        var result = UserManager.ConfirmEmail(user.Id, codeStr);

                        logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Received e-mail confirmation token", codeStr);
                        logger.Log(DateTime.Now, Logger.Level.Trace, userId, "UserManager.ConfirmEmail", "Result: " + result.Succeeded.ToString());

                        if (result.Succeeded)
                        {
                            logger.Log(DateTime.Now, Logger.Level.Trace, userId, "Activation e-mail confirmed successfully", null);

                            ConfirmPasswordModel model = new ConfirmPasswordModel()
                            {
                                Email = user.Email
                            };

                            return View(model);
                        }
                    }
                }
            }

            return View("ConfirmPasswordError");
        }

        [AllowAnonymous]
        public ActionResult ConfirmPasswordError()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetOldPassword(string userId, string code)
        {
            if (ModelState.IsValid)
            {
                if (userId == null || code == null)
                {
                    return View("Error");
                }

                string codeStr = System.Web.HttpUtility.UrlDecode(code);
                PasswordResetModel model = new PasswordResetModel()
                {
                    UserId = userId,
                    Code = code
                };

                return View(model);
            }

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ResetOldPassword(PasswordResetModel model)
        {
            if(ModelState.IsValid)
            {
                if(model.Password != model.ConfirmPassword)
                {
                    ModelState.AddModelError("", "Hesla nejsou shodná");
                    return View(model);
                }

                var result = await UserManager.ResetPasswordAsync(model.UserId, model.Code, model.Password);
                if (result.Succeeded)
                {
                    return View("ResetOldPasswordConfirmed");
                }
            }

            return View("ResetOldPasswordError");
        }


        //[AllowAnonymous]
        //public ActionResult ResetOldPasswordConfirmed()
        //{
        //    return View();
        //}

        //[AllowAnonymous]
        //public ActionResult ResetOldPasswordError()
        //{
        //    return View();
        //}

        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private AppRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }

        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


//https://help.shoptet.cz/topic/prihlasovani-pres-socialni-site/
//https://forums.asp.net/t/1948844.aspx?OWIN+GetExternalLoginInfoAsync+Allways+returns+null

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        private async Task SignInAsync(AppUser user, bool isPersistent)
        {
            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthManager.SignOut();
            AuthManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, ident);
        }

        private async Task<ExternalLoginInfo> AuthenticationManager_GetExternalLoginInfoAsync_Workaround()
        {
            ExternalLoginInfo loginInfo = null;

            var result = await AuthManager.AuthenticateAsync(DefaultAuthenticationTypes.ExternalCookie);

            if (result != null && result.Identity != null)
            {
                var idClaim = result.Identity.FindFirst(ClaimTypes.NameIdentifier);
                if (idClaim != null)
                {
                    loginInfo = new ExternalLoginInfo()
                    {
                        DefaultUserName = result.Identity.Name == null ? "" : result.Identity.Name.Replace(" ", ""),
                        Login = new UserLoginInfo(idClaim.Issuer, idClaim.Value)
                    };
                }
            }
            return loginInfo;
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            //var user = await UserManager.FindAsync(loginInfo.Login);
            var user = UserManager.FindByEmail(loginInfo.Email);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                if (user.Roles.Count > 0)
                {
                    AppRole role = RoleManager.FindById(user.Roles.FirstOrDefault().RoleId);
                    if (role.Name == "SOSBodyMedic")
                    {
                        logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "User logged - external", "Role - " + role.Name);
                        return Redirect("/Admin/HomeMedic");
                    }
                    else if (role.Name == "SOSBodyPatient")
                    {
                        logger.Log(DateTime.Now, Logger.Level.Trace, user.Id, "User logged - external", "Role - " + role.Name);
                        return Redirect("/Admin/HomePatient");
                    }
                    else
                    {
                        logger.Log(DateTime.Now, Logger.Level.Error, user.Id, "User login - external", "Role - no role exists");
                        return Redirect("/Home/ErrorPage");
                    }
                }

                return RedirectToAction("Login");
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

                //CreateModel createModel = new CreateModel()
                //{
                //    Email = loginInfo.Email
                //};

                TempData["RegisterEmail"] = loginInfo.Email;
                return RedirectToAction("RegisterMedic", "Admin");
                //return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        private const string XsrfKey = "XsrfId";
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

    }
}
