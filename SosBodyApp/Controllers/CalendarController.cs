﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.UI;
using System.Web.UI.WebControls;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Google.Apis.Plus.v1;
using Google.Apis.Plus.v1.Data;


//using Google.GData.Calendar;
//using Google.GData.Extensions;
//using Google.GData.Client;

using SOSBody.Helpers.Google;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2.Responses;
using System.Threading;

namespace SOSBody.Controllers
{
    public class GoogleUserOutputData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string email { get; set; }
        public string picture { get; set; }
    }

    public class GooglePlusAccessToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
        public string refresh_token { get; set; }
    }

    public class CalendarController : Controller
    {
        protected string googleplus_client_id = "248882027328-477sa4fs2l45720f8slbuk0q5vtr0u53.apps.googleusercontent.com";    // Replace this with your Client ID
        protected string googleplus_client_secret = "eLQfi0scc1OLeMPJzMmsBefE";                                                // Replace this with your Client Secret
        protected string googleplus_redirect_url = "http://localhost:51672/Calendar/";                                         // Replace this with your Redirect URL; Your Redirect URL from your developer.google application should match this URL.
        protected string Parameters;


        //
        // GET: /Calendar/


        public async Task<ActionResult> Index()
        {
            if (Session.Contents.Count > 0)
            {
                if (Session["loginWith"] != null)
                {
                    if (Session["loginWith"].ToString() == "google")
                    {
                        try
                        {
                            var url = Request.Url.Query;
                            if (url != "")
                            {
                                string queryString = url.ToString();
                                char[] delimiterChars = { '=' };
                                string[] words = queryString.Split(delimiterChars);
                                string code = words[1];

                                if (code != null)
                                {
                                    //get the access token 
                                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                                    webRequest.Method = "POST";
                                    Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + googleplus_redirect_url + "&grant_type=authorization_code";
                                    byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(Parameters);
                                    webRequest.ContentType = "application/x-www-form-urlencoded";
                                    webRequest.ContentLength = byteArray.Length;
                                    Stream postStream = webRequest.GetRequestStream();
                                    // Add the post data to the web request
                                    postStream.Write(byteArray, 0, byteArray.Length);
                                    postStream.Close();

                                    WebResponse response = webRequest.GetResponse();
                                    postStream = response.GetResponseStream();
                                    StreamReader reader = new StreamReader(postStream);
                                    string responseFromServer = reader.ReadToEnd();

                                    GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                                    if (serStatus != null)
                                    {
                                        string accessToken = string.Empty;
                                        accessToken = serStatus.access_token;

                                        if (!string.IsNullOrEmpty(accessToken))
                                        {
                                            // This is where you want to add the code if login is successful.
                                            //getgoogleplususerdataSer(accessToken);

                                            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                                               new ClientSecrets
                                               {
                                                   ClientId = googleplus_client_id,
                                                   ClientSecret = googleplus_client_secret,
                                               },
                                               new[] { CalendarService.Scope.Calendar },
                                               "user",
                                               CancellationToken.None).Result;


                                            // Create the service.
                                            CalendarService service = new CalendarService(new BaseClientService.Initializer()
                                            {
                                                HttpClientInitializer = credential,
                                                ApplicationName = "Calendar API Sample",
                                            });

                                            // Fetch the list of calendar list
                                            //IList<CalendarListEntry> list = service.CalendarList.List().Execute().Items;
                                            //CalendarListEntry entry = list[0];

                                            //EventsResource.ListRequest events = service.Events.List(list[0].Id);

                                            // Define parameters of request.
                                            EventsResource.ListRequest request = service.Events.List("primary");
                                            request.TimeMin = DateTime.Now;
                                            request.ShowDeleted = false;
                                            request.SingleEvents = true;
                                            request.MaxResults = 10;
                                            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                                            // List events.
                                            Events events = request.Execute();
                                            List<Event> sosBodyEventList = new List<Event>();

                                            if (events.Items != null && events.Items.Count > 0)
                                            {
                                                foreach (var eventItem in events.Items)
                                                {
                                                    if (eventItem.Summary != null && eventItem.Summary.ToUpper() == "SOSBODY")
                                                        sosBodyEventList.Add(eventItem);

                                                    //string when = eventItem.Start.DateTime.ToString();
                                                    //if (String.IsNullOrEmpty(when))
                                                    //{
                                                    //    when = eventItem.Start.Date;
                                                    //}
                                                    //Console.WriteLine("{0} ({1})", eventItem.Summary, when);
                                                }
                                            }

                                            return Redirect("/Admin/Calendar");
                                        }
                                        else
                                        { }
                                    }
                                    else
                                    { }
                                }
                                else
                                { }
                            }
                        }
                        catch (Exception ex)
                        {
                            string s = ex.Message;
                            //throw new Exception(ex.Message, ex);
                            //Response.Redirect("index.aspx");
                        }
                    }
                }

            }
            //else
            {
                var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
                Session["loginWith"] = "google";
                Response.Redirect(Googleurl);
            }

            



            //IList  list = service.SynchronizeEvents();
            return View();
        }


        private async void getgoogleplususerdataSer(string access_token)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token;

                client.CancelPendingRequests();
                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        
                    }
                }
            }
            catch (Exception ex)
            {
                //catching the exception
                string exception = ex.Message;
            }
        }

    }

    


    //http://stackoverflow.com/questions/24057939/login-using-google-oauth-2-0-with-c-sharp
}
