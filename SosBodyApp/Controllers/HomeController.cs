﻿using SosBody.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using SOSBody;
using System.Globalization;
using System.IO;
using SosBody.Infrastructure;
using System.Text;
using Microsoft.AspNet.Identity;
using SOSBody.Helpers;
using System.Net.Mail;
//using System.Net.Http.HttpClient;

namespace SosBody.Controllers
{
    public class HomeController : Controller
    {
        IdentityDbEntities db = new IdentityDbEntities();
        private static Logger logger = new Logger();
        private static PhoneHelper ph = new PhoneHelper();
        private static Validators validator = new Validators();

        static HttpClient client = new HttpClient();

        //
        // GET: /Home/

        public async Task<ActionResult> Payment()
        {
            return View();
        }

        [HttpPost]
        public string Payment(FormCollection collection)
        {
            string merchant = Request["merchant"];
            string price = Request["price"];
            string curr = Request["curr"];
            string label = Request["label"];
            string refId = Request["refId"];
            string cat = Request["cat"];
            string method = Request["method"];
            string email = Request["email"];
            string phone = Request["phone"];
            string transId = Request["transId"];
            string secret = Request["secret"];
            string status = Request["status"];
            logger.Log(DateTime.Now, Logger.Level.Trace, "COMGATE", "Comgate payment merchant", "Merchant: " + merchant + ", refId: " + refId + "email: " + email + "phone: " + phone + "status: " + status);

            var payment = db.Payments.Where(x => x.TransId == transId).FirstOrDefault();
            payment.Status = status;
            if (status == "PAID")
            {
                payment.PaidDate = DateTime.Now;
                BookActionWithEmail(payment.OrderId);
                SendConfirmEmailToPatient(payment.OrderId);
            }
            else if (status == "CANCELLED")
            {
                payment.CancelledDate = DateTime.Now;
                //SendCancelEmailToMedic(payment.OrderId);
            }

            db.SaveChanges();

            return "code=0&message=OK";
        }

        //https://platebnibrana.comgate.cz/cz/protokol-api
        // IP: 217.198.114.80

        /// <summary>
        /// Email 005
        /// </summary>
        /// <param name="medicEmail"></param>
        /// <param name="date"></param>
        /// <param name="name"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        private void SendConfirmMailToMedic(string medicEmail, DateTime date, string name, string phone, string email)
        {
            string body = @"<!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal 80% arial,sans-serif;'>
                        <strong>Nová rezervace termínu SOS Body</strong><br /><br />";

            body += "Jméno: " + name + "<br /> Datum: " + date.ToString("dd.MM.yyyy HH:mm") + " <br /> Telefon: " + phone + " <br /> E - mail: " + email;
            body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>";

            MailMessage message = new MailMessage("info@sosbody.cz", medicEmail, "Rezervace termínu SOS Body", body);
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.zoner.com");
            client.Send(message);   
        }

        /// <summary>
        /// Email 006
        /// </summary>
        /// <param name="orderId"></param>
        private void SendConfirmEmailToPatient(int orderId)
        {
            var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
            if (order == null)
                return;

            var payment = db.Payments.Where(x => x.OrderId == orderId).FirstOrDefault();
            if (payment == null)
                return;

            var action = db.Calendar.Where(x => x.Id == order.ActionId).FirstOrDefault();
            if (action == null)
                return;

            var office = db.OfficeDetails.Where(x => x.Id == action.OfficeId).FirstOrDefault();
            if (office == null)
                return;

            var medic = db.MedicDetails.Where(x => x.UserId == order.MedicId).FirstOrDefault();
            if (medic != null)
            {
                var callbackUrl = Url.Action("Reservation", "Home", new { transId = payment.TransId }, protocol: Request.Url.Scheme);

                string body = @"<!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Děkujeme za Vaší objednávku,</strong><br /><br />

                        <strong>potvrzujeme přijetí Vaší platby a zasíláme souhrnné informace k Vaší návštěvě.</strong><br />";


                body += "<br /> <strong>Datum:</strong> " + action.Date.ToString("dd.MM.yyyy HH:mm");
                body += "<br /> <strong>Jméno fyzioterapeuta:</strong> " + medic.Title + " " + medic.Name + " " + medic.Lastname + " " + medic.TitleBehind;
                body += "<br /> <strong>Telefon do ordinace:</strong> " + medic.Phone;
                body += "<br /> <strong>E-mail:</strong> " + medic.Email;
                body += "<br /> <strong>Adresa:</strong> " + office.Street + " " + office.StreetNo + ", " + office.City;
                body += "<br /> <strong>Kód rezervace: </strong> " + payment.TransId;
                body += @"
                        <br /><br />
                        <strong>Pro bližší detaily klikněte <a href=\'" + callbackUrl + "\'>zde.</a></strong>";
                body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>";

                MailMessage message = new MailMessage("info@sosbody.cz", payment.Email, "Potvrzení objednávky termínu SOS Body", body);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.zoner.com");
                client.Send(message);
            }
        }

        /// <summary>
        /// Email 007
        /// </summary>
        /// <param name="orderId"></param>
        private void SendCancelEmailToMedic(int? orderId)
        {
            var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
            if (order == null)
                return;

            var payment = db.Payments.Where(x => x.OrderId == orderId).FirstOrDefault();
            if (payment == null)
                return;

            var action = db.Calendar.Where(x => x.Id == order.ActionId).FirstOrDefault();
            if (action == null)
                return;

            var office = db.OfficeDetails.Where(x => x.Id == action.OfficeId).FirstOrDefault();
            if (office == null)
                return;

            var medic = db.MedicDetails.Where(x => x.UserId == order.MedicId).FirstOrDefault();
            if (medic != null)
            {

                string body = @"<!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Zrušení objednaného termínu SOS Body</strong><br /><br />";

                body += "Jméno: " + order.Name + " " + order.LastName + "<br /> Datum: " + order.Date.ToString("dd.MM.yyyy HH:mm") + " <br /> Telefon: " + payment.Phone + " <br /> E - mail: " + payment.Email;
                body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>";

                MailMessage message = new MailMessage("info@sosbody.cz", medic.Email, "Zrušení objednávky termínu SOS Body", body);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.zoner.com");
                client.Send(message);

                logger.Log(DateTime.Now, Logger.Level.Trace, "anonymous", "SendCancelEmailToMedic()", "Jméno: " + order.Name + " " + order.LastName + "<br /> Datum: " + order.Date.ToString("dd.MM.yyyy HH:mm") + " <br /> Telefon: " + payment.Phone + " <br /> E - mail: " + payment.Email + "OrderId: " + orderId);
            }
        }


        private string GetRefIdFromDate()
        {
            string ret = "";

            DateTime now = DateTime.Now;
            ret += now.Year.ToString();
            if (now.Month < 10)
                ret += "0";
            ret += now.Month.ToString();
            if (now.Day < 10)
                ret += "0";
            ret += now.Day.ToString();
            if (now.Hour < 10)
                ret += "0";
            ret += now.Hour.ToString();
            if (now.Minute < 10)
                ret += "0";
            ret += now.Minute.ToString();
            if (now.Second <= 10)
                ret += "0";
            ret += now.Second.ToString();

            return ret;
        }

        private void BookActionWithEmail(int orderId)
        {
            var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
            if (order == null)
                return;

            var action = db.Calendar.Where(x => x.Id == order.ActionId).FirstOrDefault();
            if (action == null)
                return;

            action.Booked = true;
            action.Info = order.Name.Substring(0,1) + ". " + order.LastName + " - " + order.Phone;
            db.SaveChanges();

            var medic = db.MedicDetails.Where(x => x.UserId == order.MedicId).FirstOrDefault();
            if (medic != null)
            {
                SendConfirmMailToMedic(medic.Email, action.Date, order.Name + " " + order.LastName, order.Phone, order.Email);
                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Booking in calendar", "MedicId: " + order.MedicId + ", ActionId: " + order.ActionId);
            }
        }

        public ActionResult PayNow(Order order)
        {
            string merchant = "134281";
            string curr = "CZK";
            string label = "fyziopohotovost";
            string method = "ALL";
            string cat = "DIGITAL";
            string secret = "u25sPo8muolnv6PN01V9n0F9wd3ugBXr";
            string refId = GetRefIdFromDate();

            UTF8Encoding encoding = new UTF8Encoding();
            string postData = "merchant=" + merchant;// System.Configuration.ConfigurationManager.AppSettings.Get("userid");
            postData += ("&test=" + "false");// System.Configuration.ConfigurationManager.AppSettings.Get("username"));
            postData += ("&price=" + Convert.ToInt32(order.Price) + "00");// System.Configuration.ConfigurationManager.AppSettings.Get("pinno"));
            postData += ("&curr=" + curr);
            postData += ("&label=" + label);
            postData += ("&refId=" + refId);
            postData += ("&method=" + method);
            postData += ("&cat=" + cat);
            //postData += ("&account=" + "767528900/5500");
            postData += ("&prepareOnly=" + "true");
            postData += ("&email=" + order.Email);
            postData += ("&phone=" + order.Phone);
            postData += ("&secret=" + secret);
            byte[] data = encoding.GetBytes(postData);

            // Prepare web request...
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://payments.comgate.cz/v1.0/create");

            //myRequest.TransferEncoding = "charset=utf-8";
            myRequest.Method = "POST";
            myRequest.Host = "payments.comgate.cz";
            myRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            myRequest.ContentLength = data.Length;
            Stream newStream = myRequest.GetRequestStream();

            // Send the data.
            newStream.Write(data, 0, data.Length);

            HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse();
            if (response == null)
            {
                return Redirect("PaymentError");
            }
            string resDesc = ((HttpWebResponse)response).StatusDescription;
            System.IO.StreamReader stream = new StreamReader(response.GetResponseStream());
            string responseFromServer = stream.ReadToEnd();
            stream.Close();
            response.Close();
            newStream.Close();

            responseFromServer = responseFromServer.Replace("%3A", ":");
            responseFromServer = responseFromServer.Replace("%2F", "/");
            responseFromServer = responseFromServer.Replace("%3F", "?");
            responseFromServer = responseFromServer.Replace("%3D", "=");

            int pos = responseFromServer.IndexOf("redirect");
            string redirect = responseFromServer.Substring(pos + 9);
            int transPos = responseFromServer.IndexOf("transId");
            string transId = responseFromServer.Substring(transPos + 8, 14);

            Payments payment = new Payments()
            {
                Merchant = merchant,
                Price = order.Price.ToString(),
                Curr = curr,
                Label = label,
                Method = method,
                Cat = cat,
                Secret = secret,
                CreatedDate = DateTime.Now,
                RefId = refId,
                TransId = transId,
                Email = order.Email,
                Phone = order.Phone,
                OrderId = order.Id,
                Status = "CREATED"
            };

            db.Payments.Add(payment);
            db.SaveChanges();

            Response.Redirect(redirect);

            return Redirect("Payment");
        }

        public ActionResult PaymentOK()
        {
            return View();
        }

        //https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client

        [HttpPost]
        public async Task<ActionResult> PaymentOK(string id, string refId)
        {
            return View();
        }

        public ActionResult PaymentCancelled()
        {
            return View();
        }

        //https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client

        [HttpPost]
        public async Task<ActionResult> PaymentCancelled(string id, string refId)
        {
            return View();
        }

        public ActionResult PaymentPending()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PaymentPending(string id, string refId)
        {
            return View();
        }

        //https://www.hanselman.com/blog/HowToRunBackgroundTasksInASPNET.aspx

        [HttpPost]
        public async Task Platba2()
        {
            string url = "";
            try
            {
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                string result = await response.Content.ReadAsStringAsync();
                TempData["msg"] = "<script>alert('" + result + "');</script>";
            }
            catch (Exception ex)
            {
                TempData["msg"] = "<script>alert('error');</script>";
            }
        }

        public ActionResult ErrorPage()
        {
            return View();
        }

        public ActionResult PaymentError()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Reservation(string transId)
        {
            if(String.IsNullOrEmpty(transId) == true)
                return View();

            ReservationModel model = new ReservationModel()
            {
                Reservation = transId
            };

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Reservation(ReservationModel model)
        {
            if(ModelState.IsValid)
            {
                string code = model.Reservation.ToUpper();

                var payment = db.Payments.Where(x => x.TransId == code).FirstOrDefault();
                if(payment == null)
                {
                    ModelState.AddModelError("Chyba", "Zadanému kódu neodpovídá žádná rezervace");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed", "Input code - " + code);
                    return View();
                }

                var order = db.Orders.Where(x => x.Id == payment.OrderId).FirstOrDefault();
                if(order == null)
                {
                    ModelState.AddModelError("Chyba", "Zadanému kódu neodpovídá žádná rezervace");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed", "Input code - " + code);
                    return View();
                }

                var office = db.OfficeDetails.Where(x => x.Id == order.OfficeId).FirstOrDefault();
                if(office == null)
                {
                    ModelState.AddModelError("Chyba", "K zadané rezervaci není možné zobrazit detaily");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed - can't find the office", "Office Id - " + order.OfficeId);
                    return View();
                }

                var medic = db.MedicDetails.Where(x => x.UserId == office.UserId).FirstOrDefault();
                if(medic == null)
                {
                    ModelState.AddModelError("Chyba", "K zadané rezervaci není možné zobrazit detaily");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed - can't find the medic", "Medic Id - " + office.UserId);
                    return View();
                }

                var action = db.Calendar.Where(x => x.Id == order.ActionId).FirstOrDefault();
                if (action == null)
                {
                    ModelState.AddModelError("Chyba", "K zadané rezervaci není možné zobrazit detaily");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed - can't find the action", "Action Id - " + order.ActionId);
                    return View();
                }

                DateTime nowDate = DateTime.Now;
                nowDate = nowDate.AddDays(-7);// decrease date for seven days for complaint
                if (action.Date < nowDate)
                {
                    ModelState.AddModelError("Chyba", "Tento termín již minul a detaily již nejdou zobrazit");
                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "My reservation failed - validation of termin expired", "Code - " + code);
                    return View();
                }

                string phone = ph.GetPhonePure(office.Phone);
                if (String.IsNullOrEmpty(phone) == true)
                    phone = medic.Phone;

                ReservationDetailModel resultModel = new ReservationDetailModel()
                {
                    OrderId = order.Id,
                    Code = code,
                    MedicName = medic.Name + " " + medic.Lastname,
                    MedicAddress = office.Street + " " + office.StreetNo + ", " + office.City,
                    Phone = phone,
                    Email = medic.Email,
                    CancelEnabled = false,
                    Date = action.Date.ToString("dd. MM. yyyy HH:mm")
                 };

                DateTime dateNow = DateTime.Now;
                TimeSpan span = action.Date.Subtract(dateNow);
                if (span.TotalHours > 24 && payment.Status != "CANCELLED")
                    resultModel.CancelEnabled = true;

                return View("ReservationDetail", resultModel);
            }
            return View();
        }

        /// <summary>
        /// Email 007
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult DeleteOrder(int? orderId)
        {
            if(orderId != null)
            {
                var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
                if(order == null)
                    Redirect("/Home/Reservation");

                order.Status = "CANCELED";
                db.SaveChanges();

                SendCancelEmailToMedic(orderId);

                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Reservation canceled by user", "OrderId: " + orderId.ToString());

                // thie email goes to sosbody
                string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                  </head>
                  <body>
                      <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Rezervace zrušena klientem. Id rezervace: </strong>";

                body += order.Id.ToString();
                body += @"
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>

                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>
            ";
                MailMessage message = new MailMessage("info@sosbody.cz", "info@sosbody.cz", "Rezervace zrušena", body);
                message.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.zoner.com");
                client.Send(message);

                return Redirect("/Home/ReservationCancelled");
            }

            return Redirect("/Home/Reservation");
        }

        [AllowAnonymous]
        public ActionResult ReservationCancelled()
        {
            return View();
        }

        public ActionResult Order(int id)
        {
            int actionId = id;

            var actionDetailList = db.Calendar.Where(x => x.Id == actionId);
            if (actionDetailList == null || actionDetailList.Count() == 0)
                return Redirect("/Home/ErrorPage");

            var actionDetail = actionDetailList.ToList()[0];

            var medicDetailList = db.MedicDetails.Where(x => x.UserId == actionDetail.UserId);
            if (medicDetailList == null || medicDetailList.Count() == 0)
                return Redirect("/Home/ErrorPage");
            
            var officeDetailList = db.OfficeDetails.Where(x => x.Id == actionDetail.OfficeId);
            if (officeDetailList == null || officeDetailList.Count() == 0)
                return Redirect("/Home/ErrorPage");     

            var medicDetail = medicDetailList.ToList()[0];
            var officeDetail = officeDetailList.ToList()[0];           

            string date = actionDetail.Date.Day.ToString() + ". " + actionDetail.Date.Month.ToString() + ". " + actionDetail.Date.Year.ToString() + ", "
                    + actionDetail.Date.Hour.ToString() + ":" + actionDetail.Date.Minute.ToString();
            if (actionDetail.Date.Minute == 0)
                date += "0";

            OrderUserModel model = new OrderUserModel()
            {
                MedicUserId = actionDetail.UserId,
                OfficeId = actionDetail.OfficeId,
                ActionId = actionId,
                MedicName = medicDetail.Title + " " + medicDetail.Name + " " + medicDetail.Lastname + " " + medicDetail.TitleBehind,
                Price = actionDetail.Price.ToString(),// + " Kč s DPH",
                Date = date,
                Address = officeDetail.Street + " " + officeDetail.StreetNo + ", " + officeDetail.City,
                Name = "",
                LastName = "",
                UserEmail = "",
                UserPhone = "",
                Duration = actionDetail.Duration.ToString(),
                PhonePrefix = ph.GetPhonePrefix(""),
                PhonePrefixList = ph.InitPhonePrefixList(""),
                PhonePrefixLength = ph.GetPhonePrefixLength(""),
                Accept = false
            };

            if (model.Address.Length == 3) //empty address
                model.Address = "";

            return View(model);
        }

        [HttpPost]
        public ActionResult Order(OrderUserModel model)
        {
            var item = db.PhonePrefix.Where(x => x.Text == model.PhonePrefix).FirstOrDefault();
            int phoneLength = 9;
            if (item != null)
                phoneLength = item.Length;

            if (ModelState.IsValid && ph.GetPhonePure(model.UserPhone).Length == phoneLength)
            {
                Order order = new Order()
                {
                    MedicId = model.MedicUserId,
                    OfficeId = model.OfficeId,
                    ActionId =  model.ActionId,
                    Price = Convert.ToDecimal(model.Price),
                    Date = DateTime.Now,
                    Name = model.Name,
                    LastName = model.LastName,
                    Email = model.UserEmail,
                    Phone = model.PhonePrefix + "-" + model.UserPhone,
                    Status = "NEW"
                };

                string userId = User.Identity.GetUserId();
                if (userId != null)
                    order.UserId = userId;

                db.Orders.Add(order);
                db.SaveChanges();
                
                PayNow(order);
            }

            if (ph.GetPhonePure(model.UserPhone).Length != phoneLength && ph.GetPhonePure(model.UserPhone).Length != 0)
                model.ErrorMessage = "Délka telefonního čísla není platná";

            model.PhonePrefix = ph.GetPhonePrefix(model.UserPhone);
            model.PhonePrefixList = ph.InitPhonePrefixList(model.UserPhone);
            model.PhonePrefixLength = ph.GetPhonePrefixLength(model.UserPhone);

            return View(model);
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(ContactModel model)
        {
            if (ModelState.IsValid)
            {
                Message message = new Message()
                {
                    Date = DateTime.Now,
                    Email = model.Email,
                    Text = model.Text,
                    Description = "Kontakt"
                };

                db.Message.Add(message);
                db.SaveChanges();

                ModelState.Clear();

                return View();
            }

            return View(model);
        }

        public async Task<ActionResult> Index()
        {
            FindPhysioModel model = new FindPhysioModel();

            // fill map with available medics
            model.Results = new List<FindResultModel>();
            List<FindResultModel> cachedMedicList = await GetCachedMedicsPosListAsync();
            if (cachedMedicList != null && cachedMedicList.Count > 0)
                model.Results = cachedMedicList;
            else
            {

                List<MedicDisplayInfo> medicList = await GetAllMedicsPosListAsync();
                foreach (MedicDisplayInfo p in medicList)
                {
                    //if (p.ActionList.Count > 0)
                    {
                        FindResultModel result = new FindResultModel()
                        {
                            UserId = p.UserId,
                            Latitude = p.Position.Latitude,
                            Longitude = p.Position.Longitude,
                            Description = p.Position.error,
                            Name = p.Name,
                            LastName = p.LastName,
                            Title = p.Title,
                            TitleBehind = p.TitleBehind,
                            Address = p.Address,
                            Street = p.Street,
                            StreetNo = p.StreetNo,
                            City = p.City,
                            OpenningHours = p.OpenningHours,
                            ProfileImage = p.ProfileImage,
                            Distance = p.Distance,
                            ActionList = new List<Models.ActionInfoModel>()
                        };

                        foreach (ActionInfo ai in p.ActionList)
                        {
                            Models.ActionInfoModel actionInfo = new Models.ActionInfoModel()
                            {
                                Id = ai.Id,
                                Date = ai.Date,
                                Price = ai.Price,
                                OfficeId = ai.OfficeId,
                                Duration = ai.Duration
                            };

                            result.ActionList.Add(actionInfo);
                        }

                        model.Results.Add(result);
                    }
                }
            }

            model.MapDistanceItemTypes = new List<SelectListItem>();
            SelectListItem distItem = new SelectListItem();
            distItem.Value = "10";
            distItem.Text = "10 km";
            model.MapDistanceItemTypes.Add(distItem);
            distItem = new SelectListItem();
            distItem.Value = "25";
            distItem.Text = "25 km";
            model.MapDistanceItemTypes.Add(distItem);
            distItem = new SelectListItem();
            distItem.Value = "50";
            distItem.Text = "50 km";
            model.MapDistanceItemTypes.Add(distItem);
            model.Distance = -1;

            model.MapAvailabilityItemTypes = new List<SelectListItem>();
            SelectListItem availItem = new SelectListItem();
            availItem.Value = "Today";
            availItem.Text = "Dnes";
            model.MapAvailabilityItemTypes.Add(availItem);
            availItem = new SelectListItem();
            availItem.Value = "Tomorrow";
            availItem.Text = "Zítra";
            model.MapAvailabilityItemTypes.Add(availItem);
            availItem = new SelectListItem();
            availItem.Value = "Other";
            availItem.Text = "Jindy";
            model.MapAvailabilityItemTypes.Add(availItem);
            model.OpenningHours = "Today";

            SearchDetailModel search = new SearchDetailModel()
            {
                Name = "",
                LastName = ""
            };
            model.PartialModel = search;
            model.Carousel = await InitCarousel();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(FindPhysioModel model)
        {
            if (String.IsNullOrEmpty(model.Address) == false)
            {
                GeoPoint userLocation = await GetPoint(model.Address);
                if(userLocation == null)
                    return View();

                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Index - medic search", model.Address);
                if (model.Distance == 0)
                    model.Distance = -1;
                int distance = model.Distance == -1 ? 50 : model.Distance;
                model.Distance = distance;

                List<MedicDisplayInfo> medicList = await GetMedicPosList(distance, model.OpenningHours, userLocation);

                model.Results = new List<FindResultModel>();

                FindResultModel userPosition = new FindResultModel()
                {
                    Latitude = userLocation.Latitude,
                    Longitude = userLocation.Longitude,
                    Description = "Vaše poloha"
                };

                model.Results.Add(userPosition);

                foreach (MedicDisplayInfo p in medicList)
                {
                    //if (p.ActionList.Count > 0)
                    {
                        FindResultModel result = new FindResultModel()
                        {
                            UserId = p.UserId,
                            OfficeId = p.OfficeId,
                            Latitude = p.Position.Latitude,
                            Longitude = p.Position.Longitude,
                            Description = p.Position.error,
                            Name = p.Name,
                            LastName = p.LastName,
                            Title = p.Title,
                            TitleBehind = p.TitleBehind,
                            Address = p.Address,
                            Street = p.Street,
                            StreetNo = p.StreetNo,
                            City = p.City,
                            OpenningHours = p.OpenningHours,
                            ProfileImage = p.ProfileImage,
                            Distance = p.Distance,
                            ActionList = new List<Models.ActionInfoModel>()
                        };

                        foreach (ActionInfo ai in p.ActionList)
                        {
                            Models.ActionInfoModel actionInfo = new Models.ActionInfoModel()
                            {
                                Id = ai.Id,
                                Date = ai.Date,
                                Price = ai.Price,
                                OfficeId = ai.OfficeId,
                                Duration = ai.Duration
                            };

                            result.ActionList.Add(actionInfo);
                        }

                        model.Results.Add(result);
                    }
                }
            }

            model.MapDistanceItemTypes = new List<SelectListItem>();
            SelectListItem distItem = new SelectListItem();
            distItem.Value = "10";
            distItem.Text = "10 km";
            model.MapDistanceItemTypes.Add(distItem);
            distItem = new SelectListItem();
            distItem.Value = "25";
            distItem.Text = "25 km";
            model.MapDistanceItemTypes.Add(distItem);
            distItem = new SelectListItem();
            distItem.Value = "50";
            distItem.Text = "50 km";
            model.MapDistanceItemTypes.Add(distItem);

            model.MapAvailabilityItemTypes = new List<SelectListItem>();
            SelectListItem availItem = new SelectListItem();
            availItem.Value = "Today";
            availItem.Text = "Dnes";
            model.MapAvailabilityItemTypes.Add(availItem);
            availItem = new SelectListItem();
            availItem.Value = "Tomorrow";
            availItem.Text = "Zítra";
            model.MapAvailabilityItemTypes.Add(availItem);
            availItem = new SelectListItem();
            availItem.Value = "Other";
            availItem.Text = "Jindy";
            model.MapAvailabilityItemTypes.Add(availItem);

            SearchDetailModel search = new SearchDetailModel()
            {
                Name = "",
                LastName = ""
            };
            model.PartialModel = search;
            model.Carousel = await InitCarousel();

            ViewBag.Section = "therapists-list";
            return View(model);
        }

        public ActionResult MedicProfile(string userId)
        {
            var medicDetail= db.MedicDetails.Where(x => x.UserId == userId).FirstOrDefault();
            if (medicDetail == null)
                return Redirect("/Home/ErrorPage");

            var officeDetailList = db.OfficeDetails.Where(x => x.UserId == userId);
            if (officeDetailList == null || officeDetailList.Count() == 0)
                return Redirect("/Home/ErrorPage");

            MedicProfileModel model = new MedicProfileModel()
            {
                UserId = userId,
                Name = medicDetail.Title + " " + medicDetail.Name + " " + medicDetail.Lastname + " " + medicDetail.TitleBehind,
                Email = medicDetail.Email,
                Phone = medicDetail.Phone,
                Actions = new List<MedicProfileActionModel>(),
                Spec1 = medicDetail.Spec1,
                Spec2 = medicDetail.Spec2,
                Spec3 = medicDetail.Spec3,
                Spec4 = medicDetail.Spec4,
                Spec5 = medicDetail.Spec5,
                Spec6 = medicDetail.Spec6,
                ProfileImage = GetProfileImage(Globals.ProfileImagesPath, userId)
            };

            model.Actions = GetAllActions(userId);

            model.Offices = new List<OfficeDetailModel>();

            foreach(var officeDetail in officeDetailList)
            {
                OfficeDetailModel office = new OfficeDetailModel()
                {
                    Id = officeDetail.Id,
                    Street = officeDetail.Street,
                    StreetNo = officeDetail.StreetNo,
                    Name = officeDetail.Name,
                    City = officeDetail.City,
                    Country = officeDetail.Country,
                    PostalCode = officeDetail.PostalCode,
                    Phone = officeDetail.Phone
                };

                var point = GetPointCachedByOfficeId(office.Id);
                if(point != null)
                {
                    office.Latitude = point.Latitude;
                    office.Longitude = point.Longitude;
                }

                model.Offices.Add(office);
            }

            return View(model);
        }

        private List<MedicProfileActionModel> GetAllActions(string userId) 
        {
            List<MedicProfileActionModel> list = new List<MedicProfileActionModel>();

            DateTime date = DateTime.Now;
            List<SOSBody.Calendar> actions = actions = db.Calendar.Where(x => x.UserId == userId).Where(x => x.Date >= date).OrderBy(x => x.Date).ToList();
           
            foreach(SOSBody.Calendar a in actions)
            {
                //already booked - not shown
                if (a.Booked || a.IsSOSbody == false)
                    continue;

                var officeDetail = db.OfficeDetails.Where(x => x.Id == a.OfficeId).FirstOrDefault();
                if (officeDetail != null)
                {
                    MedicProfileActionModel model = new MedicProfileActionModel()
                    {
                        Id = a.Id,
                        UserId = userId,
                        Address = officeDetail.Street + " " + officeDetail.StreetNo,
                        City = officeDetail.City,
                        Price = Convert.ToInt32(a.Price),
                        Date = a.Date.ToString("dd. MM. HH:mm"),
                        Duration = a.Duration.ToString()
                    };

                    list.Add(model);
                }
            }

            return list.ToList(); // list.Take(10).ToList();
        }
        public async Task<List<FindResultModel>> InitCarousel()
        {
            List<FindResultModel> list = new List<FindResultModel>();

            List<FindResultModel> cachedMedicList = await GetCachedMedicsPosListAsync();
            if (cachedMedicList != null && cachedMedicList.Count > 0)
                list = cachedMedicList;
            else
            {
                List<MedicDisplayInfo> medicList = await GetAllMedicsPosListAsync();
                foreach (MedicDisplayInfo p in medicList)
                {
                    //if (p.ActionList.Count > 0)
                    {
                        FindResultModel result = new FindResultModel()
                        {
                            UserId = p.UserId,
                            Latitude = p.Position.Latitude,
                            Longitude = p.Position.Longitude,
                            Description = p.Position.error,
                            Name = p.Name,
                            LastName = p.LastName,
                            Title = p.Title,
                            TitleBehind = p.TitleBehind,
                            Address = p.Address,
                            Street = p.Street,
                            StreetNo = p.StreetNo,
                            City = p.City,
                            OpenningHours = p.OpenningHours,
                            ProfileImage = p.ProfileImage,
                            Distance = p.Distance,
                            ActionList = new List<Models.ActionInfoModel>()
                        };

                        foreach (ActionInfo ai in p.ActionList)
                        {
                            Models.ActionInfoModel actionInfo = new Models.ActionInfoModel()
                            {
                                Id = ai.Id,
                                Date = ai.Date,
                                Price = ai.Price,
                                OfficeId = ai.OfficeId,
                                Duration = ai.Duration
                            };

                            result.ActionList.Add(actionInfo);
                        }

                        list.Add(result);
                    }
                }
            }


            //var medicList = db.MedicDetails.ToList();
            //for(int i = 0; i < medicList.Count; i++)
            //{
            //    FindResultModel carousel = new FindResultModel()
            //    {
            //        Name = medicList[i].Name + " " + medicList[i].Lastname,
            //        OpenningHours = "9:00 - 19:00",
            //        ProfileImage = GetProfileImage(Globals.ProfileImagesPath, medicList[i].UserId)
            //    };

            //    list.Add(carousel);

            //    if (i > 7)
            //        break;
            //}

            string userId = "01bdcc54-4183-4dbb-9277-0763b37fd5b7";
            bool foundBitnar = false;
            foreach(var item in list)
            {
                if(item.UserId == userId)
                    foundBitnar = true;
            }

            if(foundBitnar == false)
            {
                var t = await GetSingleResultByUserId(userId);
                if(t != null)
                    list.Add(t);
            }

            return list;
        }

        private async Task<FindResultModel> GetSingleResultByUserId(string userId)
        {
            var detail = db.OfficeDetails.Where(x => x.UserId == userId).FirstOrDefault();
            if (detail == null)
                return null;

            string address = detail.Street + " " + detail.StreetNo + ", " + detail.City;
            GeoPoint point = await GetPoint(address);
            if (point == null)
                return null;

            if (point.Latitude == null || point.Longitude == null)
                return null;

            MedicDisplayInfo result = new MedicDisplayInfo();
            result.Street = detail.Street;
            result.StreetNo = detail.StreetNo;
            result.City = detail.City;
            result.Position = point;
            result.UserId = detail.UserId;
            var medicDetail = db.MedicDetails.Where(x => x.UserId == detail.UserId).FirstOrDefault();
            if (medicDetail != null)
            {
                result.Name = medicDetail.Name;
                result.LastName = medicDetail.Lastname;
                result.Phone = medicDetail.Phone;
                result.Email = medicDetail.Email;
                result.Title = medicDetail.Title;
                result.TitleBehind = medicDetail.TitleBehind;
            }
            result.Address = address;
            result.ProfileImage = GetProfileImage(Globals.ProfileImagesPath, detail.UserId);
            result.ActionList = new List<ActionInfo>();
            DateTime dateToday = DateTime.Now;

            List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
            DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 1);
            endDate = endDate.AddDays(5.0);
            actions = db.Calendar.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();

            foreach (SOSBody.Calendar cal in actions)
            {
                if (cal.IsSOSbody && cal.Booked == false)
                {
                    ActionInfo actionInfo = new ActionInfo()
                    {
                        Id = cal.Id,
                        Date = cal.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                        Price = Convert.ToInt32(cal.Price),
                        OfficeId = cal.OfficeId,
                        Duration = cal.Duration.ToString()
                    };

                    result.ActionList.Add(actionInfo);
                }
            }


            FindResultModel res = new FindResultModel()
            {
                UserId = userId,
                Latitude = result.Position.Latitude,
                Longitude = result.Position.Longitude,
                Description = result.Position.error,
                Name = result.Name,
                LastName = result.LastName,
                Title = result.Title,
                TitleBehind = result.TitleBehind,
                Address = result.Address,
                Street = result.Street,
                StreetNo = result.StreetNo,
                City = result.City,
                OpenningHours = result.OpenningHours,
                ProfileImage = result.ProfileImage,
                Distance = result.Distance,
                ActionList = new List<Models.ActionInfoModel>()
            };


            return res;
        }

        public async Task<ActionResult> Find(string location, string sort, int? page, int? limit)
        {
            if (String.IsNullOrEmpty(location))
                return null;

            GeoPoint userLocation = await GetPoint(location);
            if (userLocation == null)
                return View();

            logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Index - medic search", location);

            List<MedicDisplayInfo> medicList = await GetMedicPosListFind(userLocation);

            FindPhysioModel model = new FindPhysioModel();
            model.Results = new List<FindResultModel>();

            FindResultModel userPosition = new FindResultModel()
            {
                Latitude = userLocation.Latitude,
                Longitude = userLocation.Longitude,
                Description = "Vaše poloha"
            };

            model.Results.Add(userPosition);
            List<FindResultModel> tempList = new List<FindResultModel>();

            foreach (MedicDisplayInfo p in medicList)
            {
                if (p.ActionList.Count > 0)
                {
                    FindResultModel result = new FindResultModel()
                    {
                        UserId = p.UserId,
                        Latitude = p.Position.Latitude,
                        Longitude = p.Position.Longitude,
                        Description = p.Position.error,
                        Name = p.Name,
                        LastName = p.LastName,
                        Title = p.Title,
                        TitleBehind = p.TitleBehind,
                        Address = p.Address,
                        Street = p.Street,
                        StreetNo = p.StreetNo,
                        City = p.City,
                        OpenningHours = p.OpenningHours,
                        ProfileImage = p.ProfileImage,
                        Distance = p.Distance,
                        ActionList = new List<Models.ActionInfoModel>()
                    };

                    foreach (ActionInfo ai in p.ActionList)
                    {
                        Models.ActionInfoModel actionInfo = new Models.ActionInfoModel()
                        {
                            Id = ai.Id,
                            Date = ai.Date,
                            Price = ai.Price,
                            OfficeId = ai.OfficeId,
                            Duration = ai.Duration
                        };

                        result.ActionList.Add(actionInfo);
                    }

                    tempList.Add(result);
                }
            }

            tempList = tempList.OrderBy(x => x.Distance).ToList();

            return Json(tempList , JsonRequestBehavior.AllowGet);
        }

        public ActionResult DetailView()
        {
            SearchDetailModel model = new SearchDetailModel()
            {
                Name = "Vaclav",
                LastName = "Jirkovsky"
            };

            return View(model);
        }

        //http://www.c-sharpcorner.com/UploadFile/abhikumarvatsa/various-ways-to-pass-data-from-controller-to-view-in-mvc/
        //https://code.msdn.microsoft.com/Google-Map-in-MVC5-21e19073#content
        //https://stackoverflow.com/questions/26240923/how-to-get-asp-net-mvc-controller-to-talk-with-google-maps-api-adding-markers
        //https://stackoverflow.com/questions/1527254/problem-with-asp-net-mvc-and-google-maps
        private async Task<List<MedicDisplayInfo>> GetMedicPosList(double distance, string day, GeoPoint center)
        {
            List<MedicDisplayInfo> list = new List<MedicDisplayInfo>();

            var addresses = db.OfficeDetails.ToList();

            foreach (OfficeDetail detail in addresses)
            {
                if (string.IsNullOrEmpty(detail.City) || string.IsNullOrEmpty(detail.Street) || string.IsNullOrEmpty(detail.StreetNo))
                    continue;

                string address = detail.Street + " " + detail.StreetNo + ", " + detail.City;
                //GeoPoint point = await GetPoint(address);
                GeoPoint point = await GetPointCachedByOfficeIdAsync(detail.Id);
                if (point == null)
                    return  list;

                if (point.Latitude == null || point.Longitude == null)
                    continue;

                MedicDisplayInfo result = new MedicDisplayInfo();
                result.OfficeId = detail.Id;
                result.Street = detail.Street;
                result.StreetNo = detail.StreetNo;
                result.City = detail.City;
                result.Position = point;
                result.UserId = detail.UserId;
                var medicDetail = db.MedicDetails.Where(x => x.UserId == detail.UserId).FirstOrDefault();
                if (medicDetail != null)
                {
                    result.Name = medicDetail.Name;
                    result.LastName = medicDetail.Lastname;
                    result.Phone = medicDetail.Phone;
                    result.Email = medicDetail.Email;
                    result.Title = medicDetail.Title;
                    result.TitleBehind = medicDetail.TitleBehind;
                }
                result.Address = address;
                result.ProfileImage = GetProfileImage(Globals.ProfileImagesPath, detail.UserId);
                result.ActionList = new List<ActionInfo>();
                DateTime dateToday = DateTime.Now;

                //List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
                //if (day == "Today")
                //{
                //    DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0,0,1);
                //    endDate = endDate.AddDays(1.0);
                //    var a = db.Calendars.Where(x => x.UserId == detail.UserId).ToList();
                //    actions = db.Calendars.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();
                //}
                //else if (day == "Tomorrow")
                //{
                //    dateToday = dateToday.AddDays(1.0);
                //    DateTime startDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 0);
                //    DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 0);
                //    endDate = endDate.AddDays(1.0);
                //    var a = db.Calendars.Where(x => x.UserId == detail.UserId).ToList();
                //    actions = db.Calendars.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= startDate).Where(x => x.Date < endDate).ToList();
                //}
                //else if (day == "Other")
                //{
                //    dateToday = dateToday.AddDays(2.0);
                //    actions = db.Calendars.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).ToList();
                //}

                List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
                DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 1);
                endDate = endDate.AddDays(5.0);
                actions = db.Calendar.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();

                foreach (SOSBody.Calendar cal in actions)
                {
                    if (cal.IsSOSbody && cal.Booked == false)
                    {
                        ActionInfo actionInfo = new ActionInfo()
                        {
                            Id = cal.Id,
                            Date = cal.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                            Price = Convert.ToInt32(cal.Price),
                            OfficeId = cal.OfficeId,
                            Duration = cal.Duration.ToString()
                        };

                        result.ActionList.Add(actionInfo);
                    }
                }

                double lat, lng, centLat, centLng;
                bool ret1 = Double.TryParse(point.Latitude, NumberStyles.Any, CultureInfo.InvariantCulture, out lat);
                bool ret2 = Double.TryParse(point.Longitude, NumberStyles.Any, CultureInfo.InvariantCulture, out lng);
                bool ret3 = Double.TryParse(center.Latitude, NumberStyles.Any, CultureInfo.InvariantCulture, out centLat);
                bool ret4 = Double.TryParse(center.Longitude, NumberStyles.Any, CultureInfo.InvariantCulture, out centLng);

                if (distance > 0.0 && ret1 && ret2 && ret3 && ret4)
                {
                    double dist = ComputeDistance(centLat, centLng, lat, lng, 'K');
                    if (dist < distance)
                    {
                        if (dist < 0.1)
                            dist = 0.1;

                        result.Distance = dist;
                        list.Add(result);
                    }
                }
                else
                    list.Add(result);
            }

            return list.OrderBy(x => x.Distance).ToList();
        }

        private async Task<List<MedicDisplayInfo>> GetMedicPosListFind(GeoPoint center)
        {
            double distance = 50.0;
            List<MedicDisplayInfo> list = new List<MedicDisplayInfo>();

            var addresses = db.OfficeDetails.ToList();

            foreach (OfficeDetail detail in addresses)
            {
                if (string.IsNullOrEmpty(detail.City) || string.IsNullOrEmpty(detail.Street) || string.IsNullOrEmpty(detail.StreetNo))
                    continue;

                string address = detail.Street + " " + detail.StreetNo + ", " + detail.City;
                //GeoPoint point = await GetPoint(address);
                GeoPoint point = await GetPointCachedByOfficeIdAsync(detail.Id);
                if (point == null)
                    return list;

                if (point.Latitude == null || point.Longitude == null)
                    continue;

                MedicDisplayInfo result = new MedicDisplayInfo();
                result.Street = detail.Street;
                result.StreetNo = detail.StreetNo;
                result.City = detail.City;
                result.Position = point;
                result.UserId = detail.UserId;
                var medicDetail = db.MedicDetails.Where(x => x.UserId == detail.UserId).FirstOrDefault();
                if (medicDetail != null)
                {
                    result.Name = medicDetail.Name;
                    result.LastName = medicDetail.Lastname;
                    result.Phone = medicDetail.Phone;
                    result.Email = medicDetail.Email;
                    result.Title = medicDetail.Title;
                    result.TitleBehind = medicDetail.TitleBehind;
                }
                result.Address = address;
                result.ProfileImage = GetProfileImage(Globals.ProfileImagesPath, detail.UserId);
                result.ActionList = new List<ActionInfo>();
                DateTime dateToday = DateTime.Now;

                List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
                DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 1);
                endDate = endDate.AddDays(5.0);
                actions = db.Calendar.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();

                foreach (SOSBody.Calendar cal in actions)
                {
                    if (cal.IsSOSbody && cal.Booked == false)
                    {
                        ActionInfo actionInfo = new ActionInfo()
                        {
                            Id = cal.Id,
                            Date = cal.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                            Price = Convert.ToInt32(cal.Price),
                            OfficeId = cal.OfficeId,
                            Duration = cal.Duration.ToString()
                        };

                        result.ActionList.Add(actionInfo);
                    }
                }

                double lat, lng, centLat, centLng;
                bool ret1 = Double.TryParse(point.Latitude, NumberStyles.Any, CultureInfo.InvariantCulture, out lat);
                bool ret2 = Double.TryParse(point.Longitude, NumberStyles.Any, CultureInfo.InvariantCulture, out lng);
                bool ret3 = Double.TryParse(center.Latitude, NumberStyles.Any, CultureInfo.InvariantCulture, out centLat);
                bool ret4 = Double.TryParse(center.Longitude, NumberStyles.Any, CultureInfo.InvariantCulture, out centLng);

                if (ret1 && ret2 && ret3 && ret4)
                {
                    double dist = ComputeDistance(centLat, centLng, lat, lng, 'K');
                    if (dist < distance)
                    {
                        if (dist < 0.1)
                            dist = 0.1;

                        result.Distance = dist;
                        list.Add(result);
                    }
                }
                else
                    list.Add(result);
            }

            return list.OrderBy(x => x.Distance).ToList();
        }

        private GeoPoint GetPointCachedByOfficeId(int officeId)
        {
            var item = db.CachedResults.Where(x => x.OfficeId == officeId).FirstOrDefault();
            if (item == null)
                return null;

            GeoPoint point = new GeoPoint()
            {
                Latitude = item.Latitude,
                Longitude = item.Longitude
            };

            return point;
        }

        private async Task<GeoPoint> GetPointCachedByOfficeIdAsync(int officeId)
        {
            var item = db.CachedResults.Where(x => x.OfficeId == officeId).FirstOrDefault();
            if (item == null)
                return null;

            GeoPoint point = new GeoPoint()
            {
                Latitude = item.Latitude,
                Longitude = item.Longitude
            };

            return point;
        }

        private async Task<List<FindResultModel>> GetCachedMedicsPosListAsync()
        {
            List<FindResultModel> results = new List<FindResultModel>();
            List<CachedResults> cachedResultsList = db.CachedResults.ToList();

            if (cachedResultsList == null || cachedResultsList.Count == 0)
                return results;

            foreach(CachedResults res in cachedResultsList)
            {
                List<ActionInfoModel> actionList = new List<ActionInfoModel>();
                DateTime dateToday = DateTime.Now;
                List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
                DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 1);
                endDate = endDate.AddDays(5.0);
                actions = db.Calendar.Where(x => x.UserId == res.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();

                //if (actions == null || actions.Count == 0)
                //    continue;

                foreach (SOSBody.Calendar cal in actions)
                {
                    if (cal.IsSOSbody && cal.Booked == false && cal.OfficeId == res.OfficeId)
                    {
                        ActionInfoModel actionInfo = new ActionInfoModel()
                        {
                            Id = cal.Id,
                            Date = cal.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                            Price = Convert.ToInt32(cal.Price),
                            OfficeId = cal.OfficeId,
                            Duration = cal.Duration.ToString()
                        };

                        actionList.Add(actionInfo);
                    }
                }

                //if (actionList.Count == 0)
                //    continue;

                FindResultModel info = new FindResultModel()
                {
                    Street = res.Street,
                    StreetNo = res.StreetNo,
                    City = res.City,
                    Longitude = res.Longitude,
                    Latitude = res.Latitude,
                    UserId = res.UserId,
                    Address = res.Street + " " + res.StreetNo + ", " + res.City,
                    ProfileImage = GetProfileImage(Globals.ProfileImagesPath, res.UserId),
                    Description = "",
                    OpenningHours = "",
                    OfficeId = res.OfficeId,
                    ActionList = actionList.OrderBy(x => x.Date).ToList()
                };

                var medicDetail = db.MedicDetails.Where(x => x.UserId == res.UserId).FirstOrDefault();
                if (medicDetail != null)
                {
                    info.Name = medicDetail.Name;
                    info.LastName = medicDetail.Lastname;
                    info.Title = medicDetail.Title;
                    info.TitleBehind = medicDetail.TitleBehind;
                }

                results.Add(info);
            }

            return results;
        }


        private async Task<List<MedicDisplayInfo>> GetAllMedicsPosListAsync()
        {
            List<MedicDisplayInfo> list = new List<MedicDisplayInfo>();

            var addresses = db.OfficeDetails.ToList();

            foreach (OfficeDetail detail in addresses)
            {
                if (string.IsNullOrEmpty(detail.City) || string.IsNullOrEmpty(detail.Street) || string.IsNullOrEmpty(detail.StreetNo))
                    continue;

                string address = detail.Street + " " + detail.StreetNo + ", " + detail.City;
                GeoPoint point = await GetPoint(address);
                if (point == null)
                    return list;

                if (point.Latitude == null || point.Longitude == null)
                    continue;

                MedicDisplayInfo result = new MedicDisplayInfo();
                result.Street = detail.Street;
                result.StreetNo = detail.StreetNo;
                result.City = detail.City;
                result.Position = point;
                result.UserId = detail.UserId;
                var medicDetail = db.MedicDetails.Where(x => x.UserId == detail.UserId).FirstOrDefault();
                if (medicDetail != null)
                {
                    result.Name = medicDetail.Name;
                    result.LastName = medicDetail.Lastname;
                    result.Phone = medicDetail.Phone;
                    result.Email = medicDetail.Email;
                    result.Title = medicDetail.Title;
                    result.TitleBehind = medicDetail.TitleBehind;
                }
                result.Address = address;
                result.ProfileImage = GetProfileImage(Globals.ProfileImagesPath, detail.UserId);
                result.ActionList = new List<ActionInfo>();
                DateTime dateToday = DateTime.Now;

                List<SOSBody.Calendar> actions = new List<SOSBody.Calendar>();
                DateTime endDate = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day, 0, 0, 1);
                endDate = endDate.AddDays(5.0);
                actions = db.Calendar.Where(x => x.UserId == detail.UserId).Where(x => x.Date >= dateToday).Where(x => x.Date < endDate).ToList();
                
                foreach (SOSBody.Calendar cal in actions)
                {
                    if (cal.IsSOSbody && cal.Booked == false)
                    {
                        ActionInfo actionInfo = new ActionInfo()
                        {
                            Id = cal.Id,
                            Date = cal.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                            Price = Convert.ToInt32(cal.Price),
                            OfficeId = cal.OfficeId,
                            Duration = cal.Duration.ToString()
                        };

                        result.ActionList.Add(actionInfo);
                    }
                }

                list.Add(result);
            }

            return list.OrderBy(x => x.Distance).ToList();
        }

        private async Task<GeoPoint> GetPoint(string _address)
        {
            GeoPoint location = new GeoPoint();

            string address = _address;
            //string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key={1}", Uri.EscapeDataString(address), Globals.GoogleMapsApiKey);
            string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&clientID=248882027328-gqc1ku1dqtl6pe7fqga07sr97uadb5mp.apps.googleusercontent.com?signature=M6p7eBYffuS2D4yWtgE8IOsD", Uri.EscapeDataString(address));

            try
            {
                using (var client = new HttpClient())
                {
                    var request = await client.GetAsync(requestUri);
                    var content = await request.Content.ReadAsStringAsync();
                    var xmlDocument = XDocument.Parse(content);

                    XElement status = xmlDocument.Element("GeocodeResponse").Element("status");
                    XElement errorMessage = xmlDocument.Element("GeocodeResponse").Element("error_message");
                    if (status.Value == "OK")
                    {
                        try
                        {
                            XElement result = xmlDocument.Element("GeocodeResponse").Element("result");
                            if (result != null)
                            {
                                XElement locationElement = result.Element("geometry").Element("location");
                                XElement lat = locationElement.Element("lat");
                                XElement lng = locationElement.Element("lng");
                                location.Latitude = lat.Value;
                                location.Longitude = lng.Value;
                                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Get Point - successful: " + _address, "");
                            }
                            else
                                logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "Get Point - no result from googlemaps api: " + _address, "");
                        }
                        catch (Exception ex)
                        {
                            location.Latitude = "0.0";
                            location.Longitude = "0.0";
                            location.error = ex.Message;
                            logger.Log(DateTime.Now, Logger.Level.Exception, "Anonymous user", "Get Point - address: " + _address + " - " + ex.Message, ex.InnerException.ToString());
                        }
                    }
                    else if (status.Value == "ZERO_RESULTS")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "ZERO_RESULTS " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "OVER_QUERY_LIMIT")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "OVER_QUERY_LIMIT " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "INVALID_REQUEST")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "INVALID_REQUEST " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "REQUEST_DENIED")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "REQUEST_DENIED " + _address, errorMessage != null ? errorMessage.Value : "");
                    else if (status.Value == "UNKNOWN_ERROR")
                        logger.Log(DateTime.Now, Logger.Level.Warning, "Anonymous user", "UNKNOWN_ERROR " + _address, errorMessage != null ? errorMessage.Value : "");
                }

                return location;
            }
            catch(Exception ex)
            {
                logger.Log(DateTime.Now, Logger.Level.Error, "Anonymous user", "google maps api - no connection", ex.Message);
                return null;
            }
        }

        private double ComputeDistance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) +
                          Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) *
                          Math.Cos(deg2rad(theta));
            dist = Math.Acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts decimal degrees to radians             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //::  This function converts radians to decimal degrees             :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }

        public ActionResult Career()
        {
            return View();
        }
        public ActionResult Guarantee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Guarantee(ContactModel model)
        {
            Message message = new Message()
            {
                Date = DateTime.Now,
                Email = (String.IsNullOrEmpty(model.Email) == true) ? "" : model.Email,
                Text = model.Text,
                Description = "Garance"
            };

            db.Message.Add(message);
            db.SaveChanges();

            ModelState.Clear();

            return View();
        }

        public ActionResult Proc()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public string GetProfileImage(string dir, string UserId)
        {
            string directory = Server.MapPath(dir);
            string[] fileEntries = Directory.GetFiles(directory);
            foreach (string fileName in fileEntries)
            {
                int lastSlashPos = fileName.LastIndexOf('\\');
                string name = fileName.Substring(lastSlashPos + 1);
                int lastComaPos = name.LastIndexOf('.');
                string subName = name.Substring(0, lastComaPos);

                if (subName == UserId)
                    return dir + name;
            }

            return "/Images/no_profile_photo.png";
        }

        public ActionResult RemoveEmailWatchdog(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                logger.Log(DateTime.Now, Logger.Level.Error, "Anonymous", "Remove email watchdog failed.", "E-mail: " + email);
                return View("RemoveEmailWatchdogError");
            }

            db.EmailWatchdog.RemoveRange(db.EmailWatchdog.Where(x => x.Email == email));
            db.SaveChanges();

            logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous", "Remove email watchdog successful.", "E-mail: " + email);

            return View();
        }

        public ActionResult RemoveEmailWatchdogError()
        {
            return View();
        }

        public ActionResult RemovePatientNewsletterEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                logger.Log(DateTime.Now, Logger.Level.Error, "Anonymous", "Remove patient newsletter email failed.", "E-mail: " + email);
                return View("RemovePatientNewsletterEmailError");
            }

            db.PatientNewsletter.RemoveRange(db.PatientNewsletter.Where(x => x.Email == email));
            db.SaveChanges();

            logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous", "Remove patient newsletter email successful.", "E-mail: " + email);

            return View();
        }

        public ActionResult RemovePatientNewsletterEmailError()
        {
            return View();
        }

        /// <summary>
        /// Email 009
        /// </summary>
        public void SendWatchdogEmails()
        {
            DateTime date = DateTime.Now;

            var watchdogs = db.EmailWatchdog.ToList();
            foreach(EmailWatchdog dog in watchdogs)
            {
                List<SOSBody.Calendar> items = db.Calendar.Where(x => x.Inserted > dog.LastContactDate && x.UserId == dog.UserId).ToList();

                if (items != null && items.Count > 0)
                {
                    var callbackUrl = Url.Action("MedicProfile", "Home", new { userId = dog.UserId }, protocol: Request.Url.Scheme);
                    var removeCallbackUrl = Url.Action("RemoveEmailWatchdog", "Home", new { email = dog.Email }, protocol: Request.Url.Scheme);

                    string body = @"
                        <!DOCTYPE html>
                        <html>
                        <head>
                            <title></title>
	                        <meta charset='utf-8' />
                          </head>
                          <body>
                              <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                                <strong>Vážená paní, vážený pane,</strong><br /><br />
                                <strong>Váš fyzioterapeut vypsal nové termíny. Více informací naleznete pod tímto odkazem:</strong><br /><br />";

                    body += "<a href =\"" + callbackUrl + "\">" + callbackUrl + "</a>";
                    body += @"
                               
                                <br /><br /><br />
                                Pro odhlášení z odběru informací o nových termínech klikněte ";
                    body += "<a href =\"" + removeCallbackUrl + "\">" + removeCallbackUrl + "</a>";
                    body += @"
                                <br /><br /><br /><br />
                                <strong>Váš SOS Body team</strong>
                                <br />
                                <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                            </p>
                        </body>
                        </html>";

                    MailMessage message = new MailMessage("info@sosbody.cz", dog.Email, "Nové termíny Vašeho fyzioterapeuta", body);
                    message.IsBodyHtml = true;
                    SmtpClient client = new SmtpClient("smtp.zoner.com");
                    client.Send(message);

                    dog.LastContactDate = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }

        public async Task CacheResultsAllAsync()
        {
            // clear all items
            var data = (from n in db.CachedResults select n);
            db.CachedResults.RemoveRange(data);
            db.SaveChanges();


            List<OfficeDetail> officeList = db.OfficeDetails.ToList();

            logger.Log(DateTime.Now, Logger.Level.Trace, "SERVER", "CacheResultsAllAsync()", "Requested address count: " + officeList.Count);

            int totalCount = 0;
            foreach (OfficeDetail office in officeList)
            {
                if (string.IsNullOrEmpty(office.City) || string.IsNullOrEmpty(office.Street) || string.IsNullOrEmpty(office.StreetNo))
                    continue;

                string address = office.Street + " " + office.StreetNo + ", " + office.City;
                GeoPoint point = await GetPoint(address);
                if (point == null)
                    continue;

                if (point.Latitude == null || point.Longitude == null)
                    continue;

                CachedResults result = new CachedResults()
                {
                    OfficeId = office.Id,
                    UserId = office.UserId,
                    Name = office.Name,
                    Street = office.Street,
                    StreetNo = office.StreetNo,
                    PostalCode = office.PostalCode,
                    City = office.City,
                    Country = office.Country,
                    Latitude = point.Latitude,
                    Longitude = point.Longitude
                };

                db.CachedResults.Add(result);
                totalCount++;
            }

            db.SaveChanges();

            logger.Log(DateTime.Now, Logger.Level.Trace, "SERVER", "CacheResultsAllAsync()", "Total count: " + totalCount.ToString());
        } 

        public ActionResult MedicEmail()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MedicEmail(ContactModel model)
        {
            if (String.IsNullOrEmpty(model.MedicEmail) == false)
            {
                var item = db.MedicEmails.Where(x => x.Email == model.MedicEmail).FirstOrDefault();
                if (item == null)
                {
                    MedicEmails email = new MedicEmails
                    {
                        Email = model.MedicEmail,
                        InsertedDate = DateTime.Now
                    };

                    db.MedicEmails.Add(email);
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Contact", "Home");
        }

        [HttpPost]
        public ActionResult WatchdogEmail(string email, string userId)
        {
            if (validator.IsEmailValid(email))
            {
                InsertPatientNewsletterEmail(email);

                int itemsCount = db.EmailWatchdog.Count();
                if (itemsCount > 0)
                {
                    var watchdog = db.EmailWatchdog.Where(x => x.UserId == userId && x.Email == email).FirstOrDefault();
                    if (watchdog == null)
                    {
                        EmailWatchdog newEmail = new EmailWatchdog()
                        {
                            Email = email,
                            UserId = userId,
                            LastContactDate = DateTime.Now
                        };

                        db.EmailWatchdog.Add(newEmail);
                        db.SaveChanges();

                        logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Watchdog e-mail inserted", "UserId: " + userId + ", E-mail: " + email);

                        SendWatchdogRegistrationEmail(email);

                        return Json(new { Result = "" });
                    }    

                    return Json(new { Result = "Tento e-mail již byl vložen" });
                }
                else
                {
                    EmailWatchdog newEmail = new EmailWatchdog()
                    {
                        Email = email,
                        UserId = userId,
                        LastContactDate = DateTime.Now
                    };

                    db.EmailWatchdog.Add(newEmail);
                    db.SaveChanges();

                    logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Watchdog e-mail inserted", "UserId: " + userId + ", E-mail: " + email);
                }

                SendWatchdogRegistrationEmail(email);

                return Json(new { Result = "" });
            }
            else
                return Json(new { Result = "Špatný formát e-mailu" });
        }

        private bool InsertPatientNewsletterEmail(string email)
        {
            var item = db.PatientNewsletter.Where(x => x.Email == email).FirstOrDefault();
            if (item == null)
            {
                PatientNewsletter model = new PatientNewsletter()
                {
                    Email = email,
                    Inserted = DateTime.Now
                };

                db.PatientNewsletter.Add(model);
                db.SaveChanges();

                logger.Log(DateTime.Now, Logger.Level.Trace, "Anonymous user", "Patient newsletter e-mail inserted", "E-mail: " + email);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Email 010
        /// </summary>
        /// <param name="email"></param>
        private void SendWatchdogRegistrationEmail(string email)
        {
            var removeNewsletterCallbackUrl = Url.Action("RemovePatientNewsletterEmail", "Home", new { email = email }, protocol: Request.Url.Scheme);

            string body = @"
                <!DOCTYPE html>
                <html>
                <head>
                    <title></title>
	                <meta charset='utf-8' />
                    </head>
                    <body>
                        <p style='color:rgb(56,118,29);font-size:13px;font:normal arial,sans-serif;'>
                        <strong>Vážená paní, vážený pane,</strong><br /><br />
                        <strong>hlídací pes Vašeho fyzioterapeuta byl nastaven.</strong><br /><br />
                        <br /><br /><br /><br />
                        <strong>Váš SOS Body team</strong>
                        <br />
                        <img src='https://ci3.googleusercontent.com/proxy/HfoF_ytq0yVRM40KVwRFnKjibgjBFWEibDzwZSC_rTNPoXQaXlZXQ9qx97ovbrT-314Og2GDlAf5zob_kN2s1xaT44fvu_fYd2cEpmgdUIEzb-DBhvXHjIPk7-WCx9wTOzICrR1VlFEdZAgbfMU=s0-d-e1-ft#https://drive.google.com/a/soska.tech/uc?id=0B7q5XNn_XjkvMUtFZnBQemlsbms&amp;export=download'>
                    </p>
                </body>
                </html>";

            MailMessage message = new MailMessage("info@sosbody.cz", email, "Hlídací pes termínů Vašeho fyzioterapeuta", body);
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient("smtp.zoner.com");
            client.Send(message);
        }
    }

    public class GeoPoint
    {
        public string Latitude;
        public string Longitude;
        public string error;

        public GeoPoint()
        { }
        public GeoPoint(string lat, string lng)
        {
            Latitude = lat;
            Longitude = lng;
        }
    }

    public class MedicDisplayInfo
    {
        public string UserId;
        public int OfficeId;
        public GeoPoint Position;
        public string Name;
        public string LastName;
        public string Title;
        public string TitleBehind;
        public string Address;
        public string Street;
        public string StreetNo;
        public string City;
        public string OpenningHours;
        public string Phone;
        public string Email;
        public string ProfileImage;
        public double Distance;
        public List<ActionInfo> ActionList;
    }

    public class ActionInfo
    {
        public int Id;
        public string Date;
        public int Price;
        public int OfficeId;
        public string Duration;
    }
}
