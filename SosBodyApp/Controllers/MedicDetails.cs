﻿namespace SosBody.Controllers
{
    internal class MedicDetails
    {
        public int Id { get; set; }
        public string UserId { get; set;}
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
    }
}