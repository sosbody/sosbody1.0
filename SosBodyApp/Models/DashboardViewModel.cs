﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SosBody.Models
{
    public class BasicMedicsModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool IsMedicPersonalData { get; set; }
        public bool IsMedicOfficeData { get; set; }
        public bool IsMedicInviceData { get; set; }
        public bool IsProfileImage { get; set; }
        public int FreeDates { get; set; }
        public int TotalDates { get; set; }
        public int TotalOrders { get; set; }

    }

    public class AllMedicsModel
    {
        public List<BasicMedicsModel> Medics { get; set; }
        public int CompletedRegistrations { get; set; }
        public int IncompletedRegistrations { get; set; }
    }

    public class MedicDetailModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        [Display(Name = "Fyzioterapie funkčních poruch")]
        public bool Spec1 { get; set; }
        [Display(Name = "Fyzio po úrazech a operacích")]
        public bool Spec2 { get; set; }
        [Display(Name = "Fyzio dětí")]
        public bool Spec3 { get; set; }
        [Display(Name = "Fyzio neurologických diagnóz")]
        public bool Spec4 { get; set; }
        [Display(Name = "Fyzio gerontů")]
        public bool Spec5 { get; set; }
        [Display(Name = "Home Fyzio")]
        public bool Spec6 { get; set; }
        public string IdentifyNo { get; set; }
        public string TaxIdentifyNo { get; set; }
        public string BankAccount { get; set; }
        public string InvoiceAddress { get; set; }
        public string InvoiceEmail { get; set; }
        public string InvoicePhone { get; set; }

        public List<DashboardOfficeDetailModel> Offices { get; set; }
        virtual public List<ContactHistory> History { get; set; }
        public string NewText { get; set; }
    }

    public class DashboardOfficeDetailModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [Required(ErrorMessage = "Název ordinace musí být vyplněn")]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Položka Ulice musí být vyplněna")]
        [MaxLength(128)]
        public string Street { get; set; }

        [Required(ErrorMessage = "Položka Číslo popisné musí být vyplněna")]
        [MaxLength(8)]
        public string StreetNo { get; set; }

        [Required(ErrorMessage = "Položka PSČ musí být vyplněna")]
        [RegularExpression("([0-9]*)", ErrorMessage = "Položka PSČ obsahuje neplatné znaky")]
        [MaxLength(5)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Položka Město musí být vyplněna")]
        [MaxLength(128)]
        public string City { get; set; }

        [Required(ErrorMessage = "Položka Země musí být vyplněna")]
        [MaxLength(128)]
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        public string Phone { get; set; }
        //public List<SelectListItem> PhonePrefixList { get; set; }
        //public string PhonePrefix { get; set; }
        //public int PhonePrefixLength { get; set; }
        public int Price30 { get; set; }
        public int Price60 { get; set; }
        public int Price90 { get; set; }
    }

    public class AllEmailsModel
    {
        public List<EmailModel> Emails { get; set; }
    }

    public class EmailModel
    {
        public string Email { get; set; }
        public string Invited { get; set; }
        public string Inserted { get; set; }
    }
    public class EmailDetailModel
    {
        public string Email { get; set; }
        public string Invited { get; set; }
        public string Inserted { get; set; }
        virtual public List<ContactHistory> History { get; set; }
        public string NewText { get; set; }
    }

    public class ContactHistory
    {
        public string Date { get; set; }
        public string UserLogin { get; set; }
        public string Text { get; set; }
    }

    public class AllMessagesModel
    {
        public List<MessageModel> Messages { get; set; }
    }

    public class MessageModel
    {
        public string Date { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }

    public class AllLogsModel
    {
        public List<LogModel> Logs { get; set; }
    }

    public class LogModel
    {
        public string UserId { get; set; }
        public string Date { get; set; }
        public string Level { get; set; }
        public string Info { get; set; }
        public string Description { get; set; }
    }

    public class AllPatientNewslettersModel
    {
        public List<string> Emails { get; set; }
    }

    public class WatchdogModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }

    public class AllWatchdogsModel
    {
        public List<WatchdogModel> Watchdogs { get; set; }
    }
}