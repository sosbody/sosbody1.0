﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SosBody.Models
{
    public class CalendarModel
    {
        public int WeekIndex { get; set; }
        public int StartHourIndex { get; set; }
        public int EndHourIndex { get; set; }
        public List<Day> Days { get; set; }
        public int Year { get; set; }
        public List<OfficeDetailModel> Offices { get; set; }
        public int OfficeCount { get; set; }
        public int CurrentCalendarId { get; set; }
        public List<int> StartHours { get; set; }
        public List<int> EndHours { get; set; }
        public List<int> Price30 { get; set; }
        public List<int> Price60 { get; set; }
        public List<int> Price90 { get; set; }
    }



    public class WeekForMonth
    {
        public List<Day> Week1 { get; set; }
        public List<Day> Week2 { get; set; }
        public List<Day> Week3 { get; set; }
        public List<Day> Week4 { get; set; }
        public List<Day> Week5 { get; set; }
        public List<Day> Week6 { get; set; }
        public string nextMonth { get; set; }
        public string prevMonth { get; set; }
    }

    public class Day
    {
        public DateTime Date { get; set; }
        public string _Date { get; set; }
        public string DateStr { get; set; }
        public int dtDay { get; set; }
        public int? daycolumn { get; set; }
        public bool Weekend { get; set; }
        public List<Action> Actions { get; set; }
        public int Index { get; set; }
        public bool Visible { get; set; }
        public bool IsToday { get; set; }
    }

    public class Action
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public int StartIndex { get; set; }
        public DateTime End { get; set; }
        public int EndIndex { get; set; }
        public string Info { get; set; }
        public float Duration { get; set; }
        public float StartPos { get; set; }
        public int Price { get; set; }
        public bool Booked { get; set; }
        public bool IsSosBody { get; set; }
    }

    public class PopulateMonth
    {
        public int month { get; set; }
        public int totaldayformonth { get; set; }
        public int year { get; set; }
    }

}