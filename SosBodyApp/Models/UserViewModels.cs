﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SosBody.Models
{

    [NotMapped]
    public class CreateModel
    {
        [Required(ErrorMessage = "Položka E-mail musí být vyplněna")]
        [Display(Name = "Uživatelské jméno (E-mail)")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Položka Heslo musí být vyplněna")]
        [Display(Name = "Heslo")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Položka Opakujte heslo musí být vyplněna")]
        [Display(Name = "Opakujte heslo")]
        public string ConfirmPassword { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Chybí souhlas s všeobecnými obchodními podmínkami")]
        public bool Accept { get; set; }
    }

    public class CreateModelPatient
    {
        [Required(ErrorMessage = "Položka E-mail musí být vyplněna")]
        [Display(Name = "Uživatelské jméno (E-mail)")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Položka Heslo musí být vyplněna")]
        [Display(Name = "Heslo")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Položka Opakujte heslo musí být vyplněna")]
        [Display(Name = "Opakujte heslo")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Uživatelské jméno")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Heslo")]
        public string Password { get; set; }
    }

    public class ReservationModel
    {
        [Required]
        [Display(Name = "Rezervační kód (XXXX-XXXX-XXXX)")]
        [MaxLength(14)]
        public string Reservation { get; set; }
    }

    public class ReservationDetailModel
    {
        public int OrderId { get; set; }
        public string Code { get; set; }
        public string MedicName { get; set; }
        public string MedicAddress { get; set; }
        public string Date { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool CancelEnabled { get; set; }
    }

    public class PasswordResetEmailModel
    {
        [Required]
        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }
    }

    public class InviteMedicModel
    {
        [Required]
        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }
    }


    public class CalendarSettingsModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int OfficeId { get; set; }
        public string ProfileImage { get; set; }

        [Display(Name = "Po")]
        public bool Monday { get; set; }
        [Display(Name = "Út")]
        public bool Tuesday { get; set; }
        [Display(Name = "St")]
        public bool Wednesday { get; set; }
        [Display(Name = "Čt")]
        public bool Thursday { get; set; }
        [Display(Name = "Pá")]
        public bool Friday { get; set; }
        [Display(Name = "So")]
        public bool Saturday { get; set; }
        [Display(Name = "Ne")]
        public bool Sunday { get; set; }
        public int MondayStartHour { get; set; }
        public int MondayEndHour { get; set; }
        public int TuesdayStartHour { get; set; }
        public int TuesdayEndHour { get; set; }
        public int WednesdayStartHour { get; set; }
        public int WednesdayEndHour { get; set; }
        public int ThursdayStartHour { get; set; }
        public int ThursdayEndHour { get; set; }
        public int FridayStartHour { get; set; }
        public int FridayEndHour { get; set; }
        public int SaturdayStartHour { get; set; }
        public int SaturdayEndHour { get; set; }
        public int SundayStartHour { get; set; }
        public int SundayEndHour { get; set; }
        public bool GoogleCalendarConnection { get; set; }
        public List<SelectListItem> MapHoursItemTypes { get; set; }
        public List<OfficeDetailModel> Offices { get; set; }
        public int CurrentCalendarId { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Zadané číslo není platné")]
        public int Price30 { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Zadané číslo není platné")]
        public int Price60 { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Zadané číslo není platné")]
        public int Price90 { get; set; }
    }

    public class FormMedicModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required(ErrorMessage = "Položka Jméno musí být vyplněna")]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Položka Příjmení musí být vyplněna")]
        [MaxLength(128)]
        public string Lastname { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        [Required(ErrorMessage = "Položka Telefon musí být vyplněna")]
        public string Phone { get; set; }
        public List<SelectListItem> PhonePrefixList { get; set; }
        public string PhonePrefix { get; set; }
        public int PhonePrefixLength { get; set; }

        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        [MaxLength(128)]
        public string Email { get; set; }
        public string Title { get; set; }
        public string TitleBehind { get; set; }
        public string ProfileImage { get; set; }
        //[Required(ErrorMessage = "Položka Rok musí být vyplněna")]
        //public string Year { get; set; }
        //[Required(ErrorMessage = "Položka Měsíc musí být vyplněna")]
        //public string Month { get; set; }
        //[Required(ErrorMessage = "Položka Den musí být vyplněna")]
        //public string Day { get; set; }
        //public List<SelectListItem> MapYearItemTypes { get; set; }
        //public List<SelectListItem> MapMonthItemTypes { get; set; }
        //public List<SelectListItem> MapDayItemTypes { get; set; }

        public List<OfficeDetailModel> Offices { get; set; }

        [Display(Name = "Fyzioterapie funkčních poruch")]
        public bool Spec1 { get; set; }
        [Display(Name = "Fyzio po úrazech a operacích")]
        public bool Spec2 { get; set; }
        [Display(Name = "Fyzio dětí")]
        public bool Spec3 { get; set; }
        [Display(Name = "Fyzio neurologických diagnóz")]
        public bool Spec4 { get; set; }
        [Display(Name = "Fyzio gerontů")]
        public bool Spec5 { get; set; }
        [Display(Name = "Home Fyzio")]
        public bool Spec6 { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class MedicProfileModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        //[Required(ErrorMessage = "Položka Telefon musí být vyplněna")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Položka E-mail musí být vyplněna")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public List<OfficeDetailModel> Offices { get; set; }
        public List<MedicProfileActionModel> Actions { get; set; }
        [Display(Name = "Fyzioterapie funkčních poruch")]
        public bool Spec1 { get; set; }
        [Display(Name = "Fyzio po úrazech a operacích")]
        public bool Spec2 { get; set; }
        [Display(Name = "Fyzio dětí")]
        public bool Spec3 { get; set; }
        [Display(Name = "Fyzio neurologických diagnóz")]
        public bool Spec4 { get; set; }
        [Display(Name = "Fyzio gerontů")]
        public bool Spec5 { get; set; }
        [Display(Name = "Home Fyzio")]
        public bool Spec6 { get; set; }
    }

    public class MedicProfileActionModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int Price { get; set; }
        public int OfficeId { get; set; }
        public string UserId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Duration { get; set; }
    }

    public class OfficeDetailModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        
        [Required(ErrorMessage = "Název ordinace musí být vyplněn")]
        [MaxLength(64)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Položka Ulice musí být vyplněna")]
        [MaxLength(128)]
        public string Street { get; set; }

        [Required(ErrorMessage = "Položka Číslo popisné musí být vyplněna")]
        [MaxLength(8)]
        public string StreetNo { get; set; }

        [Required(ErrorMessage = "Položka PSČ musí být vyplněna")]
        [RegularExpression("([0-9]*)", ErrorMessage = "Položka PSČ obsahuje neplatné znaky")]
        [MaxLength(5)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Položka Město musí být vyplněna")]
        [MaxLength(128)]
        public string City { get; set; }

        [Required(ErrorMessage = "Položka Země musí být vyplněna")]
        [MaxLength(128)]
        public string Country { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        public string Phone { get; set; }
        public List<SelectListItem> PhonePrefixList { get; set; }
        public string PhonePrefix { get; set; }
        public int PhonePrefixLength { get; set; }
    }

    public class OfficeListModel
    {
        public List<OfficeDetailModel> Offices { get; set; }
        public bool ShowAddOfficeButton { get; set; }
    }

    public class InvoiceDetailModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        [MaxLength(8)]
        public string IdentifyNo { get; set; }

        [MaxLength(10)]
        public string TaxIdentifyNo { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Předčíslí bankovního účtu obsahuje chyby")]
        [MaxLength(6)]
        public string Prefix { get; set; }

        [RegularExpression("([0-9]*)", ErrorMessage = "Číslo bankovního účtu obsahuje chyby")]
        [MaxLength(10)]
        public string BankAccount { get; set; }
        public List<SelectListItem> PostfixList { get; set; }
        public string Postfix { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        public string Phone { get; set; }
        public List<SelectListItem> PhonePrefixList { get; set; }
        public string PhonePrefix { get; set; }
        public int PhonePrefixLength { get; set; }
        //[Required(ErrorMessage = "Položka E-mail musí být vyplněna")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public string ErrorMessage { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Chybí souhlas se zprostředkovatelskou smlouvou")]
        public bool Accept { get; set; }
    }

    public class ConfirmPasswordModel
    {
        public string Email { get; set; }
    }

    public class FormPatientModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        [Required(ErrorMessage = "Položka Telefon musí být vyplněna")]
        public string Phone { get; set; }
    }

    public class PasswordResetModel
    {
        public string UserId { get; set; }
        public string Code { get; set; }

        [Required(ErrorMessage = "Položka Nové heslo musí být vyplněna")]
        [Display(Name = "Nové heslo:")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Položka Opakujte heslo musí být vyplněna")]
        [Display(Name = "Opakujte heslo:")]
        public string ConfirmPassword { get; set; }
    }

    public class CarouselModel
    {
        public string Name { get; set; }

        public string OpenningHours { get; set; }

        public string ProfileImage { get; set; }
    }

    public class FindPhysioModel
    {
        [Display(Name = "Adresa:")]
        public string Address { get; set; }

        [Display(Name = "Vzdálenost:")]
        public int Distance { get; set; }

        [Display(Name = "Volný termín:")]
        public string OpenningHours { get; set; }

        public List<double> Lats { get; set; } 

        public List<FindResultModel> Results { get; set; }
        public List<SelectListItem> MapDistanceItemTypes { get; set; }

        public List<SelectListItem> MapAvailabilityItemTypes { get; set; }
        public List<FindResultModel> Carousel { get; set; }

        public SearchDetailModel PartialModel { get; set; }
    }

    public class FindResultModel
    {
        public string UserId { get; set; }
        public int OfficeId { get; set; }
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string TitleBehind { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string City { get; set; }
        public string OpenningHours { get; set; }
        public string ProfileImage { get; set; }
        public double Distance { get; set; }
        public virtual List<ActionInfoModel> ActionList { get; set; }
    }

    public class ActionInfoModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int Price { get; set; }
        public int OfficeId { get; set; }
        public string Duration { get; set; }
    }

    public class SearchDetailModel
    {
        public string Name { get; set; }
        public string LastName { get; set; }
    }

    public class OrderUserModel
    {
        public string MedicUserId { get; set; }
        public int OfficeId { get; set; }
        public int ActionId { get; set; }
        public string MedicName { get; set; }
        public string Address { get; set; }
        public string Date { get; set; }
        public string Price { get; set; }

        [Required(ErrorMessage = "Položka Jméno musí být vyplněna")]
        [Display(Name = "Jméno*:")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Položka Příjmení musí být vyplněna")]
        [Display(Name = "Příjmení*:")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Položka E-mail musí být vyplněna")]
        [Display(Name = "E-mail*:")]
        public string UserEmail { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Telefonní číslo obsahuje chyby")]
        [Required(ErrorMessage = "Položka Telefon musí být vyplněna")]
        [Display(Name = "Telefon*:")]
        public string UserPhone { get; set; }
        public List<SelectListItem> PhonePrefixList { get; set; }
        public string PhonePrefix { get; set; }
        public int PhonePrefixLength { get; set; }

        public string Duration { get; set; }
        public string ErrorMessage { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Chybí souhlas s podmínkami užití SOSbody.cz")]
        public bool Accept { get; set; }
    }


    public class ContactModel
    {
        [Required(ErrorMessage = "Položka E-mail není vyplněná")]
        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Neplatná E-mailová adresa")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Položka Vzkaz není vyplněná")]
        [Display(Name = "Vzkaz")]
        public string Text { get; set; }

        [Display(Name = "E-mail")]
        public string MedicEmail { get; set; }
    }


    public class HomePatientModel
    {
        public string UserId { get; set; }
    }

    public class HistoryPatientModel
    {
        public List<ReservationDetailModel> Reservations { get; set; }
    }
}